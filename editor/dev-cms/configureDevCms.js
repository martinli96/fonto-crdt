'use strict';

const routes = [
	require('./routes/customizeCmsStandardBrowseRoute'),
	require('./routes/customizeCmsStandardGetDocumentRoute'),
	require('./routes/customizeCmsStandardOutputSupport'),
	require('./routes/customizeCmsStandardPostDocument')
];

module.exports = (router, config) => {
	return {
		routes: routes.map(route => route(router, config))
	};
};
