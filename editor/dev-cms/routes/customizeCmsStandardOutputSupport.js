var path = require('path');

function pathJoin(item, item2) {
	var fileId = path.join(item, item2);
	if (path.sep === '\\') {
		fileId = fileId.replace(/\\/g, '/');
	}
	return fileId;
}

function determineHierarchyByItemId(itemId) {
	var hierarchyIds = itemId.split('/');
	hierarchyIds.unshift(null);
	return hierarchyIds.reduce((hierarchy, id, index) => {
		if (id === null) {
			hierarchy[index] = {
				id: null,
				label: 'My drive',
				type: 'folder'
			};
			return hierarchy;
		}

		var previousHierarchyItem = hierarchy[index - 1];
		if (previousHierarchyItem) {
			hierarchy[index] = {
				id: pathJoin(previousHierarchyItem.id || '', id),
				label: id,
				type: 'folder'
			};
			return hierarchy;
		}
	}, []);
}

function handleCmsStandardOutputSupport(req, res, next) {
	// Override json response method to apply custom metadata.
	var originalResJson = res.json;
	res.json = function addHierarchyToResponseAndMetadataWithHierarchyToItems(data) {
		data.metadata = {
			hierarchy: determineHierarchyByItemId(req.query.documentId)
		};

		originalResJson.call(res, data);
	};

	next();
}

module.exports = (router, _config) => {
	router.route('/connectors/cms/standard/asset').get(handleCmsStandardOutputSupport);
	return router;
};
