const path = require('path');
const fs = require('fs');

const extensionsByAssetType = {
	'document': ['.document.html', '.template.html', '.definition.html', '.fragment.html'],
	// 'template': ['.template.html'],
	// 'definition': ['.definition.html'],
	// 'fragment': ['.fragment.html'],
	'output-support': ['.css', '.hf.html'],
	'image': ['.png', '.gif', '.bmp', '.jpg', '.jpeg', '.svg']
};

function pathJoin(item, item2) {
	let fileId = path.join(item, item2);
	if (path.sep === '\\') {
		fileId = fileId.replace(/\\/g, '/');
	}
	return fileId;
}

function determineHierarchyByItemId(itemId, assetTypes) {
	const hierarchyIds = itemId.split('/');
	hierarchyIds.unshift(null);
	return hierarchyIds.reduce((hierarchy, id, index) => {
		if (id === null) {
			hierarchy[index] = {
				id: null,
				label: assetTypes[0] === 'document-template' ? 'Templates' : 'My drive',
				type: 'folder'
			};
			return hierarchy;
		}

		const previousHierarchyItem = hierarchy[index - 1];
		if (previousHierarchyItem) {
			hierarchy[index] = {
				id: pathJoin(previousHierarchyItem.id || '', id),
				label: id,
				type: 'folder'
			};
			return hierarchy;
		}
		return undefined;
	}, []);
}

function handleCmsStandardBrowsePostRequest(req, res, next) {
	// Override json response method to apply custom metadata.
	const originalResJson = res.json;
	const allowedAssetTypes = (req.body.assetTypes || []).concat();
	req.body.assetTypes.push('unknown');
	res.json = function addHierarchyToResponseAndMetadataWithHierarchyToItems(data) {
		if (data && data.items && data.items.length > 0) {
			// Extensions in the assetTypes
			const filteredExtensions = allowedAssetTypes
				.map(type => extensionsByAssetType[type])
				.reduce((accum, item) => accum.concat(item), []);

			let items = data.items.filter(
				({ id, type }) =>
					type === 'folder' ||
					filteredExtensions.some(extension => id.endsWith(extension))
			);
			items = items.map(function(item) {
				item.metadata = Object.assign({}, item.metadata);
				item.metadata.hierarchy = determineHierarchyByItemId(item.id, req.body.assetTypes);

				return item;
			});

			const itemWithHierarchy = items.find(function(item) {
				return !!item.metadata.hierarchy;
			});
			const hierarchy = itemWithHierarchy ? itemWithHierarchy.metadata.hierarchy.slice() : [];
			hierarchy.pop();

			data.items = items;
			// TODO: this does not work correctly if the data.items was limited because limit was set in the request params
			// does it even need to be recalculated at this point? items are only mapped? not filtered?
			// data.totalItemCount = data.items.length;
			data.metadata = { hierarchy: hierarchy };
		} else if (req.body.folderId) {
			data.metadata = {
				hierarchy: determineHierarchyByItemId(req.body.folderId, req.body.assetTypes)
			};
		} else {
			data.metadata = {
				hierarchy: []
			};
		}

		originalResJson.call(res, data);
	};

	// Make sure the original handlers for route('/connectors/cms/standard/browse') are called.
	next();
}

module.exports = (router, _config) => {
	router.route('/connectors/cms/standard/browse').post(handleCmsStandardBrowsePostRequest);
	return router;
};
