var path = require('path');

function pathJoin(item, item2) {
	var fileId = path.join(item, item2);
	if (path.sep === '\\') {
		fileId = fileId.replace(/\\/g, '/');
	}
	return fileId;
}

function determineHierarchyByItemId(itemId) {
	var hierarchyIds = itemId.split('/');
	hierarchyIds.unshift(null);
	return hierarchyIds.reduce((hierarchy, id, index) => {
		if (id === null) {
			hierarchy[index] = {
				id: null,
				label: 'My drive',
				type: 'folder'
			};
			return hierarchy;
		}

		var previousHierarchyItem = hierarchy[index - 1];
		if (previousHierarchyItem) {
			hierarchy[index] = {
				id: pathJoin(previousHierarchyItem.id || '', id),
				label: id,
				type: 'folder'
			};
			return hierarchy;
		}
	}, []);
}

function handleCmsStandardPostDocument(req, res, next) {
	// Override json response method to apply custom metadata.
	const fs = require('fs');

	const data = req.body;
	const filename = data.folderId + '/' + data.metadata.fileName + data.metadata.fileExtension;

	let path = __dirname;
	path = path.replace('routes', 'files/' + filename);

	if (!fs.existsSync(path)) {
		fs.writeFile(path, data.content, err => {
			if (err) {
				throw err;
			}
		});
	}

	const originalResJson = res.json;
	res.json = data => {
		data.metadata = {
			hierarchy: determineHierarchyByItemId(filename)
		};

		data.documentId = filename;

		originalResJson.call(res, data);
	};

	next();
}

module.exports = (router, _config) => {
	router.route('/connectors/cms/standard/document').post(handleCmsStandardPostDocument);
	return router;
};
