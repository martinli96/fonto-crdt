import configurationManager from 'fontoxml-configuration/src/configurationManager.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';

// Experiments
configurationManager.set('enable-experiment/correct-selection-after-enter', true);
configurationManager.set('enable-experiment/drag-and-drop-in-structure-view-sidebar', true);
configurationManager.set('enable-experiment/default-generic-drag-and-drop-operation', false);

configurationManager.set('remote-document-state-hide-widget-when-acquired', true);

configurationManager.set('paragraph-node-name-for-pasting', 'p');

configurationManager.set('initial-editor-sidebar-tab-id', 'structure');

configurationManager.set('document-title-element-xpath', '/html/body/h1');

configurationManager.set('unique-id-configurations', [
	{
		selector: 'self::*',
		namespaceURI: null,
		localName: 'id'
	}
]);

configurationManager.set('preferred-locales', ['en-fonto', 'en']);

namespaceManager.addNamespace('wws', 'urn:fontoxml:working:with:structures');

// Enable XML view only in localhost.
configurationManager.set('enable-xml-view', window.location.hostname.includes('localhost'));

// Enable or disable the review feature in author mode
configurationManager.set('enable-review', true);

// Enable or disable the track changes feature in author mode
configurationManager.set('track-changes-is-enabled-on-startup', true);

// Neutral Theme
configurationManager.set('theme-name', 'neutral');
