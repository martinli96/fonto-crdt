import configurationManager from 'fontoxml-configuration/src/configurationManager.js';
import sxManager from 'fontoxml-modular-schema-experience/src/sxManager.js';
import SchemaLocationToSchemaExperienceResolver from 'fontoxml-schema-experience-resolver/src/SchemaLocationToSchemaExperienceResolver.js';
import FONTO_HTML_SX_SHELL_LOCATION from 'fonto-html-sx-shell/src/SCHEMA_LOCATIONS.js';

const schemaLocationToSchemaExperienceResolver = new SchemaLocationToSchemaExperienceResolver();

schemaLocationToSchemaExperienceResolver.register(
	sxManager.defineSchemaExperience('assets/schemas/fonto-html-sx-shell.json', [
		'fonto-html-sx-shell'
	]),
	FONTO_HTML_SX_SHELL_LOCATION
);

configurationManager.set('schema-experience-resolver', schemaLocationToSchemaExperienceResolver);
