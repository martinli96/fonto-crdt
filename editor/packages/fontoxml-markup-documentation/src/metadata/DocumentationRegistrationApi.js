import addNodeSelectionApi from 'fontoxml-modular-schema-experience/src/addNodeSelectionApi.js';

/**
 * The part of the fluent registration interface where a node has been selected and documentation properties can be
 * defined.
 *
 * @param {DeclarationsForModule}        declarationsForModule Provided as an argument in
 *                                                               NodeMetadataRegistry#_createRegistrar.
 * @param {function(string, any): void}  registerComponent     Provided by addNodeSelectionApi, used to register
 *                                                               components by the methods of this registrar.
 */
function DocumentationRegistrationApi(declarationsForModule, registerComponent) {
	this._registerComponent = registerComponent;

	addNodeSelectionApi(this, declarationsForModule, function(registerComponent) {
		return new DocumentationRegistrationApi(declarationsForModule, registerComponent);
	});
}

/**
 * Sets the human-readable name for a node
 *
 * @param  {string}  [markupLabel=null]  The human-readable name.
 *
 * @return  {DocumentationRegistrationApi}  The context object, to allow method chaining.
 */
DocumentationRegistrationApi.prototype.setMarkupLabel = function(markupLabel) {
	this._registerComponent('markup-label', markupLabel === undefined ? null : markupLabel);

	return this;
};

/**
 * Sets the title query in order to retrieve title content for a node.
 *
 * @param  {string}                         [titleQuery]
 *
 * @return  {DocumentationRegistrationApi}  The context object, to allow method chaining.
 */
DocumentationRegistrationApi.prototype.setTitleQuery = function(titleQuery) {
	this._registerComponent('title-query', titleQuery || null);

	return this;
};

export default DocumentationRegistrationApi;
