import NodeMetadataRegistry from 'fontoxml-modular-schema-experience/src/NodeMetadataRegistry.js';
import addNodeSelectionApi from 'fontoxml-modular-schema-experience/src/addNodeSelectionApi.js';
import DocumentationRegistrationApi from './DocumentationRegistrationApi.js';

/**
 * A DocumentationRegistry collects documentation. This documentation may be used in, e.g., human-readable
 * breadcrumbs.
 */
function DocumentationRegistry() {
	NodeMetadataRegistry.call(this);

	// Pre-register components
	this._declarations.predefineComponent('markup-label', undefined);
	this._declarations.predefineComponent('title-query', '()');
}

DocumentationRegistry.prototype = Object.create(NodeMetadataRegistry.prototype);
DocumentationRegistry.prototype.constructor = DocumentationRegistry;

DocumentationRegistry.prototype._createRegistrar = function(_moduleName, declarationsForModule) {
	// We'll need a node spec first, use the plain node selection API to get it
	const initialRegistrarApi = Object.create(null);
	addNodeSelectionApi(initialRegistrarApi, declarationsForModule, function(registerComponent) {
		return new DocumentationRegistrationApi(declarationsForModule, registerComponent);
	});

	return initialRegistrarApi;
};

export default DocumentationRegistry;
