import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';

const TITLE_CONTENT_XPATH = [
	'let $titleQuery := fonto:metadata-property(., "title-query")',
	'return if ($titleQuery) then',
	// (: new-style title query :)
	'  fontoxpath:evaluate($titleQuery, map { ".": . })',
	'else',
	// (: old-style title selector + getTextContent :)
	'  let $titleSelector := fonto:metadata-property(., "title-selector")',
	'  return if ($titleSelector) then',
	'    fontoxpath:evaluate(',
	'      "descendant-or-self::node()[" || $titleSelector || "]",',
	'      map { ".": . }',
	'    )//text()/string() => string-join(" ")',
	'  else',
	'    ()'
].join('\n');

/**
 * Get text content of the title for a given node
 *
 * @param   {Node}       node         A node from which to get the title content
 * @param   {Object}     [_metadata]  The metadata from which to get the title content
 * @param   {Blueprint}  [blueprint]  Blueprint in which to consider node
 *
 * @return  {string}  The title content for the given node
 */
export default function getTitleContent(node, _metadata, blueprint) {
	blueprint = blueprint || readOnlyBlueprint;

	return evaluateXPathToString(TITLE_CONTENT_XPATH, node, blueprint);
}
