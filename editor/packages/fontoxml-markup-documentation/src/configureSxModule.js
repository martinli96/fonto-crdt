import sxManager from 'fontoxml-modular-schema-experience/src/sxManager.js';
import DocumentationRegistry from './metadata/DocumentationRegistry.js';

export default function configureSxModule(sxModule) {
	sxManager.addMetadataRegistry('documentation', new DocumentationRegistry());
}
