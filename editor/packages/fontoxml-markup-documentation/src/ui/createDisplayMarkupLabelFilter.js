import getMarkupLabel from '../getMarkupLabel.js';

export default function createDisplayMarkupLabelFilter() {
	return function getMarkupLabelWithFallback(element) {
		return getMarkupLabel(element) || element.nodeName;
	};
}
