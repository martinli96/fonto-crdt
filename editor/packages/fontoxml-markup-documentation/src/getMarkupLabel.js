import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';

/**
 * Get the configured markup label for the given node
 *
 * @fontosdk
 *
 * @param    {Node}       node         A node from which to get the markup label
 * @param    {Object}     [metadata]   The metadata from which to get the markup label. This will be retrieved if absent
 * @param    {Blueprint}  [blueprint]  The blueprint. Always try to pass one if you have one
 *
 * @return  {string}  The markup label for the given node
 */
export default function getMarkupLabel(node, metadata, blueprint) {
	if (!blueprint) {
		blueprint = readOnlyBlueprint;
	}
	if (!metadata) {
		const documentId = documentsManager.getDocumentIdByNodeId(
			getNodeId(blueprintQuery.getDocumentNode(blueprint, node))
		);
		const schemaExperience = documentsManager.getSchemaExperience(documentId);
		if (!schemaExperience) {
			throw new Error(
				'Could not find a schema experience for a node. Only run XPaths for loaded documents.',
				node
			);
		}
		metadata = schemaExperience.format.metadata;
	}

	const markupLabel = metadata.get('markup-label', node, blueprint);

	if (typeof markupLabel === 'string' && markupLabel.startsWith('x__')) {
		// This is an XPath, execute it
		const xpathResult = evaluateXPathToString(markupLabel.substr(3), node, blueprint);

		return xpathResult === '' ? null : xpathResult;
	}

	return markupLabel;
}
