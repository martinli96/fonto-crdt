import React, { useEffect, useState } from 'react';

import {
	CompactStateMessage,
	KeyValueList,
	Popover,
	PopoverBody,
	PopoverHeader,
	PopoverFooter
} from 'fds/components';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import dataSourcesManager from '../api/dataSourcesManager.js';

export default function DataSourcePopover({
	data: { contextNodeId, editOperationName, extraOperationName, removeOperationName },
	togglePopover
}) {
	const [dataSource, setDataSource] = useState(null);

	useEffect(() => {
		const dataSourceId = evaluateXPathToString(
			'@data-data-source-id',
			documentsManager.getNodeById(contextNodeId),
			readOnlyBlueprint
		);
		setDataSource(dataSourcesManager.getDataSource(dataSourceId) || null);
	}, []);

	return (
		<Popover minWidth="300px" maxWidth="400px">
			<PopoverHeader
				icon={(dataSource && dataSource.type) || null}
				title={(dataSource && dataSource.name) || 'Data source'}
			/>

			<PopoverBody>
				{!dataSource && (
					<CompactStateMessage
						connotation="warning"
						message="The data source could not be found. Make sure the correct definition document is linked."
						visual="exclamation-triangle"
					/>
				)}
				{!!dataSource && (
					<KeyValueList
						valueByKey={{
							'URI template': dataSource.uriTemplate || ''
						}}
					/>
				)}
			</PopoverBody>

			{editOperationName && (
				<PopoverFooter>
					{removeOperationName && (
						<FxOperationButton
							onClick={togglePopover}
							icon=""
							label="Remove"
							operationName={removeOperationName}
							operationData={{
								contextNodeId
							}}
						/>
					)}

					{extraOperationName && (
						<FxOperationButton
							onClick={togglePopover}
							icon=""
							operationName={extraOperationName}
							operationData={{
								contextNodeId
							}}
						/>
					)}

					<FxOperationButton
						onClick={togglePopover}
						icon=""
						operationName={editOperationName}
						operationData={{
							contextNodeId
						}}
						type="primary"
					/>
				</PopoverFooter>
			)}
		</Popover>
	);
}
