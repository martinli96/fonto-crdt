import React, { useEffect, useState } from 'react';

import {
	Button,
	Card,
	Flex,
	Icon,
	KeyValueList,
	Label,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	StateMessage,
	VirtualList
} from 'fds/components';

import dataSourcesManager from '../api/dataSourcesManager.js';

const DataSourceBrowseModal = ({
	submitModal,
	cancelModal,
	data: { dataSourceId, modalPrimaryButtonLabel }
}) => {
	const [selectedItemId, setSelectedItemId] = useState(null);
	const [dataSources, setDataSources] = useState([]);

	useEffect(() => {
		const currentDataSources = dataSourcesManager.getDataSources() || [];
		setDataSources(currentDataSources);

		if (dataSourceId && currentDataSources.some(dataSource => dataSource.id === dataSourceId)) {
			setSelectedItemId(dataSourceId);
		}
	}, []);

	const handleSubmit = () => {
		submitModal({ dataSourceId: selectedItemId });
	};

	const handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;
			case 'Enter':
				if (selectedItemId) {
					handleSubmit();
				}
		}
	};

	const handleItemClick = item => setSelectedItemId(item.id);

	const handleItemDoubleClick = item => submitModal({ dataSourceId: item.id });

	const renderListItem = ({ key, item, onClick, onDoubleClick, onRef }) => (
		<Card
			key={key}
			isSelected={item.id === selectedItemId}
			onClick={onClick}
			onDoubleClick={onDoubleClick}
			onRef={onRef}
		>
			<Flex spaceSize="s">
				<Icon icon={item.type} />
				<Label isBold tooltipContent={item.name}>
					{item.name}
				</Label>
			</Flex>
			<KeyValueList
				valueByKey={{
					'URI template': item.uriTemplate
				}}
			/>
		</Card>
	);

	return (
		<Modal size="s" isFullHeight onKeyDown={handleKeyDown}>
			<ModalHeader title="Select a data source" />

			<ModalBody>
				<ModalContent flexDirection="column">
					{dataSources.length === 0 && (
						<StateMessage
							visual="meh-o"
							title="Missing data sources"
							message="There wheren't any data sources found in the definition file or there isn't a definition file attached."
						/>
					)}
					{dataSources.length > 0 && (
						<VirtualList
							estimatedItemHeight={30}
							items={dataSources}
							onItemClick={handleItemClick}
							onItemDoubleClick={handleItemDoubleClick}
							paddingSize="m"
							renderItem={renderListItem}
							idToScrollIntoView={selectedItemId}
							spaceVerticalSize="s"
						/>
					)}
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label="Cancel" onClick={cancelModal} />

				<Button
					isDisabled={!selectedItemId}
					label={modalPrimaryButtonLabel || 'Insert'}
					onClick={handleSubmit}
					type="primary"
				/>
			</ModalFooter>
		</Modal>
	);
};

export default DataSourceBrowseModal;
