import React, { useState, useCallback } from 'react';

import {
	Button,
	Flex,
	Form,
	FormRow,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	SingleSelect,
	Text,
	TextArea,
	TextInput
} from 'fds/components';

function hasInvalidValues(keys, valueByName) {
	return keys.some(key => !valueByName[key]);
}

const formFieldNames = ['name', 'type', 'uriTemplate'];

const typeOptions = [
	{
		label: 'Table',
		value: 'table'
	},
	{
		label: 'Image',
		value: 'image'
	}
];

const DataSourceFormModal = ({
	submitModal,
	cancelModal,
	data: { modalPrimaryButtonLabel, type, name, uriTemplate }
}) => {
	const [showFeedback, setShowFeedback] = useState(false);
	const [valueByName, setValueByName] = useState({
		type: type || null,
		name: name || null,
		uriTemplate: uriTemplate || null
	});

	const handleSubmit = () => {
		if (showFeedback) {
			return;
		}

		if (hasInvalidValues(formFieldNames, valueByName)) {
			setShowFeedback(true);
			return;
		}

		submitModal(valueByName);
	};

	const handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;
			case 'Enter':
				if (showFeedback) {
					handleSubmit();
				}
		}
	};

	const handleFormChange = useCallback(
		({ valueByName: newValueByName }) => {
			setValueByName(newValueByName);
			if (showFeedback) {
				setShowFeedback(hasInvalidValues(formFieldNames, newValueByName));
			}
		},
		[showFeedback]
	);

	return (
		<Modal size="s" onKeyDown={handleKeyDown}>
			<ModalHeader title="Define data source" />

			<ModalBody>
				<ModalContent flexDirection="column" paddingSize="l">
					<Form
						labelPosition="before"
						onChange={handleFormChange}
						valueByName={valueByName}
					>
						<FormRow label="Name">
							<TextInput name="name" />
						</FormRow>

						<FormRow label="Type">
							<SingleSelect name="type" items={typeOptions} />
						</FormRow>

						<FormRow label="URI template">
							<TextArea rows={{ minimum: 1, maximum: 5 }} name="uriTemplate" />
						</FormRow>
					</Form>
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label="Cancel" onClick={cancelModal} />
				<Flex flexDirection="row" alignItems="center" spaceSize="m">
					{showFeedback && (
						<Flex spaceSize="s" alignItems="baseline">
							<Icon icon="times-circle" colorName="icon-s-error-color" />
							<Text colorName="text-error-color">
								Not all fields are filled in yet.
							</Text>
						</Flex>
					)}
					<Button
						isDisabled={showFeedback}
						label={modalPrimaryButtonLabel || 'Insert'}
						onClick={handleSubmit}
						type="primary"
					/>
				</Flex>
			</ModalFooter>
		</Modal>
	);
};

export default DataSourceFormModal;
