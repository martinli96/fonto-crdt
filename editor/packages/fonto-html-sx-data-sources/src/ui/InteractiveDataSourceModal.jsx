import React, { useEffect, useState, useCallback } from 'react';

import {
	Button,
	Flex,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	Text,
	SpinnerIcon,
	StateMessage
} from 'fds/components';

import operationsManager from 'fontoxml-operations/src/operationsManager.js';

import dataSourcesManager from '../api/dataSourcesManager.js';
import getUrlForUriTemplate from '../api/getUrlForUriTemplate.js';

let isMountedInDOM = false;

const overlayStyles = {
	position: 'absolute',
	left: '0',
	right: '0',
	top: '0',
	bottom: '0',
	width: '100%',
	height: '100%',
	margin: 'auto',
	backgroundColor: 'rgba(147, 198, 231, .6)'
};

const InteractiveDataSourceModal = ({
	submitModal,
	cancelModal,
	data: { contextNodeId, dataSourceId, editParameters = {} }
}) => {
	const [dataSource, setDataSource] = useState({});
	const [isSaveable, setIsSaveable] = useState(false);
	const [isLoading, setIsLoading] = useState(true);
	const [isSaving, setIsSaving] = useState(false);
	const [isErrored, setIsErrored] = useState(false);
	const [isInvalid, setIsInvalid] = useState(false);

	const handlePostMessage = useCallback(
		event => {
			// TODO Check origin based on the uri template
			if (!event.data) {
				console.trace('A unknown post messages has been send');
				return;
			}

			if (event.data.action === 'IS-SAVING') {
				setIsErrored(false);
				setIsInvalid(false);
				setIsSaving(true);
				return;
			}
			if (event.data.action === 'TOGGLE-SAVE-BUTTON') {
				if (isSaving) {
					console.error('Can not toggle save button while saving.');
					return;
				}

				if (event.data.canBeSaved === true) {
					setIsSaveable(true);
				} else if (event.data.canBeSaved === false) {
					setIsSaveable(false);
				} else {
					console.error(
						'The toggle save button action should contain parameter "canBeSaved"=true/false.'
					);
				}
				return;
			}
			if (event.data.action === 'SEND-HTML-FRAGMENT') {
				if (event.data.isErrored) {
					console.error(event.data.errorMessage);
					setIsSaving(false);
					setIsSaveable(false);
					setIsErrored(true);
					return;
				}

				const submitData = {
					htmlFragment: event.data.htmlFragment,
					editParameters: event.data.editParameters
				};

				operationsManager
					.getOperationState('_contextual-insert-html-fragment', {
						contextNodeId: contextNodeId,
						...submitData
					})
					.then(operationState => {
						if (!isMountedInDOM) {
							return;
						}

						setIsSaving(false);
						setIsSaveable(false);
						if (!operationState.enabled) {
							setIsInvalid(true);
							return;
						}

						submitModal(submitData);
					})
					.catch(error => {
						if (!isMountedInDOM || !error) {
							return;
						}

						console.error(error);

						setIsSaving(false);
						setIsSaveable(false);
						setIsErrored(true);
					});
				return;
			}

			console.trace('A unknown post messages has been send, with event data: ', event.data);
		},
		[isSaving, dataSource]
	);

	useEffect(() => {
		setDataSource(dataSourcesManager.getDataSource(dataSourceId) || {});

		window.addEventListener('message', handlePostMessage, false);
		isMountedInDOM = true;

		return () => {
			window.removeEventListener('message', handlePostMessage, false);
			isMountedInDOM = false;
		};
	}, [handlePostMessage]);

	const handleSubmit = () => {
		// TODO Set origin based on the uri template
		const iframe = window.frames.DATASOURCE;
		iframe.postMessage(
			{
				action: 'SAVE-HTML-FRAGMENT'
			},
			'*'
		);
		setIsSaving(true);
	};

	const handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;
			case 'Enter':
				if (isSaveable && !isSaving) {
					handleSubmit();
				}
		}
	};

	const handIframeLoad = _event => {
		setIsLoading(false);
	};

	return (
		<Modal size="none" isFullHeight onKeyDown={handleKeyDown}>
			<ModalHeader title={`Retrieve ${dataSource.name ? `"${dataSource.name}"` : 'data'}`} />

			<ModalBody>
				<ModalContent flexDirection="column">
					{(isLoading || isSaving) && (
						<Flex applyCss={overlayStyles} alignItems="center" justifyContent="center">
							<StateMessage
								title={isSaving ? 'Saving…' : 'Loading…'}
								visual={<SpinnerIcon size="l" />}
							/>
						</Flex>
					)}
					{dataSource.uriTemplate && (
						<iframe
							src={getUrlForUriTemplate(
								dataSource.uriTemplate,
								dataSource.parameters,
								editParameters
							)}
							onLoad={handIframeLoad}
							name="DATASOURCE"
							frameBorder="0"
							height="100%"
							width="100%"
						/>
					)}
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label="Cancel" onClick={cancelModal} />

				<Flex flexDirection="row" alignItems="center" spaceSize="m">
					{(isErrored || isInvalid) && (
						<Flex spaceSize="s" alignItems="baseline">
							<Icon icon="times-circle" colorName="icon-s-error-color" />
							<Text colorName="text-error-color">
								{`${
									isErrored
										? 'An error occurred while retrieving the data'
										: 'The send html is not valid and could not be inserted'
								}, please contact your support team or try again later.`}
							</Text>
						</Flex>
					)}

					<Button
						icon={isSaving && 'spinner'}
						isDisabled={!isSaveable || isSaving}
						label={isSaving ? 'Saving…' : 'Save'}
						onClick={handleSubmit}
						type="primary"
					/>
				</Flex>
			</ModalFooter>
		</Modal>
	);
};

export default InteractiveDataSourceModal;
