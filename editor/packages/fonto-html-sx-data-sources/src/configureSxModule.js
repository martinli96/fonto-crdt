import configureAsFrame from 'fontoxml-families/src/configureAsFrame.js';
import configureAsObject from 'fontoxml-families/src/configureAsObject.js';
import configureAsReadOnly from 'fontoxml-families/src/configureAsReadOnly.js';
import configureAsStructure from 'fontoxml-families/src/configureAsStructure.js';
import configureProperties from 'fontoxml-families/src/configureProperties.js';
import createIconWidget from 'fontoxml-families/src/createIconWidget.js';
import createLabelQueryWidget from 'fontoxml-families/src/createLabelQueryWidget.js';
import createLabelWidget from 'fontoxml-families/src/createLabelWidget.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import dataSourceItemJsonMl from './api/dataSourceItemJsonMl.js';
import dataSourcePlaceholderJsonMl from './api/dataSourcePlaceholderJsonMl.js';

export default function configureSxModule(sxModule) {
	// data source in definition file
	configureAsObject(sxModule, 'self::data[@data-is-data-source = "true"]', 'data source', {
		contextualOperations: [
			{
				name: 'contextual-edit-data-source--definition',
				hideIn: []
			},
			{
				name: 'contextual-remove-data-source--definition',
				hideIn: []
			}
		],
		outputClass: undefined,
		blockHeaderLeft: undefined,
		createInnerJsonMl: dataSourceItemJsonMl
	});
	// first data source
	configureProperties(
		sxModule,
		'self::data[@data-is-data-source = "true"][not(preceding-sibling::data[@data-is-data-source = "true"])]',
		{
			blockHeaderLeft: [
				createLabelQueryWidget('""', {
					prefixQuery: '"data sources"'
				})
			]
		}
	);

	// data source placeholder
	configureAsObject(sxModule, 'self::div[@data-data-source-id]', 'data source placeholder', {
		contextualOperations: [
			{
				name: 'contextual-replace-data-source-placeholder',
				hideIn: ['context-menu']
			},
			{
				name: 'contextual-remove-data-source--placeholder',
				hideIn: ['context-menu']
			}
		],
		outputClass: undefined,
		blockHeaderLeft: undefined,
		createInnerJsonMl: dataSourcePlaceholderJsonMl
	});
	configureProperties(
		sxModule,
		'self::div[wws:is-in-mode("author") and @data-data-source-id and not(child::div[@data-is-html-fragment = "true"])]',
		{
			contextualOperations: [
				{
					name: 'contextual-remove-data-source--placeholder',
					hideIn: ['context-menu']
				}
			]
		}
	);

	// data source container
	configureAsFrame(
		sxModule,
		'self::div[@data-data-source-id][child::div[@data-is-html-fragment = "true"]]',
		'data source',
		{
			contextualOperations: [
				{
					name: 'contextual-edit-data-source',
					hideIn: ['context-menu']
				},
				{
					name: 'contextual-make-editable-data-source',
					hideIn: ['context-menu']
				},
				{
					name: 'contextual-remove-data-source',
					hideIn: ['context-menu']
				}
			],
			outputClass: undefined,
			backgroundColor: 'grey',
			blockHeaderLeft: [
				function(sourceNode, renderer) {
					const dataSourceId = evaluateXPathToString(
						'./@data-data-source-id',
						sourceNode,
						readOnlyBlueprint
					);
					const isEditable = evaluateXPathToBoolean(
						'not(@data-is-read-only = "true")',
						sourceNode,
						readOnlyBlueprint
					);
					const dataSource = sourceNode.getExternalValue(
						'data-source-by-id|' + dataSourceId
					);
					return createLabelWidget(
						(dataSource && dataSource.name ? dataSource.name : 'data source') +
							(isEditable ? ' - edited' : '')
					)(sourceNode, renderer);
				}
			],
			blockHeaderRight: [
				createIconWidget('fas fa-info-square', {
					popoverData: {
						extraOperationName: 'contextual-make-editable-data-source',
						editOperationName: 'contextual-edit-data-source'
					},
					clickPopoverComponentName: 'DataSourcePopover'
				})
			]
		}
	);
	configureProperties(
		sxModule,
		'self::div[@data-data-source-id and not(@data-is-read-only="true")]',
		{
			backgroundColor: 'white'
		}
	);

	// html-fragment
	configureAsStructure(sxModule, 'self::div[@data-is-html-fragment = "true"]', 'html fragment', {
		outputClass: undefined
	});
	configureAsReadOnly(
		sxModule,
		'self::div[@data-is-html-fragment = "true" and parent::div[@data-data-source-id and @data-is-read-only="true"]]'
	);
}
