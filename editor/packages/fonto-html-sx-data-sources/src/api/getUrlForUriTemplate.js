export default function getUrlForUriTemplate(uriTemplate, parameters = {}, editParameters = {}) {
	// TODO add logic for Uri templates
	if (Object.entries(editParameters).length === 0) {
		return uriTemplate;
	}
	return (
		uriTemplate +
		'?' +
		Object.entries(editParameters)
			.map(kv => kv.map(encodeURIComponent).join('='))
			.join('&')
	);
}
