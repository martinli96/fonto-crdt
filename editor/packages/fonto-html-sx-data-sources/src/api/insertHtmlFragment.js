import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';
import parseXmlDocument from 'fontoxml-dom-utils/src/parseXmlDocument.js';

const insertHtmlFragment = (argument, blueprint) => {
	const contextNode = blueprint.lookup(argument.contextNodeId);
	if (
		!contextNode ||
		!blueprintQuery.isInDocument(blueprint, contextNode) ||
		(!argument.htmlFragment && !argument.getState)
	) {
		return CustomMutationResult.notAllowed();
	}

	if (!evaluateXPathToBoolean('@data-is-read-only = "true"', contextNode, blueprint)) {
		blueprint.setAttribute(contextNode, 'data-is-read-only', 'true');
	}

	let editParametersNode = evaluateXPathToFirstNode(
		'./data[@data-name="edit-parameters"]',
		contextNode,
		blueprint
	);

	if (!editParametersNode) {
		editParametersNode = namespaceManager.createElement(contextNode, 'data');
		blueprint.setAttribute(editParametersNode, 'data-name', 'edit-parameters');
		blueprint.appendChild(contextNode, editParametersNode);
	} else {
		blueprint.getChildNodes(editParametersNode).forEach(childNode => {
			blueprint.removeChild(editParametersNode, childNode);
		});
	}

	if (argument.editParameters) {
		Object.keys(argument.editParameters).forEach(key => {
			const parameterNode = namespaceManager.createElement(contextNode, 'data');
			blueprint.setAttribute(parameterNode, 'data-name', key);
			blueprint.setAttribute(parameterNode, 'value', argument.editParameters[key].toString());
			blueprint.appendChild(editParametersNode, parameterNode);
		});
	}

	let htmlFragmentContainerNode = evaluateXPathToFirstNode(
		'./div[@data-is-html-fragment = "true"]',
		contextNode,
		blueprint
	);

	if (htmlFragmentContainerNode) {
		blueprint.removeChild(contextNode, htmlFragmentContainerNode);
	}

	htmlFragmentContainerNode = namespaceManager.createElement(contextNode, 'div');
	blueprint.setAttribute(htmlFragmentContainerNode, 'data-is-html-fragment', 'true');

	if (argument.getState) {
		blueprint.appendChild(contextNode, htmlFragmentContainerNode);
		return CustomMutationResult.ok();
	}

	const htmlFragmentDocument = parseXmlDocument(argument.htmlFragment);
	const htmlFragmentNode = evaluateXPathToFirstNode(
		'//html-fragment',
		htmlFragmentDocument,
		blueprint
	);

	if (!htmlFragmentNode) {
		return CustomMutationResult.notAllowed();
	}

	blueprint
		.getChildNodes(htmlFragmentNode)
		.map(childNode => blueprint.cloneNode(childNode, true))
		.forEach(clonedChildNode => {
			blueprint.appendChild(htmlFragmentContainerNode, clonedChildNode);
		});

	blueprint.appendChild(contextNode, htmlFragmentContainerNode);

	// TODO track events

	return CustomMutationResult.ok();
};

export default insertHtmlFragment;
