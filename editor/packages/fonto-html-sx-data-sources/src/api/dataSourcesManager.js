import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import externalDataManager from 'fontoxml-templated-views/src/externalDataManager.js';

import hierarchyNodesLoader from 'fonto-html-multi-document/src/api/hierarchyNodesLoader.js';

function mapDataSourceNodes(dataSourceNode) {
	return {
		id: evaluateXPathToString('@value', dataSourceNode, readOnlyBlueprint),
		name: evaluateXPathToString(
			'./data[@data-name="name"]/@value',
			dataSourceNode,
			readOnlyBlueprint
		),
		type: evaluateXPathToString(
			'./data[@data-name="type"]/@value',
			dataSourceNode,
			readOnlyBlueprint
		),
		uriTemplate: evaluateXPathToString(
			'./data[@data-name="uri-template"]/@value',
			dataSourceNode,
			readOnlyBlueprint
		),
		isInteractive: evaluateXPathToBoolean(
			'./data[@data-name="is-interactive" and @value="true"]',
			dataSourceNode,
			readOnlyBlueprint
		),
		parameters: evaluateXPathToNodes(
			'./data[@data-name="parameters"]/data',
			dataSourceNode,
			readOnlyBlueprint
		).reduce((parameters, parameterNode) => {
			const paraName = evaluateXPathToString(
					'./@data-name',
					parameterNode,
					readOnlyBlueprint
				),
				paraValue = evaluateXPathToString('./@value', parameterNode, readOnlyBlueprint);
			parameters[paraName] = paraValue;
			return parameters;
		}, {})
	};
}

// It will not (yet) be possible to override data sources on template level
function DataSourceManager() {
	this.dataSources = [];
	this._removeVariablesChangedCallback = null;
	this._firstHierarchyNodeId = null;

	hierarchyNodesLoader.templateDocumentIdChangedNotifier.addCallback(() => {
		this.dataSources.forEach(dataSource => {
			externalDataManager.setExternalData('data-source-by-id|' + dataSource.id, undefined);
		});
		this.dataSources = [];
		const templateDocumentId = hierarchyNodesLoader.templateDocumentId;
		const templateDocumentNode = templateDocumentId
			? documentsManager.getDocumentNode(templateDocumentId)
			: null;
		if (!templateDocumentNode) {
			return;
		}
		this.dataSources = evaluateXPathToNodes(
			'//html/head/template[1]/data[@data-is-data-source = "true"]',
			templateDocumentNode,
			readOnlyBlueprint
		).map(mapDataSourceNodes);

		this.dataSources.forEach(dataSource => {
			externalDataManager.setExternalData('data-source-by-id|' + dataSource.id, dataSource);
		});
	});
}

DataSourceManager.prototype.getDataSource = function(dataSourceId) {
	return this.dataSources.find(dataSource => dataSource.id === dataSourceId);
};

DataSourceManager.prototype.getDataSources = function() {
	return this.dataSources;
};

export default new DataSourceManager();
