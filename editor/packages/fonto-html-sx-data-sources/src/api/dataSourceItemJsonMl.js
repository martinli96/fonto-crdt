import { select } from 'glamor';

import createIconWidget from 'fontoxml-families/src/createIconWidget.js';
import createLabelQueryWidget from 'fontoxml-families/src/createLabelQueryWidget.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import createFrameJsonMl from 'fontoxml-families/src/createFrameJsonMl.js';

export default function dataSourceItemJsonMl(sourceNode, renderer) {
	const type = evaluateXPathToString(
		'./data[@data-name="type"]/@value',
		sourceNode,
		readOnlyBlueprint
	);

	const innerJsonMl = [
		'cv-content',
		select('> cv-label-query-widget', {
			whiteSpace: 'normal'
		}),
		createLabelQueryWidget('./data[@data-name="uri-template"]/@value', {
			prefixQuery: '"URI template: "'
		})(sourceNode, renderer)
	];

	return createFrameJsonMl(innerJsonMl, sourceNode, renderer, {
		backgroundColor: 'white',
		expression: 'context',
		showWhen: 'always',
		startDelimiter: '',
		endDelimiter: '',
		blockHeaderLeft: [
			type && createIconWidget(type, { isInline: true }),
			createLabelQueryWidget('./data[@data-name="name"]/@value', {
				inline: true,
				prefixQuery: type ? '" "' : undefined
			})
		],
		blockHeaderRight: [
			createIconWidget('edit', {
				clickOperation: 'contextual-edit-data-source--definition',
				isInline: true
			}),
			createIconWidget('times', {
				clickOperation: 'contextual-remove-data-source--definition',
				isInline: true
			})
		]
	});
}
