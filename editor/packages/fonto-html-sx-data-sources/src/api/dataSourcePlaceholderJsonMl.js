import { flex, fontStack, fontSize } from 'fds/system';
import applyCss from 'fontoxml-styles/src/applyCss.js';
import createFrameJsonMl from 'fontoxml-families/src/createFrameJsonMl.js';
import createIconWidget from 'fontoxml-families/src/createIconWidget.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

// Private API
import isEditableDocumentAndView from 'fontoxml-families/src/shared/isEditableDocumentAndView.js';

function createJsonMlWrapper(
	sourceNode,
	renderer,
	innerJsonMl,
	loadingState,
	hasCallToAction = false
) {
	return createFrameJsonMl(
		[
			'cv-content',

			{
				class:
					'placeholder-container' +
					(hasCallToAction ? ' placeholder-with-call-to-action' : ''),

				contenteditable: 'false'
			},
			innerJsonMl
		],
		sourceNode,
		renderer,
		Object.assign(
			{
				backgroundColor: loadingState === 'errored' ? 'red' : 'grey',
				expression: 'context',
				showWhen: 'always',
				startDelimiter: '',
				endDelimiter: ''
			},
			hasCallToAction
				? {}
				: {
						blockHeaderRight: [
							createIconWidget('fas fa-info-square', {
								popoverData: {
									removeOperationName:
										'contextual-remove-data-source--placeholder',
									editOperationName: 'contextual-replace-data-source-placeholder'
								},
								clickPopoverComponentName: 'DataSourcePopover'
							})
						]
				  }
		)
	);
}

function getErrorInnerJsonML(errorMessage) {
	return [
		'div',
		Object.assign(
			{
				'block-selection-change-on-click': 'true',
				'tooltip-content':
					'Please contact your support team or right-click to remove the placeholder.'
			},
			applyCss([
				fontStack('interface'),
				{
					color: '#d32f2f',
					padding: '4px 8px'
				}
			])
		),
		['span', errorMessage]
	];
}

const insertOperationName = 'contextual-insert-data-source';

export default function dataSourcePlaceholderJsonMl(sourceNode, renderer) {
	const isEditable = isEditableDocumentAndView(sourceNode);

	const dataSourceId = evaluateXPathToString(
		'./@data-data-source-id',
		sourceNode,
		readOnlyBlueprint
	);
	const isInAuthorMode = evaluateXPathToBoolean(
		'wws:is-in-mode("author")',
		sourceNode,
		readOnlyBlueprint
	);

	if (!dataSourceId) {
		return createJsonMlWrapper(
			sourceNode,
			renderer,
			getErrorInnerJsonML('The data source identifier is missing.'),
			'errored'
		);
	}

	const dataSourceInfo = sourceNode.getExternalValue('data-source-by-id|' + dataSourceId);

	if (!dataSourceInfo) {
		return createJsonMlWrapper(
			sourceNode,
			renderer,
			getErrorInnerJsonML(
				'The data source could not be found. Make sure the correct definition document is linked.'
			),
			'errored'
		);
	}

	const innerJsonMl = [
		'div',
		applyCss([flex('column'), { flex: '1', alignItems: 'center', justifyContent: 'center' }])
	];

	const hasCallToAction = dataSourceInfo.isInteractive && isInAuthorMode;

	if (hasCallToAction) {
		innerJsonMl.push([
			'div',
			applyCss({ flex: '1', marginLeft: 'auto' }),
			createIconWidget('fas fa-info-square', {
				clickPopoverComponentName: 'DataSourcePopover'
			})(sourceNode, renderer)
		]);
	}

	if (dataSourceInfo.type) {
		innerJsonMl.push([
			'cv-icon-widget',
			applyCss({
				marginTop: `calc(1rem - (0.875rem * 2)${hasCallToAction ? '' : ' + 9.14288px'})`,
				lineHeight: '1',
				fontSize: '3rem'
			}),
			[
				'cv-icon',
				{
					style: 'line-height: 1;',
					class: 'far fa-' + dataSourceInfo.type
				}
			]
		]);
	}

	innerJsonMl.push([
		'cv-label-widget',
		{
			'style': hasCallToAction ? '' : 'margin-bottom: .625rem;',
			'cv-font-stack': 'interface',
			'variation': ''
		},
		dataSourceInfo.name
	]);

	if (hasCallToAction) {
		const clickOperation = isEditable
			? {
					'operation-name': insertOperationName,
					'operation-context-node-id': getNodeId(sourceNode)
			  }
			: {};

		innerJsonMl.push([
			'div',
			Object.assign(
				{
					'class':
						'placeholder-button' + (isEditable ? '' : ' placeholder-button-disabled'),
					'block-selection-change-on-click': true
				},
				clickOperation
			),
			['span', applyCss([fontStack('interface'), fontSize('m')]), 'Insert']
		]);
	}

	return createJsonMlWrapper(sourceNode, renderer, innerJsonMl, 'done', hasCallToAction);
}
