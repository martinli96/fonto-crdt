import addCustomMutation from 'fontoxml-base-flow/src/addCustomMutation.js';
import addTransform from 'fontoxml-operations/src/addTransform.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import generateUUID from 'fontoxml-uuid-generator/src/generateUUID.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';

import DataSourceBrowseModal from './ui/DataSourceBrowseModal.jsx';
import DataSourceFormModal from './ui/DataSourceFormModal.jsx';
import DataSourcePopover from './ui/DataSourcePopover.jsx';
import insertHtmlFragment from './api/insertHtmlFragment.js';
import InteractiveDataSourceModal from './ui/InteractiveDataSourceModal.jsx';

export default function install() {
	uiManager.registerReactComponent('DataSourceBrowseModal', DataSourceBrowseModal);
	uiManager.registerReactComponent('DataSourceFormModal', DataSourceFormModal);
	uiManager.registerReactComponent('DataSourcePopover', DataSourcePopover);
	uiManager.registerReactComponent('InteractiveDataSourceModal', InteractiveDataSourceModal);

	addCustomMutation('insert-html-fragment', insertHtmlFragment);

	addTransform('generateDataSourceId', function(stepData) {
		stepData.dataSourceId = 'data-source-id-' + generateUUID();
		return stepData;
	});

	addTransform('determineOperationNameForWarningModal', function(stepData) {
		stepData.operationName = 'do-nothing';

		const contextNode = documentsManager.getNodeById(stepData.contextNodeId);
		if (!contextNode) {
			return stepData;
		}

		if (
			!evaluateXPathToBoolean('@data-is-read-only = "true"', contextNode, readOnlyBlueprint)
		) {
			stepData.operationName = 'open-warning-modal';
		}

		return stepData;
	});

	addTransform('retrieveEditParameters', function(stepData) {
		const contextNode = documentsManager.getNodeById(stepData.contextNodeId);
		if (!contextNode) {
			return stepData;
		}
		const editParametersNode = evaluateXPathToFirstNode(
			'./data[@data-name="edit-parameters"]',
			contextNode,
			readOnlyBlueprint
		);

		if (!editParametersNode) {
			return stepData;
		}

		const parameterNodes = evaluateXPathToNodes(
			'./data',
			editParametersNode,
			readOnlyBlueprint
		);

		if (parameterNodes.length === 0) {
			return stepData;
		}

		stepData.editParameters = parameterNodes.reduce((parameters, parameterNode) => {
			const name = evaluateXPathToString('./@data-name', parameterNode, readOnlyBlueprint);
			const value = evaluateXPathToString('./@value', parameterNode, readOnlyBlueprint);
			parameters[name] = value;
			return parameters;
		}, {});

		return stepData;
	});
}
