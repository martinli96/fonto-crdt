import Notifier from 'fontoxml-utils/src/Notifier.js';

import refreshHierarchy from 'fonto-html-multi-document/src/api/refreshHierarchy.js';

function ModeSwitchingManager() {
	this.mode = null;
	this._notifier = new Notifier();
}

ModeSwitchingManager.prototype.addCallback = function(callback) {
	return this._notifier.addCallback(callback);
};

ModeSwitchingManager.prototype.switchMode = function(mode) {
	this.mode = mode;
	this._notifier.executeCallbacks();
	refreshHierarchy().catch(() => {});
};

export default new ModeSwitchingManager();
