export default {
	AUTHOR: 'author',
	COMMENT: 'comment',
	DEFINITION: 'definition',
	TEMPLATE: 'template'
};
