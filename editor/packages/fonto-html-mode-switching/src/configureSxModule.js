import registerCustomXPathFunction from 'fontoxml-selectors/src/registerCustomXPathFunction.js';

import modeSwitchingManager from './api/modeSwitchingManager.js';

export default function configureSxModule(sxModule) {
	registerCustomXPathFunction('wws:is-in-mode', ['xs:string'], 'xs:boolean', function(
		dynamicContext,
		mode
	) {
		const isInMode = mode === modeSwitchingManager.mode;

		if (dynamicContext.domFacade.tryAddExternalDependency) {
			dynamicContext.domFacade.tryAddExternalDependency(addExternalDependency => {
				// eslint-disable-next-line prefer-const
				let removeCallback;

				const invalidateExternalDependency = addExternalDependency(() => removeCallback());

				removeCallback = modeSwitchingManager.addCallback(() => {
					if ((modeSwitchingManager.mode === mode) !== isInMode) {
						invalidateExternalDependency();
					}
				});
			});
		}

		return isInMode;
	});

	registerCustomXPathFunction('wws:get-active-mode', [], 'xs:string?', function(dynamicContext) {
		const mode = modeSwitchingManager.mode;

		if (dynamicContext.domFacade.tryAddExternalDependency) {
			dynamicContext.domFacade.tryAddExternalDependency(addExternalDependency => {
				// eslint-disable-next-line prefer-const
				let removeCallback;

				const invalidateExternalDependency = addExternalDependency(() => removeCallback());

				removeCallback = modeSwitchingManager.addCallback(() => {
					if (modeSwitchingManager.mode !== mode) {
						invalidateExternalDependency();
					}
				});
			});
		}

		return mode;
	});
}
