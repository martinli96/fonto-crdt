import configurationManager from 'fontoxml-configuration/src/configurationManager.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import hideSidebarTab from 'fontoxml-editor/src/hideSidebarTab.js';
import initialDocumentsManager from 'fontoxml-remote-documents/src/initialDocumentsManager.js';
import showSidebarTab from 'fontoxml-editor/src/showSidebarTab.js';

import Mode from './ModeType.js';
import modeSwitchingManager from './api/modeSwitchingManager.js';

export default function install() {
	const reviewEnabled = configurationManager.get('enable-review');

	const determineMode = () => {
		// Determine editor mode
		let newTopLevelDocumentLabel = null;
		const newTopLevelDocumentHierarchyNode = documentsHierarchy.find(function(hierarchyNode) {
			return hierarchyNode.documentReference && hierarchyNode.documentReference.documentId;
		});

		if (!newTopLevelDocumentHierarchyNode) {
			return;
		}

		const newTopLevelDocumentFile = documentsManager.getDocumentFile(
			newTopLevelDocumentHierarchyNode.documentReference.documentId
		);

		if (!newTopLevelDocumentFile) {
			return;
		}

		if (
			newTopLevelDocumentFile.metadata &&
			newTopLevelDocumentFile.metadata.hierarchy &&
			newTopLevelDocumentFile.metadata.hierarchy.length
		) {
			const newTopLevelDocumentHierarchy = newTopLevelDocumentFile.metadata.hierarchy;

			newTopLevelDocumentLabel =
				newTopLevelDocumentHierarchy[newTopLevelDocumentHierarchy.length - 1].label;
		}
		if (newTopLevelDocumentLabel) {
			const splitDocumentId = newTopLevelDocumentLabel.split('.');
			const fontoExtensionIndex =
				splitDocumentId.length - 2 >= 0
					? splitDocumentId.length - 2
					: splitDocumentId.length - 1;
			const fontoExtension = splitDocumentId[fontoExtensionIndex];
			switch (fontoExtension) {
				case 'template':
					modeSwitchingManager.switchMode('template');
					break;
				case 'fragment':
					modeSwitchingManager.switchMode('template');
					break;
				case 'definition':
					modeSwitchingManager.switchMode('definition');
					break;
				case 'document':
					modeSwitchingManager.switchMode('author');
					break;
				case 'css':
					modeSwitchingManager.switchMode('template');
					break;
				case 'html':
					modeSwitchingManager.switchMode('comment');
					break;
				default:
					modeSwitchingManager.switchMode('author');
					break;
			}
		}
	};

	documentsManager.documentsCollectionChangedNotifier.addCallback(determineMode);
	initialDocumentsManager.initialDocumentsLoadedNotifier.addCallback(determineMode);

	function changeSidebar() {
		const newMode = modeSwitchingManager.mode;

		switch (newMode) {
			case Mode.AUTHOR:
				if (!reviewEnabled) {
					hideSidebarTab('review');
				}

				showSidebarTab('output-options');
				showSidebarTab('structure');
				showSidebarTab('author-guidances');
				break;

			case Mode.COMMENT:
				hideSidebarTab('output-options');
				hideSidebarTab('structure');
				hideSidebarTab('author-guidances');

				showSidebarTab('review');
				break;

			case Mode.DEFINITION:
				hideSidebarTab('output-options');
				hideSidebarTab('review');
				hideSidebarTab('structure');
				hideSidebarTab('author-guidances');
				break;

			case Mode.TEMPLATE:
				hideSidebarTab('review');
				hideSidebarTab('output-options');

				showSidebarTab('structure');
				showSidebarTab('author-guidances');
				break;
		}
	}

	modeSwitchingManager.addCallback(changeSidebar);
}
