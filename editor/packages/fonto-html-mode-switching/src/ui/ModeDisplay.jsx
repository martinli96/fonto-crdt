import React, { useState, useEffect } from 'react';

import { Flex } from 'fds/components';

import modeSwitchingManager from '../api/modeSwitchingManager.js';
import Mode from '../ModeType.js';

const getFriendlyLabelForMode = mode => {
	switch (mode) {
		case Mode.AUTHOR:
			return 'Author mode';
		case Mode.COMMENT:
			return 'Comment mode';
		case Mode.DEFINITION:
			return 'Definition mode';
		case Mode.TEMPLATE:
			return 'Template mode';
		default:
			return '';
	}
};

const getBackgroundColorForMode = mode => {
	switch (mode) {
		case Mode.AUTHOR:
			return '#D9245C';
		case Mode.COMMENT:
			return '#9C8C7D';
		case Mode.DEFINITION:
			return '#6EA387';
		case Mode.TEMPLATE:
			return '#0D024C';
		default:
			return '#FFFFFF';
	}
};

function ModeDisplay() {
	const [currentMode, setCurrentMode] = useState(
		getFriendlyLabelForMode(modeSwitchingManager.mode)
	);

	const [currentBGColor, setCurrentBGColor] = useState(
		getBackgroundColorForMode(modeSwitchingManager.mode)
	);

	useEffect(() => {
		const removeCallback = modeSwitchingManager.addCallback(() => {
			setCurrentMode(getFriendlyLabelForMode(modeSwitchingManager.mode));
			setCurrentBGColor(getBackgroundColorForMode(modeSwitchingManager.mode));
		});

		return () => {
			removeCallback();
		};
	}, []);

	return (
		<Flex
			applyCss={Object.assign(
				{
					marginTop: '0.05rem',
					padding: '0.2rem 0.5rem 0.2rem 0.5rem',
					color: 'white',
					fontFamily: 'Lato, "Helvetica Neue", Helvetica, Arial, sans-serif',
					fontSize: '.875rem'
				},
				currentBGColor ? { backgroundColor: currentBGColor } : {}
			)}
		>
			{currentMode}
		</Flex>
	);
}

export default ModeDisplay;
