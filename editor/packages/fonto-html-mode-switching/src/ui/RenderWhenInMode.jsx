import useXPath from 'fontoxml-fx/src/useXPath.js';

function RenderWhenInMode({ modes, children }) {
	const modeSelectors = modes.map(mode => 'wws:is-in-mode("' + mode + '")');
	const selector = modeSelectors.join(' or ');
	const getMode = useXPath(selector, null);

	return getMode ? children : null;
}

export default RenderWhenInMode;
