import React, { useState } from 'react';

import { Block, Button, Drop, DropButton, Flex, RadioButtonGroup } from 'fds/components';

import modeSwitchingManager from '../api/modeSwitchingManager.js';

function renderButton({ isDropOpened, toggleDrop }, currentMode) {
	let label = 'Unknown mode';
	switch (currentMode) {
		case 'definition':
			label = 'Definition mode';
			break;
		case 'template':
			label = 'Template mode';
			break;
		case 'author':
			label = 'Author mode';
			break;
		default:
			label = 'Unknown mode';
			break;
	}
	return (
		<Button
			onClick={toggleDrop}
			label={label}
			iconAfter={isDropOpened ? 'angle-up' : 'angle-down'}
			isSelected={isDropOpened}
		/>
	);
}

function renderDrop(value, onChange) {
	return (
		<Drop>
			<Block applyCss={{ margin: '0.25rem' }}>
				<RadioButtonGroup
					items={[
						{ value: 'definition', label: 'Definition mode' },
						{ value: 'template', label: 'Template mode' },
						{ value: 'author', label: 'Author mode' }
					]}
					onChange={onChange}
					value={value}
				/>
			</Block>
		</Drop>
	);
}

function ModeSwitchingButton() {
	const [currentMode, setCurrentMode] = useState(modeSwitchingManager.mode);

	const changeMode = mode => {
		modeSwitchingManager.switchMode(mode);
		setCurrentMode(mode);
	};

	return (
		<Flex flexDirection="row" flex="none">
			<DropButton
				renderButton={({ isDropOpened, toggleDrop }) =>
					renderButton({ isDropOpened, toggleDrop }, currentMode)
				}
				renderDrop={() => renderDrop(currentMode, changeMode)}
			/>
		</Flex>
	);
}

export default ModeSwitchingButton;
