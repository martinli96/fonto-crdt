import PropTypes from 'prop-types';
import React, { Component } from 'react';

import {
	Button,
	Flex,
	FormRow,
	Modal,
	ModalBody,
	ModalFooter,
	ModalHeader,
	NumericStepper
} from 'fds/components';

import t from 'fontoxml-localization/src/t.js';

class ConvertTextToTableModal extends Component {
	static propTypes = {
		cancelModal: PropTypes.func.isRequired,
		data: PropTypes.shape({
			modalPrimaryButtonLabel: PropTypes.string,
			columns: PropTypes.number.isRequired,
			minimumNumberOfCells: PropTypes.number.isRequired,
			rows: PropTypes.number.isRequired
		}),
		submitModal: PropTypes.func.isRequired
	};

	firstNumericStepperRef = null;

	state = {
		columns: this.props.data.columns,
		rows: this.props.data.rows
	};

	submitModal = () =>
		this.props.submitModal({
			columns: this.state.columns,
			rows: this.state.rows
		});

	handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				this.props.cancelModal();
				break;
			case 'Enter':
				this.submitModal();
				break;
		}
	};

	handleColumnStepperChange = columns => {
		const { minimumNumberOfCells } = this.props.data;

		if (columns > minimumNumberOfCells) {
			return;
		}

		const rows = Math.ceil(minimumNumberOfCells / columns);

		this.setState({ columns, rows });
	};

	handleRowStepperChange = rows => {
		const { minimumNumberOfCells } = this.props.data;

		if (rows > minimumNumberOfCells) {
			return;
		}

		const columns = Math.ceil(minimumNumberOfCells / rows);

		this.setState({ columns, rows });
	};

	handleFirstNumericStepperRef = firstNumericStepperRef =>
		(this.firstNumericStepperRef = firstNumericStepperRef);

	render() {
		return (
			<Modal size="s" onKeyDown={this.handleKeyDown}>
				<ModalHeader title={t('Determine number of columns and rows for selected text')} />

				<ModalBody>
					<Flex spaceSize="l">
						<FormRow label="Columns">
							<NumericStepper
								numberOfDecimals={0}
								onChange={this.handleColumnStepperChange}
								ref={this.handleFirstNumericStepperRef}
								value={this.state.columns}
							/>
						</FormRow>
						<FormRow label="Rows">
							<NumericStepper
								numberOfDecimals={0}
								onChange={this.handleRowStepperChange}
								value={this.state.rows}
							/>
						</FormRow>
					</Flex>
				</ModalBody>

				<ModalFooter>
					<Button label="Cancel" onClick={this.props.cancelModal} />

					<Button
						label={this.props.data.modalPrimaryButtonLabel || 'Insert table'}
						onClick={this.submitModal}
						type="primary"
					/>
				</ModalFooter>
			</Modal>
		);
	}

	componentDidMount() {
		this.firstNumericStepperRef.focus();
	}
}

export default ConvertTextToTableModal;
