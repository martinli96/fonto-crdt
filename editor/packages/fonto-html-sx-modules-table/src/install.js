import addCustomMutation from 'fontoxml-base-flow/src/addCustomMutation.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';
import addTransform from 'fontoxml-operations/src/addTransform.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import cursorIsAtTheEndOfTheNode from 'fonto-html-sx-modules/src/api/cursorIsAtTheEndOfTheNode.js';
import cursorIsAtTheStartOfTheNode from 'fonto-html-sx-modules/src/api/cursorIsAtTheStartOfTheNode.js';
import convertTextToTable from './api/convertTextToTable.js';
import ConvertTextToTableModal from './ui/ConvertTextToTableModal.jsx';

export default function install() {
	addCustomMutation('convert-text-to-table', convertTextToTable);
	uiManager.registerReactComponent('ConvertTextToTableModal', ConvertTextToTableModal);

	function filterNodesWithinRange(nodes, startContainer, startOffset, endContainer, endOffset) {
		let startNode = startContainer;
		let endNode = endContainer;
		let startIndex = nodes.indexOf(startNode, 0);
		let endIndex = nodes.indexOf(endNode, 0);

		while (startIndex === -1 && startNode) {
			startNode = startNode.parentElement;
			startIndex = nodes.indexOf(startNode, 0);
		}

		if (
			startIndex !== -1 &&
			cursorIsAtTheEndOfTheNode(startNode, startContainer, startOffset, readOnlyBlueprint)
		) {
			startIndex++;
		}

		if (startIndex === -1) {
			startIndex = startOffset;
		}

		while (endIndex === -1 && endNode) {
			endNode = endNode.parentElement;
			endIndex = nodes.indexOf(endNode, startIndex);
		}

		if (
			endIndex !== -1 &&
			cursorIsAtTheStartOfTheNode(endNode, endContainer, endOffset, readOnlyBlueprint)
		) {
			endIndex--;
		}

		if (endIndex === -1) {
			endIndex = endOffset - 1;
		}

		return nodes.slice(startIndex, endIndex + 1);
	}

	addTransform('countNumberOfNodesInSelection', function countNumberOfNodesInSelection(stepData) {
		let selectionContainer = selectionManager.getSelectedElement();
		let commonAncestorNode = selectionContainer;
		const ancestorParagraphNode = evaluateXPathToFirstNode(
			'ancestor-or-self::p[1]',
			commonAncestorNode,
			readOnlyBlueprint
		);
		let selectedNodes = [];

		if (ancestorParagraphNode) {
			// paragraph is selected with the breadcrumbs
			if (
				commonAncestorNode === ancestorParagraphNode &&
				commonAncestorNode.parentElement === selectionManager.getCommonAncestorContainer()
			) {
				selectedNodes = [ancestorParagraphNode];
				stepData.isSelectedWithBreadCrumbs = true;
			}
			selectionContainer = commonAncestorNode = ancestorParagraphNode.parentElement;
		} else if (
			commonAncestorNode.parentElement === selectionManager.getCommonAncestorContainer()
		) {
			return stepData;
		}

		if (evaluateXPathToBoolean('self::ul or self::ol', commonAncestorNode, readOnlyBlueprint)) {
			commonAncestorNode = commonAncestorNode.parentElement;
			stepData.convertListItems = true;
		}

		// The common ancestor is not a pontential parent for table
		if (
			!evaluateXPathToBoolean(
				'self::body or self::div or self::figure or self::li or self::section',
				commonAncestorNode,
				readOnlyBlueprint
			)
		) {
			return stepData;
		}

		if (selectionManager.isSelectionCollapsed()) {
			if (!ancestorParagraphNode) {
				return stepData;
			}

			selectedNodes = [ancestorParagraphNode];
		} else if (!selectedNodes.length) {
			selectedNodes = filterNodesWithinRange(
				selectionContainer.childNodes,
				selectionManager.getStartContainer(),
				selectionManager.getStartOffset(),
				selectionManager.getEndContainer(),
				selectionManager.getEndOffset()
			);
			const testNodeName = stepData.convertListItems ? 'li' : 'p';
			if (
				selectedNodes.some(function(node) {
					return node.nodeName !== testNodeName;
				})
			) {
				return stepData;
			}
		}

		if (!selectedNodes.length) {
			return stepData;
		}

		stepData.commonAncestorNodeId = getNodeId(commonAncestorNode);
		stepData.selectedNodeIds = selectedNodes.map(function(node) {
			return getNodeId(node);
		});

		stepData.rows = selectedNodes.length;
		stepData.minimumNumberOfCells = selectedNodes.length;
		stepData.columns = 1;

		return stepData;
	});
}
