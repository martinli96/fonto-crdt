import configureAsTitleFrame from 'fontoxml-families/src/configureAsTitleFrame.js';
import configureAsXhtmlTableElements from 'fontoxml-table-flow-xhtml/src/configureAsXhtmlTableElements.js';
import configureProperties from 'fontoxml-families/src/configureProperties.js';
import createElementMenuButtonWidget from 'fontoxml-families/src/createElementMenuButtonWidget.js';
import createMarkupLabelWidget from 'fontoxml-families/src/createMarkupLabelWidget.js';
import createSemanticBlockLabelWidget from 'fonto-html-semantics/src/createSemanticBlockLabelWidget.js';
import t from 'fontoxml-localization/src/t.js';

export default function configureSxModule(sxModule) {
	configureAsXhtmlTableElements(sxModule, {
		td: {
			defaultTextContainer: 'p'
		},
		th: {
			defaultTextContainer: 'p'
		},
		table: {
			tableFilterSelector: 'not(@data-conref)'
		}
	});

	// caption
	configureAsTitleFrame(sxModule, 'self::caption', 'caption', {
		priority: 2,
		fontVariation: 'figure-title',
		emptyElementPlaceholderText: t('type the caption')
	});

	// col
	configureProperties(sxModule, 'self::col', {
		markupLabel: 'column'
	});

	// colgroup
	configureProperties(sxModule, 'self::colgroup', {
		markupLabel: 'column group'
	});

	// table
	configureProperties(sxModule, 'self::table', {
		contextualOperations: [
			{
				name: 'semantics::set-element-type',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-set-output-options',
				hideIn: ['context-menu']
			},
			{ name: 'xhtml-table-caption-insert', hideIn: ['context-menu'] },
			{ name: 'table-delete', hideIn: ['context-menu'] }
		],
		markupLabel:
			'x__if (@class) then if (contains(@class, ":")) then tokenize(@class, ":")[last()] else @class else "table"',
		tabNavigationItemSelector: 'self::td or self::th',
		titleQuery: './caption',
		blockOutsideAfter: [createElementMenuButtonWidget()],
		blockHeaderLeft: [createSemanticBlockLabelWidget()]
	});

	// table in output
	configureProperties(sxModule, 'self::table[ancestor::html[@data-is-output-document]]', {
		contextualOperations: [],
		blockOutsideAfter: [],
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// tbody
	configureProperties(sxModule, 'self::tbody', {
		markupLabel: 'body'
	});

	// td
	configureProperties(sxModule, 'self::td', {
		markupLabel: 'cell'
	});

	// tfoot
	configureProperties(sxModule, 'self::tfoot', {
		markupLabel: 'footer'
	});

	// th
	configureProperties(sxModule, 'self::th', {
		markupLabel: 'cell'
	});

	// thead
	configureProperties(sxModule, 'self::thead', {
		markupLabel: 'header'
	});

	// tr
	configureProperties(sxModule, 'self::tr', {
		markupLabel: 'row'
	});
}
