import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import cursorIsAtTheStartOfTheNode from 'fonto-html-sx-modules/src/api/cursorIsAtTheStartOfTheNode.js';

function offsetIsAtTheEndOfContainer(container, offset, blueprint) {
	return (
		(domInfo.isElement(container) && offset === blueprint.getChildNodes(container).length) ||
		(domInfo.isTextNode(container) && offset === blueprint.getData(container).length)
	);
}

/*
    New version which does not split if the selection is at the end or start of the container
*/
function unsafeSplitUntil(container, offset, parentOfLastSplit, blueprint) {
	let secondHalf = null;

	while (container !== parentOfLastSplit) {
		if (offset === 0) {
			secondHalf = container;
			offset = blueprintQuery.getParentChildIndex(blueprint, container);
		} else if (offsetIsAtTheEndOfContainer(container, offset, blueprint)) {
			secondHalf = blueprint.getNextSibling(container);
			offset = blueprintQuery.getParentChildIndex(blueprint, container) + 1;
		} else {
			secondHalf = blueprintMutations.unsafeSplitNode(container, offset, blueprint);
			offset = blueprintQuery.getParentChildIndex(blueprint, secondHalf);
		}
		container = blueprint.getParentNode(container);
	}

	return secondHalf;
}

/*
    Wrap one or multiple paragraph in a table each in it's own cell
    Or wrap multiple list items in a table each item in it's own cell
*/

export default function convertTextToTable(argument, blueprint, _format, selection) {
	if (
		!argument.rows ||
		!argument.columns ||
		!argument.selectedNodeIds ||
		!argument.selectedNodeIds.length
	) {
		return CustomMutationResult.notAllowed();
	}

	const nodesAreListItems = argument.convertListItems;

	let commonAncestorNode = blueprint.lookup(argument.commonAncestorNodeId);
	if (!commonAncestorNode || !blueprintQuery.isInDocument(blueprint, commonAncestorNode)) {
		return CustomMutationResult.notAllowed();
	}

	const selectedNodes = argument.selectedNodeIds.map(function(nodeId) {
		return blueprint.lookup(nodeId);
	});

	const totalSelectedNodes = selectedNodes.length;
	let commonAncestorIsListNode = false;
	let listNode = nodesAreListItems ? selectedNodes[0].parentElement : null;
	let secondHalf = null;

	if (nodesAreListItems) {
		const cursorIsAtStartOfList = cursorIsAtTheStartOfTheNode(
			listNode,
			selection.startContainer,
			selection.startOffset,
			blueprint
		);
		if (commonAncestorNode.nodeName === 'li' && !cursorIsAtStartOfList) {
			commonAncestorNode = listNode;
			commonAncestorIsListNode = true;
		}
	}

	if (!selection.collapsed && !argument.isSelectedWithBreadCrumbs) {
		secondHalf = unsafeSplitUntil(
			selection.startContainer,
			selection.startOffset,
			commonAncestorNode,
			blueprint
		);
		if (secondHalf !== null) {
			selectedNodes[0] = secondHalf;
		}

		// The second half is a list node
		if (nodesAreListItems && !commonAncestorIsListNode) {
			listNode = secondHalf;
			selectedNodes[0] = blueprint.getFirstChild(secondHalf);
		}

		unsafeSplitUntil(
			selection.endContainer,
			selection.endOffset,
			commonAncestorNode,
			blueprint
		);
	}

	if (
		nodesAreListItems &&
		selectedNodes.some(function(node) {
			return evaluateXPathToBoolean('child::table', node, blueprint);
		})
	) {
		return CustomMutationResult.notAllowed();
	}

	// new List node should be unwrapped
	if (listNode && !commonAncestorIsListNode) {
		blueprintMutations.unsafeCollapseElement(commonAncestorNode, listNode, blueprint);
	}

	const newParentNode = commonAncestorIsListNode
		? evaluateXPathToFirstNode('preceding-sibling::li[1]', selectedNodes[0], blueprint)
		: commonAncestorNode;

	let insertedSelectedNodesCounter = 0;

	let tableNode = namespaceManager.createElement(newParentNode, 'table');

	tableNode = blueprint.insertBefore(
		newParentNode,
		tableNode,
		commonAncestorIsListNode ? null : selectedNodes[0]
	);

	let i, j;
	for (i = 0; i < argument.rows; ++i) {
		let rowElement = namespaceManager.createElement(tableNode, 'tr');
		rowElement = blueprint.appendChild(tableNode, rowElement);
		for (j = 0; j < argument.columns; ++j) {
			let cellElement = namespaceManager.createElement(tableNode, 'td');
			cellElement = blueprint.appendChild(rowElement, cellElement);
			if (insertedSelectedNodesCounter < totalSelectedNodes) {
				const selectedNode = selectedNodes[insertedSelectedNodesCounter];
				blueprintMutations.unsafeMoveNodes(
					nodesAreListItems ? blueprint.getFirstChild(selectedNode) : selectedNode,
					nodesAreListItems ? blueprint.getLastChild(selectedNodes) : selectedNode,
					blueprint,
					cellElement,
					null,
					false
				);

				if (nodesAreListItems) {
					blueprint.removeChild(blueprint.getParentNode(selectedNode), selectedNode);
				}

				insertedSelectedNodesCounter++;
			}
		}
	}

	// Not all paragraph nodes where inserted
	if (insertedSelectedNodesCounter !== totalSelectedNodes) {
		return CustomMutationResult.notAllowed();
	}

	return CustomMutationResult.ok();
}
