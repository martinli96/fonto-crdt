import uiManager from 'fontoxml-modular-ui/src/uiManager.js';
import footNoteSvg from './icons/footnote.svg';
import Masthead from './Masthead.jsx';

export default function install() {
	uiManager.registerReactComponent('Masthead', Masthead);
	uiManager.registerCustomIcon('footnote', footNoteSvg);
}
