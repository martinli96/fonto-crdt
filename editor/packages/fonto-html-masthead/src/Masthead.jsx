import React, { Component } from 'react';

import {
	ButtonWithDrop,
	Drop,
	Flex,
	MastheadToolbarButtons,
	Menu,
	MenuGroup
} from 'fds/components';

import configurationManager from 'fontoxml-configuration/src/configurationManager.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import FxEditorMasthead from 'fontoxml-fx/src/FxEditorMasthead.jsx';
import FxMultiOperationsMenuItem from 'fontoxml-fx/src/FxMultiOperationsMenuItem.jsx';
import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import FxSaveButton from 'fontoxml-fx/src/FxSaveButton.jsx';
import FxXPath, { XPATH_RETURN_TYPES } from 'fontoxml-fx/src/FxXPath.jsx';
import initialDocumentsManager from 'fontoxml-remote-documents/src/initialDocumentsManager.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';

import ModeDisplay from 'fonto-html-mode-switching/src/ui/ModeDisplay.jsx';
import modeSwitchingManager from 'fonto-html-mode-switching/src/api/modeSwitchingManager.js';
import ModeType from 'fonto-html-mode-switching/src/ModeType.js';

import StartToolbar from './toolbars/StartToolbar.jsx';
import TableToolbar from './toolbars/TableToolbar.jsx';
import TemplateToolbar from './toolbars/TemplateToolbar.jsx';
import DocumentToolbar from './toolbars/DocumentToolbar.jsx';
import OutputOptionsToolbar from './toolbars/OutputOptionsToolbar.jsx';

class HtmlMasthead extends Component {
	removeInitialDocumentsLoadedCallback = null;

	state = {
		isDefinitionDocument: false,
		isRootDocument: false,
		isOutputDocument: false
	};

	componentDidMount() {
		this.removeInitialDocumentsLoadedCallback = initialDocumentsManager.initialDocumentsLoadedNotifier.addCallback(
			() => {
				if (documentsManager.getAllDocumentIds({ 'cap/editable': true }).length > 0) {
					const firstHierarchyNode = documentsHierarchy.getFirstVisibleHierarchyNode();
					if (!firstHierarchyNode || !firstHierarchyNode.documentReference) {
						return;
					}
					const globalDocumentNode = documentsManager.getDocumentNode(
						firstHierarchyNode.documentReference.documentId
					);

					const isRootDocument = globalDocumentNode
						? evaluateXPathToBoolean(
								'//html[@data-is-root-document]',
								globalDocumentNode,
								readOnlyBlueprint
						  )
						: false;

					// TODO: Remove this and use a mode manager kind of thing to determine masthead tabs.
					// Other components were already fixed by the <RenderWhenInMode/>
					const isOutputDocument = globalDocumentNode
						? evaluateXPathToBoolean(
								'//html[@data-is-output-document]',
								globalDocumentNode,
								readOnlyBlueprint
						  )
						: false;
					this.setState({ isRootDocument, isOutputDocument });
				}
			}
		);
	}

	getTabs = mode =>
		mode === ModeType.TEMPLATE
			? [
					{
						id: 'start',
						label: t('Start'),
						content: <StartToolbar />
					},
					{
						id: 'template',
						label: t('Template'),
						content: <TemplateToolbar />
					},
					{
						id: 'table',
						label: t('Table'),
						isVisibleTabQuery: 'ancestor-or-self::table',
						isHighlightedTab: true,
						content: <TableToolbar />
					}
			  ]
			: mode === ModeType.AUTHOR
			? [
					{
						id: 'start',
						label: t('Start'),
						content: <StartToolbar />
					},
					{
						id: 'document',
						label: t('Document'),
						content: <DocumentToolbar />
					},
					{
						id: 'table',
						label: t('Table'),
						isVisibleTabQuery: 'ancestor-or-self::table',
						isHighlightedTab: true,
						content: <TableToolbar />
					},
					{
						id: 'output-options',
						label: t('Output'),
						content: <OutputOptionsToolbar />
					}
			  ]
			: [
					{
						id: 'start',
						label: t('Start'),
						content: <StartToolbar />
					},
					{
						id: 'table',
						label: t('Table'),
						isVisibleTabQuery: 'ancestor-or-self::table',
						isHighlightedTab: true,
						content: <TableToolbar />
					}
			  ];

	getQuickAccess = () => {
		const isDebugBuild = configurationManager.get('fonto-debug-build');
		return (
			<Flex flexDirection="row" flex="none">
				<FxOperationButton
					label=""
					operationName="save-and-release-all-locks-and-send-close-signal-to-iframe"
				/>
				<FxOperationButton label="" operationName="undo" />
				<FxOperationButton label="" operationName="redo" />
				<FxOperationButton label="" operationName="convert-range-to-plain-text" />
				{isDebugBuild && (
					<FxOperationButton
						operationName="AUXILIAR-OPEN-DOCUMENT-ONLY-FOR-TESTING-PURPOSES"
						tooltipContent="This button is provisional, just for development purposes"
					/>
				)}

				<FxSaveButton />
			</Flex>
		);
	};

	getRightContent = () => (
		<MastheadToolbarButtons>
			<ButtonWithDrop
				icon="search-plus"
				label="Zoom"
				renderDrop={() => (
					<Drop>
						<Menu>
							<MenuGroup>
								<FxOperationMenuItem operationName="zoom-content-view-to-75%-75%" />
								<FxOperationMenuItem operationName="zoom-content-view-to-100%-100%" />
								<FxOperationMenuItem operationName="zoom-content-view-to-125%-125%" />
								<FxOperationMenuItem operationName="zoom-content-view-to-150%-150%" />
								<FxOperationMenuItem operationName="zoom-content-view-to-200%-200%" />
							</MenuGroup>
							<MenuGroup>
								<FxMultiOperationsMenuItem
									operations={[
										{
											operationName:
												'wide-canvas-content-view-to-150%-text-size-not-150%'
										},
										{
											operationName:
												'untoggle-wide-canvas-content-view-to-150%-text-size-not-150%'
										}
									]}
								/>
							</MenuGroup>
						</Menu>
					</Drop>
				)}
			/>
			{modeSwitchingManager.mode && <ModeDisplay />}
		</MastheadToolbarButtons>
	);

	render() {
		return (
			<FxXPath
				expression="wws:get-active-mode()"
				context={{}}
				returnType={XPATH_RETURN_TYPES.STRING_TYPE}
			>
				{mode => (
					<FxEditorMasthead
						showFontoLogo={false}
						quickAccessButtons={this.getQuickAccess()}
						mastheadAlignRightContent={this.getRightContent()}
						tabs={this.getTabs(mode)}
					/>
				)}
			</FxXPath>
		);
	}

	componentWillUnmount() {
		this.removeInitialDocumentsLoadedCallback();
	}
}

export default HtmlMasthead;
