export default function openDocumentInNewTab(remoteDocumentId) {
	if (!remoteDocumentId) {
		return;
	}

	const scope = '/?scope={"documentIds":["' + remoteDocumentId + '"]}';
	const toOpen = new URL(window.location.origin + scope);
	window.open(toOpen);
}
