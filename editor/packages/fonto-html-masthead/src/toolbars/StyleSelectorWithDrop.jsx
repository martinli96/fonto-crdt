import React, { Component } from 'react';

import { ButtonWithDrop, Drop, Menu } from 'fds/components';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';

import getRefNodeIdReferencingDocument from 'fonto-html-multi-document/src/getRefNodeIdReferencingDocument.js';

const selector = '[self::section or self::document-ref]';

class StyleSelectorWithDrop extends Component {
	timeoutId = null;
	removeSelectionChangeCallback = null;

	state = {
		headingLevels: [1, 2, 3],
		dropLabel: 'Normal text'
	};

	render() {
		return (
			<ButtonWithDrop
				label={this.state.dropLabel}
				renderDrop={() => (
					<Drop>
						<Menu>
							<FxOperationMenuItem operationName="convert-h1-to-p" />

							{this.state.headingLevels.map(headingLevel => (
								<FxOperationMenuItem
									key={headingLevel}
									label={'Heading ' + headingLevel}
									operationName="set-section-level"
									operationData={{ level: headingLevel }}
								/>
							))}
						</Menu>
					</Drop>
				)}
			/>
		);
	}

	determineNumberOfHeadingLevels = () => {
		const selectedNode = selectionManager.getSelectedElement();

		if (!selectedNode) {
			return;
		}

		const styledNode = evaluateXPathToFirstNode(
			'ancestor-or-self::*[self::p or self::h1][1][parent::body or parent::section]',
			selectedNode,
			readOnlyBlueprint
		);

		if (!styledNode) {
			if (this.state.dropLabel !== 'Normal text' || this.state.headingLevels.length !== 3) {
				this.setState({ dropLabel: 'Normal text', headingLevels: [1, 2, 3] });
			}
			return;
		}

		let sectionNode = readOnlyBlueprint.getParentNode(styledNode);
		if (sectionNode.nodeName === 'body') {
			if (
				evaluateXPathToBoolean(
					'parent::html[@data-is-root-document or @data-is-definition-document]',
					sectionNode,
					readOnlyBlueprint
				)
			) {
				if (styledNode.nodeName === 'p') {
					this.setState({ dropLabel: 'Normal text', headingLevels: [1, 2, 3] });
					return;
				}
				this.setState({ dropLabel: 'Title', headingLevels: [1, 2, 3] });
				return;
			}
			const htmlNode = readOnlyBlueprint.getParentNode(sectionNode);
			const refNodeId = getRefNodeIdReferencingDocument(htmlNode);
			sectionNode = documentsManager.getNodeById(refNodeId);
			if (!sectionNode) {
				if (styledNode.nodeName === 'p') {
					this.setState({ dropLabel: 'Normal text', headingLevels: [1, 2, 3] });
					return;
				}
				this.setState({ dropLabel: 'Heading ?', headingLevels: [1, 2, 3] });
				return;
			}
		}

		const newHeadingLevels = [1, 2, 3];
		const currentLevel = evaluateXPathToNumber(
			'count(ancestor-or-self::*' + selector + ')',
			sectionNode,
			readOnlyBlueprint
		);
		let i = 4;

		if (styledNode.nodeName === 'p') {
			while (currentLevel + 1 >= i) {
				newHeadingLevels.push(i);
				i++;
			}

			this.setState({ dropLabel: 'Normal text', headingLevels: newHeadingLevels });
			return;
		}

		let maximumLevel = currentLevel;

		sectionNode = readOnlyBlueprint.getParentNode(styledNode);

		if (sectionNode) {
			const firstPrecedingSection = evaluateXPathToFirstNode(
				'preceding-sibling::*' +
					selector +
					'[1]/descendant-or-self::*' +
					selector +
					'[last()]',
				sectionNode,
				readOnlyBlueprint
			);

			if (firstPrecedingSection) {
				maximumLevel =
					evaluateXPathToNumber(
						'count(ancestor-or-self::*' + selector + ')',
						firstPrecedingSection,
						readOnlyBlueprint
					) + 1;
			}

			while (maximumLevel >= i) {
				newHeadingLevels.push(i);
				i++;
			}
		}

		this.setState({
			dropLabel: 'Heading ' + currentLevel,
			headingLevels: newHeadingLevels
		});
	};

	componentDidMount() {
		this.removeSelectionChangeCallback = selectionManager.selectionChangeNotifier.addCallback(
			() => {
				clearTimeout(this.timeoutId);
				this.timeoutId = setTimeout(this.determineNumberOfHeadingLevels, 300);
			}
		);
		this.determineNumberOfHeadingLevels();
	}

	componentWillUnmount() {
		clearTimeout(this.timeoutId);
		this.removeSelectionChangeCallback();
	}
}

export default StyleSelectorWithDrop;
