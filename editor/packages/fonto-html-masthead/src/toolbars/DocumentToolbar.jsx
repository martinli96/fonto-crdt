import React from 'react';

import { ButtonWithDrop, Drop, MastheadToolbar, MastheadToolbarButtons } from 'fds/components';

import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import t from 'fontoxml-localization/src/t.js';

import InsertVariableMenu from './InsertVariableMenu.jsx';

function DocumentToolbar() {
	return (
		<MastheadToolbar>
			<MastheadToolbarButtons>
				<FxOperationButton operationName="edit-variables" />

				<ButtonWithDrop
					label={t('Insert variable')}
					icon="far fa-brackets-curly"
					renderDrop={() => (
						<Drop>
							<InsertVariableMenu />
						</Drop>
					)}
				/>
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				<FxOperationButton
					label={t('Include text from...')}
					operationName="conref-insert"
				/>
			</MastheadToolbarButtons>
		</MastheadToolbar>
	);
}

export default DocumentToolbar;
