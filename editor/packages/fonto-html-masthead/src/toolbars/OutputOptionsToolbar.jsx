import React, { useState, useEffect, useCallback } from 'react';

import {
	ButtonWithDrop,
	Drop,
	Flex,
	MastheadToolbar,
	MastheadToolbarButtons,
	Menu,
	MenuGroup,
	RadioButtonGroup
} from 'fds/components';

import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';

import outputOptionsManager from 'fonto-html-sx-modules/src/api/outputOptionsManager.js';
import useDefinitionFile from 'fonto-html-sx-modules/src/ui/useDefinitionFile.jsx';

function getOutputOptions() {
	return outputOptionsManager.getOutputOptions(
		null,
		[{ value: '[ALL]', label: t('ALL') }],
		true,
		false
	);
}

function OutputOptionsToolbar() {
	const [outputOptions, setOutputOptions] = useState(getOutputOptions());
	const [rootDocumentNodeId, setRootDocumentNodeId] = useState(null);
	const definitionFile = useDefinitionFile(rootDocumentNodeId);

	const handleRadioButtonChange = (value, name) => {
		const newValue = value === '[ALL]' ? null : value;
		const newOutputOptions = outputOptions.map(outputOption => {
			if (outputOption.name === name) {
				return { ...outputOption, value: newValue };
			}
			return outputOption;
		});

		outputOptionsManager.setOutputOption(name, newValue);

		setOutputOptions(newOutputOptions);
	};

	useEffect(() => {
		setOutputOptions(getOutputOptions());
	}, [definitionFile]);

	const hierarchyHasChanged = useCallback(() => {
		const firstHierarchyNode = documentsHierarchy.getFirstVisibleHierarchyNode();
		if (!firstHierarchyNode || !firstHierarchyNode.documentReference) {
			if (rootDocumentNodeId) {
				setRootDocumentNodeId(null);
			}
			return;
		}
		const globalDocumentNode = documentsManager.getDocumentNode(
			firstHierarchyNode.documentReference.documentId
		);
		const rootDocumentNode = globalDocumentNode
			? evaluateXPathToFirstNode(
					'//html[@data-is-root-document]',
					globalDocumentNode,
					readOnlyBlueprint
			  )
			: null;

		const newRootDocumentNodeId = rootDocumentNode ? getNodeId(rootDocumentNode) : null;

		if (newRootDocumentNodeId !== rootDocumentNodeId) {
			setRootDocumentNodeId(newRootDocumentNodeId);
		}
	}, [rootDocumentNodeId]);

	useEffect(() => {
		hierarchyHasChanged();
		const removeHierarchyChangeCallback = documentsHierarchy.hierarchyChangedNotifier.addCallback(
			hierarchyHasChanged
		);

		return removeHierarchyChangeCallback;
	}, [hierarchyHasChanged]);

	useEffect(() => {
		outputOptionsManager.openToolbar();

		return function() {
			outputOptionsManager.closeToolbar();
		};
	}, []);

	return (
		<MastheadToolbar>
			<MastheadToolbarButtons>
				<FxOperationButton
					operationName="create-output"
					operationData={{
						outputOptions:
							outputOptions &&
							outputOptions.reduce((outputOptions, outputOption) => {
								if (outputOption.value !== null) {
									outputOptions.push({
										name: outputOption.name,
										value: outputOption.value
									});
								}
								return outputOptions;
							}, [])
					}}
					type="primary"
					label={t('Create output')}
				/>
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				{outputOptions.map((outputOption, index) => (
					<ButtonWithDrop
						key={index}
						label={outputOption.name}
						renderDrop={() => {
							return (
								<Drop>
									<Menu>
										<Flex
											applyCss={{
												marginLeft: '0.5em',
												marginRight: '0.5em'
											}}
											flex="none"
										>
											<MenuGroup>
												<RadioButtonGroup
													items={outputOption.valueItems}
													name={outputOption.name}
													value={
														outputOption.value === null
															? '[ALL]'
															: outputOption.value
													}
													onChange={value => {
														handleRadioButtonChange(
															value,
															outputOption.name
														);
													}}
												/>
											</MenuGroup>
										</Flex>
									</Menu>
								</Drop>
							);
						}}
					/>
				))}
			</MastheadToolbarButtons>
		</MastheadToolbar>
	);
}

export default OutputOptionsToolbar;
