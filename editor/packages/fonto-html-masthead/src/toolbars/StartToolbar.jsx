import React from 'react';

import {
	ButtonWithDrop,
	Drop,
	MastheadToolbar,
	MastheadToolbarButtons,
	Menu,
	MenuItemWithDrop
} from 'fds/components';

import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import FxOperationInsertTableMenu from 'fontoxml-fx/src/FxOperationInsertTableMenu.jsx';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import FxOperationsSplitButtonWithDropMenu from 'fontoxml-fx/src/FxOperationsSplitButtonWithDropMenu.jsx';
import t from 'fontoxml-localization/src/t.js';
import useXPath from 'fontoxml-fx/src/useXPath.js';

import ModeType from 'fonto-html-mode-switching/src/ModeType.js';

import StyleSelectorWithDrop from './StyleSelectorWithDrop.jsx';

function StartToolbar() {
	const mode = useXPath('wws:get-active-mode()', null);

	return (
		<MastheadToolbar>
			<MastheadToolbarButtons>
				<FxOperationButton label="" operationName="cut" />
				<FxOperationButton label="" operationName="copy" />
				<FxOperationButton label="" operationName="paste" />
			</MastheadToolbarButtons>

			{mode === ModeType.DEFINITION && (
				<MastheadToolbarButtons>
					<FxOperationButton operationName="insert-semantic" />
					<FxOperationButton operationName="insert-variable" />
					<FxOperationButton operationName="insert-output-option" />
					<FxOperationButton operationName="insert-data-source--definition" />
				</MastheadToolbarButtons>
			)}

			{(mode === ModeType.TEMPLATE || mode === ModeType.AUTHOR) && (
				<MastheadToolbarButtons>
					<FxOperationButton label="" operationName="outdent-section" />
					<StyleSelectorWithDrop />
					<FxOperationButton label="" operationName="indent-section" />
				</MastheadToolbarButtons>
			)}

			{(mode === ModeType.TEMPLATE || mode === ModeType.AUTHOR) && (
				<MastheadToolbarButtons>
					<FxOperationButton label="" operationName="toggle-span-bold" />
					<FxOperationButton label="" operationName="toggle-span-italic" />
					<FxOperationButton label="" operationName="toggle-span-underline" />
					<FxOperationButton label="" operationName="toggle-span-sup" />
					<FxOperationButton label="" operationName="toggle-span-sub" />
					<FxOperationButton label="" operationName="aside-insert" />
				</MastheadToolbarButtons>
			)}

			<MastheadToolbarButtons>
				{(mode === ModeType.TEMPLATE || mode === ModeType.AUTHOR) && (
					<>
						<FxOperationButton label="" operationName="insert-ol" />
						<FxOperationButton label="" operationName="insert-ul" />
					</>
				)}

				<FxOperationButton
					label=""
					tooltipContent={t('Decrease the indentation level of one or more list items.')}
					operationName="outdent-list-item"
				/>
				<FxOperationButton label="" operationName="indent-list-items" />
			</MastheadToolbarButtons>

			{(mode === ModeType.TEMPLATE || mode === ModeType.AUTHOR) && (
				<MastheadToolbarButtons>
					<ButtonWithDrop
						icon="table"
						renderDrop={() => (
							<Drop>
								<Menu>
									<MenuItemWithDrop
										label={t('Insert table')}
										renderDrop={() => (
											<Drop>
												<FxOperationInsertTableMenu operationName="xhtml-table-insert" />
											</Drop>
										)}
									/>
									<FxOperationMenuItem operationName="convert-text-to-table" />
								</Menu>
							</Drop>
						)}
					/>

					<FxOperationsSplitButtonWithDropMenu
						icon="picture-o"
						label=""
						operations={[
							{ operationName: 'insert-img.figure' },
							{ operationName: 'insert-img' }
						]}
					/>
					<FxOperationButton label="" operationName="insert-div" />
				</MastheadToolbarButtons>
			)}

			<MastheadToolbarButtons>
				<FxOperationButton label="" operationName="default-special-character-insert" />
			</MastheadToolbarButtons>
		</MastheadToolbar>
	);
}

export default StartToolbar;
