import React from 'react';

import { ButtonWithDrop, Drop, MastheadToolbar, MastheadToolbarButtons } from 'fds/components';

import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import t from 'fontoxml-localization/src/t.js';

import InsertVariableMenu from './InsertVariableMenu.jsx';

function TemplateToolbar() {
	return (
		<MastheadToolbar>
			<MastheadToolbarButtons>
				<FxOperationButton label={t('Definition')} operationName="open-semantics-modal" />
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				<ButtonWithDrop
					label={t('Insert variable')}
					icon="far fa-brackets-curly"
					renderDrop={() => (
						<Drop>
							<InsertVariableMenu />
						</Drop>
					)}
				/>
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				<FxOperationButton operationName="insert-author-guidance" />
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				<FxOperationButton
					label={t('Include text from...')}
					operationName="conref-insert"
				/>
			</MastheadToolbarButtons>

			<MastheadToolbarButtons>
				<FxOperationButton operationName="insert-data-source-placeholder" />
			</MastheadToolbarButtons>
		</MastheadToolbar>
	);
}

export default TemplateToolbar;
