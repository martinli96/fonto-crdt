import React from 'react';

import {
	Drop,
	ButtonWithDrop,
	MastheadToolbar,
	MastheadToolbarButtons,
	Menu,
	MenuGroup
} from 'fds/components';

import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';

const TableToolbar = () => (
	<MastheadToolbar>
		<MastheadToolbarButtons>
			<ButtonWithDrop
				label="Row"
				renderDrop={() => (
					<Drop>
						<Menu>
							<FxOperationMenuItem operationName="row-insert" />
							<FxOperationMenuItem operationName="row-after-insert" />
							<FxOperationMenuItem operationName="row-delete" />
						</Menu>
					</Drop>
				)}
			/>

			<ButtonWithDrop
				label="Column"
				renderDrop={() => (
					<Drop>
						<Menu>
							<FxOperationMenuItem operationName="column-insert" />
							<FxOperationMenuItem operationName="column-after-insert" />
							<FxOperationMenuItem operationName="column-delete" />
						</Menu>
					</Drop>
				)}
			/>

			<ButtonWithDrop
				label="Cell"
				renderDrop={() => (
					<Drop>
						<Menu>
							<MenuGroup>
								<FxOperationMenuItem operationName="split-cell-into-rows" />
								<FxOperationMenuItem operationName="split-cell-into-columns" />
							</MenuGroup>

							<MenuGroup>
								<FxOperationMenuItem operationName="merge-cell-right" />
								<FxOperationMenuItem operationName="merge-cell-left" />
								<FxOperationMenuItem operationName="merge-cell-above" />
								<FxOperationMenuItem operationName="merge-cell-below" />
							</MenuGroup>

							<MenuGroup>
								<FxOperationMenuItem operationName="split-cell-completely" />
							</MenuGroup>
						</Menu>
					</Drop>
				)}
			/>
		</MastheadToolbarButtons>

		<MastheadToolbarButtons>
			<ButtonWithDrop
				label="Headers"
				renderDrop={() => (
					<Drop>
						<Menu>
							<FxOperationMenuItem operationName="increase-header-row-count" />
							<FxOperationMenuItem operationName="decrease-header-row-count" />
						</Menu>
					</Drop>
				)}
			/>

			<ButtonWithDrop
				label="Borders"
				renderDrop={() => (
					<Drop>
						<Menu>
							<FxOperationMenuItem operationName="add-table-borders" />
							<FxOperationMenuItem operationName="remove-table-borders" />
						</Menu>
					</Drop>
				)}
			/>

			<ButtonWithDrop
				label="Cell alignment"
				renderDrop={() => (
					<Drop>
						<Menu>
							<MenuGroup>
								<FxOperationMenuItem operationName="xhtml-set-cell-horizontal-alignment-left" />
								<FxOperationMenuItem operationName="xhtml-set-cell-horizontal-alignment-right" />
								<FxOperationMenuItem operationName="xhtml-set-cell-horizontal-alignment-center" />
								<FxOperationMenuItem operationName="xhtml-set-cell-horizontal-alignment-justify" />
							</MenuGroup>

							<MenuGroup>
								<FxOperationMenuItem operationName="xhtml-set-cell-vertical-alignment-top" />
								<FxOperationMenuItem operationName="xhtml-set-cell-vertical-alignment-bottom" />
								<FxOperationMenuItem operationName="xhtml-set-cell-vertical-alignment-center" />
							</MenuGroup>
						</Menu>
					</Drop>
				)}
			/>
		</MastheadToolbarButtons>
	</MastheadToolbar>
);

export default TableToolbar;
