import React, { useState, useEffect } from 'react';

import { Menu, MenuGroup } from 'fds/components';

import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';

import variablesManager from 'fonto-html-sx-modules/src/api/variablesManager.js';

function InsertVariableMenu() {
	const [variables, setVariables] = useState([]);
	useEffect(() => {
		const newVariables = variablesManager.getVariables();

		setVariables(newVariables);
	}, []);

	return (
		<Menu>
			{variables.length > 0 && (
				<MenuGroup>
					{variables.map((variable, index) => (
						<FxOperationMenuItem
							key={index}
							label={`${variable.name} ${
								variable.value === null ? '' : ': ' + variable.value
							}`}
							operationName="insert-inline-variable"
							operationData={{ variable: variable }}
						/>
					))}
				</MenuGroup>
			)}
		</Menu>
	);
}

export default InsertVariableMenu;
