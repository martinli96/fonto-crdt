import configureAsInclusion from 'fontoxml-families/src/configureAsInclusion.js';
import configureAsRemoved from 'fontoxml-families/src/configureAsRemoved.js';
import configureAsStructureViewItem from 'fontoxml-families/src/configureAsStructureViewItem.js';

export default function configureSxModule(sxModule) {
	sxModule.markAsAddon();

	// template-ref
	configureAsRemoved(sxModule, 'self::template-ref', 'definition reference');

	// data-conref
	configureAsInclusion(sxModule, 'self::element()[@data-conref]', 'Reused content', {
		functionName: 'resolve-data-conref',
		namespaceUri: 'http://www.fontoxml.com/functions'
	});

	// document-ref
	configureAsInclusion(
		sxModule,
		'self::document-ref',
		'document reference',
		{
			functionName: 'resolve-document-ref',
			namespaceUri: 'http://www.fontoxml.com/functions'
		},
		{
			contextualOperations: [
				{
					name: 'contextual-insert-document-ref--to-existing-document',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-move-section-up',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-move-section-down',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-indent-section',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-outdent-section',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-document-ref-remove',
					hideIn: ['context-menu', 'element-menu', 'breadcrumbs-menu']
				}
			]
		}
	);

	configureAsStructureViewItem(sxModule, 'self::document-ref', {
		icon: 'file-o',
		titleQuery: `let $source := @src,
			$documentPart := if ($source => contains('#')) then substring-before($source, '#') else $source
			return if (fn:empty($documentPart)) then '' else (if (not(fonto:is-document-loaded($documentPart))) then 'Loading...' else
			concat(wws:get-section-numbering(wws:sectionCounter(fonto:current-hierarchy-node-id(), .), true()), " ", fonto:document($documentPart)/descendant::h1[1]))`,
		recursionQuery: `let $source := @src,
			$documentPart := if ($source => contains('#')) then substring-before($source, '#') else $source
			return if (not(fonto:is-document-loaded($documentPart))) then () else
			fonto:document($documentPart)/html/node()`
	});
}
