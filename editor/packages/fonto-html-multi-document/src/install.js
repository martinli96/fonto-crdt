import addCustomMutation from 'fontoxml-base-flow/src/addCustomMutation.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import addGlobalHook from 'fontoxml-core/src/addGlobalHook.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import addAction from 'fontoxml-operations/src/addAction.js';
import addTransform from 'fontoxml-operations/src/addTransform.js';
import initialDocumentsManager from 'fontoxml-remote-documents/src/initialDocumentsManager.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import addDragAndDropOperation from 'fontoxml-structure/src/addDragAndDropOperation.js';

import convertDocumentRefToSection from './api/convertDocumentRefToSection.js';
import convertSectionToDocumentRef from './api/convertSectionToDocumentRef.js';
import createDocumentFromContent from './api/createDocumentFromContent.js';
import createDocumentFromPreview from './api/createDocumentFromPreview.js';
import createPreviewFromContent from './api/createPreviewFromContent.js';
import createOutput from './api/createOutput.js';
import getRefNodeIdReferencingDocument from './getRefNodeIdReferencingDocument.js';
import HierarchyRefreshHook from './api/HierarchyRefreshHook.js';
import loadInitialDocuments from './api/loadInitialDocuments.js';
import moveHierarchyNodeOrChunkInDom from './api/moveHierarchyNodeOrChunkInDom.js';
import reloadDocument from './api/reloadDocument.js';
import retryLoadingDocumentForHierarchyNode from './api/retryLoadingDocumentForHierarchyNode.js';
import unloadDocuments from './api/unloadDocuments.js';
import hierarchyNodesLoader from './api/hierarchyNodesLoader.js';

export default function install() {
	addGlobalHook(new HierarchyRefreshHook());

	addDragAndDropOperation({ name: 'move-hierarchy-node-or-chunk-in-dom', priority: 5 });

	addCustomMutation('convert-document-ref-to-section', convertDocumentRefToSection);
	addCustomMutation('convert-section-to-document-ref', convertSectionToDocumentRef);
	addCustomMutation('move-hierarchy-node-or-chunk-in-dom', moveHierarchyNodeOrChunkInDom);

	addTransform('getDocumentIdForNodeId', function getDocumentIdForNodeId(stepData) {
		if (stepData.documentId) {
			return stepData;
		}

		if (stepData.contextNodeId) {
			stepData.documentId = documentsManager.getDocumentIdByNodeId(stepData.contextNodeId);
		}

		return stepData;
	});

	addTransform(
		'setHierarchyNodeIdToNewlyInsertedDocumentRef',
		function setHierarchyNodeIdToNewlyInsertedDocumentRef(stepData) {
			const contextNode = documentsManager.getNodeById(stepData.contextNodeId);
			if (!contextNode) {
				return stepData;
			}
			const newDocumentRefNode = evaluateXPathToFirstNode(
				'child::document-ref[last()]',
				contextNode,
				readOnlyBlueprint
			);

			const newDocumentRefNodeId = getNodeId(newDocumentRefNode);

			const documentRefHierarchyNode = documentsHierarchy.find(
				hierarchyNode =>
					hierarchyNode.documentReference &&
					hierarchyNode.documentReference.sourceNodeId === newDocumentRefNodeId
			);

			stepData.hierarchyNodeId = documentRefHierarchyNode.getId();
			// TODO: This does not work, fix when DEV-3710 is fixed
			return stepData;
		}
	);

	addTransform(
		'setContextNodeIdToRefReferencingContextNodeId',
		function setContextNodeIdToRefReferencingContextNodeId(stepData) {
			const contextNode = documentsManager.getNodeById(stepData.contextNodeId);
			if (!contextNode) {
				return stepData;
			}
			if (domInfo.isElement(contextNode, 'section')) {
				return stepData;
			}

			const documentElement = contextNode.ownerDocument.documentElement;
			stepData.contextNodeId = getRefNodeIdReferencingDocument(documentElement);

			return stepData;
		}
	);

	addTransform('setBrowseContextDocumentId', function setBrowseContextDocumentId(stepData) {
		if (!stepData.documentId) {
			return stepData;
		}
		stepData.browseContextDocumentId = documentsManager.getRemoteDocumentId(
			stepData.documentId
		);

		return stepData;
	});

	addTransform(
		'setBrowseContextDocumentIdToDefinitionFile',
		function setBrowseContextDocumentIdToDefinitionFile(stepData) {
			if (hierarchyNodesLoader.templateDocumentId) {
				stepData.browseContextDocumentId = documentsManager.getRemoteDocumentId(
					hierarchyNodesLoader.templateDocumentId
				);
				return stepData;
			}

			if (stepData.rootDocumentId) {
				stepData.browseContextDocumentId = documentsManager.getRemoteDocumentId(
					stepData.rootDocumentId
				);
			}

			return stepData;
		}
	);

	addTransform(
		'createPreviewForOutput',
		async function createPreviewForOutput(stepData) {
			const documentId = await createPreviewFromContent({
				modifyTemplateCallback: () => {
					const htmlNode = documentsManager.getNodeById(stepData.contextNodeId);
					return createOutput(htmlNode, readOnlyBlueprint, {
						outputOptions: stepData.outputOptions
					});
				}
			});
			stepData.documentId = documentId;
			return stepData;
		},
		function createPreviewForOutputState(stepData) {
			if (!stepData.contextNodeId) {
				stepData.operationState = {
					enabled: false
				};
			}

			return stepData;
		}
	);

	addTransform(
		'createDocumentForOutput',
		function createDocumentForOutput(stepData) {
			return createDocumentFromPreview(stepData.documentId, {
				folderId: stepData.folderId,
				modifyTemplateCallback: function(documentNode, blueprint) {
					const styleNode = evaluateXPathToFirstNode(
						'/html/head/style',
						documentNode,
						blueprint
					);
					if (!styleNode) {
						return;
					}

					if (!stepData.cssContent) {
						blueprint.removeChild(blueprint.getParentNode(styleNode), styleNode);
						return;
					}

					blueprint.getChildNodes(styleNode).forEach(childNode => {
						blueprint.removeChild(styleNode, childNode);
					});

					const cssTextNode = styleNode.ownerDocument.createTextNode(stepData.cssContent);
					blueprint.appendChild(styleNode, cssTextNode);
				}
			}).then(function(documentId) {
				stepData.targetRemoteDocumentId = documentsManager.getRemoteDocumentId(documentId);
				stepData.documentId = documentId;

				return stepData;
			});
		},
		function createDocumentForOutputState(stepData) {
			if (!stepData.documentId) {
				stepData.operationState = {
					enabled: false
				};
			}
			stepData.targetRemoteDocumentId = '591792d0-aa99-4a1a-9e34-43cb4f43f179.html';

			return stepData;
		}
	);

	addTransform(
		'createNewFileForChunk',
		function createNewFileForChunk(stepData) {
			const oldSectionNode = documentsManager.getNodeById(stepData.sectionNodeId);
			const fileName = evaluateXPathToString(
				'./h1/text()',
				oldSectionNode,
				readOnlyBlueprint
			);

			return createDocumentFromContent({
				metadata: { fileName, fileExtension: '.fragment.html' },
				folderId: stepData.folderId,
				modifyTemplateCallback: function(documentNode, blueprint) {
					const oldSectionNode = documentsManager.getNodeById(stepData.sectionNodeId);
					const newSectionNode = blueprint.cloneNode(oldSectionNode, true);
					const classAttributeValue = evaluateXPathToString(
						'./@class',
						oldSectionNode,
						blueprint
					);
					const newHtmlNode = evaluateXPathToFirstNode('/html', documentNode, blueprint);
					const newBodyNode = evaluateXPathToFirstNode('./body', newHtmlNode, blueprint);
					const newH1Node = blueprint.cloneNode(
						evaluateXPathToFirstNode('./h1', oldSectionNode, blueprint),
						true
					);

					blueprint.appendChild(newBodyNode, newH1Node);
					blueprint.appendChild(newBodyNode, newSectionNode);

					if (classAttributeValue) {
						blueprint.setAttribute(newHtmlNode, 'class', classAttributeValue);
					}
				}
			}).then(function(documentId) {
				stepData.targetRemoteDocumentId = documentsManager.getRemoteDocumentId(documentId);
				stepData.documentId = documentId;

				return stepData;
			});
		},
		function createNewFileForChunkState(stepData) {
			stepData.targetRemoteDocumentId = '591792d0-aa99-4a1a-9e34-43cb4f43f179.html';

			return stepData;
		}
	);

	addAction('unload-all-documents', _stepData => {
		unloadDocuments();
	});

	initialDocumentsManager.setLoadingStrategy(
		loadInitialDocuments,
		unloadDocuments,
		reloadDocument,
		retryLoadingDocumentForHierarchyNode
	);
}
