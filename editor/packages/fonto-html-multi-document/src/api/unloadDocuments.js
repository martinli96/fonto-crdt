import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';

export default function unloadDocuments() {
	documentsHierarchy.clear();
	documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

	documentsManager.removeAllDocuments();
}
