import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';

function getContextNode(blueprint, ids) {
	return (ids && blueprint.lookup(ids.contextNodeId)) || null;
}

function getSourceNode(blueprint, ids) {
	return (ids && blueprint.lookup(ids.sourceNodeId)) || null;
}

export default function moveHierarchyNodeOrChunkInDom(stepData, blueprint, _format, _selection) {
	// Moving multiple nodes is not yet supported by the UI nor allowed by this custom mutation
	if (stepData.movingNodes.length !== 1) {
		return CustomMutationResult.notAllowed();
	}

	const movingNode =
		getSourceNode(blueprint, stepData.movingNodes[0]) ||
		getContextNode(blueprint, stepData.movingNodes[0]);
	let newParentNode =
		getSourceNode(blueprint, stepData.newParentNode) ||
		getContextNode(blueprint, stepData.newParentNode);

	if (newParentNode && newParentNode.nodeName === 'html') {
		newParentNode = evaluateXPathToFirstNode(
			'self::html[@data-is-root-document]/body',
			newParentNode,
			blueprint
		);
	}

	let dropParentNode =
		getSourceNode(blueprint, stepData.dropPosition.parentNode) ||
		getContextNode(blueprint, stepData.dropPosition.parentNode);

	if (dropParentNode && dropParentNode.nodeName === 'html') {
		dropParentNode = evaluateXPathToFirstNode(
			'self::html[@data-is-root-document]/body',
			dropParentNode,
			blueprint
		);
	}
	let dropReferenceNode =
		getSourceNode(blueprint, stepData.dropPosition.referenceNode) ||
		getContextNode(blueprint, stepData.dropPosition.referenceNode);

	if (
		!newParentNode ||
		!dropParentNode ||
		!movingNode ||
		!blueprintQuery.isInDocument(blueprint, movingNode) ||
		domInfo.isDocument(newParentNode) ||
		movingNode.ownerDocument !== newParentNode.ownerDocument
	) {
		// This operation does not support drag & drop to the root level (which could
		// possibly create a new document?), or between different documents.
		// Additionally, ignore any operation state computation for nodes that are no
		// longer in their document (see warning above).
		return CustomMutationResult.notAllowed();
	}

	if (blueprintQuery.contains(blueprint, movingNode, newParentNode)) {
		// A node should not be inserted as its own descendant
		return CustomMutationResult.notAllowed();
	}

	if (
		dropReferenceNode !== null &&
		blueprint.getParentNode(dropReferenceNode) !== dropParentNode
	) {
		// The drop position reference node and parent node are not directly related, try to
		// find an ancestor of the reference node that is a child of the parent node.
		dropReferenceNode = blueprintQuery.findClosestAncestor(
			blueprint,
			dropReferenceNode,
			function(ancestor) {
				return blueprint.getParentNode(ancestor) === dropParentNode;
			}
		);
		if (dropReferenceNode === null) {
			// No descendant relation at all, block the operation
			return CustomMutationResult.notAllowed();
		}
	}

	// Check if we can traverse from the drop position to the new parent without skipping over
	// following elements or text, and determine the corresponding insertion position for the
	// moved node.
	let referenceNode = dropReferenceNode;
	let movingReferenceNode = null;
	for (
		let ancestor = dropParentNode;
		ancestor !== newParentNode;
		referenceNode = blueprint.getNextSibling(ancestor),
			ancestor = blueprint.getParentNode(ancestor)
	) {
		if (movingReferenceNode) {
			return CustomMutationResult.notAllowed();
		}
		if (referenceNode === movingNode) {
			referenceNode = blueprint.getNextSibling(referenceNode);
		}
		if (referenceNode === null) {
			continue;
		}
		if (
			domInfo.isElement(referenceNode) ||
			domInfo.isTextNode(referenceNode) ||
			evaluateXPathToNumber(
				'count(following-sibling::*) + count(following-sibling::text())',
				referenceNode,
				blueprint
			) > 0
		) {
			movingReferenceNode = referenceNode;
			continue;
		}
	}

	// Dropping a node before itself has no effect
	if (movingNode !== referenceNode) {
		// We can now just move the moving node to the new position
		blueprintMutations.unsafeMoveNodes(
			movingNode,
			movingNode,
			blueprint,
			newParentNode,
			referenceNode,
			false
		);
	}

	// Also split certain nodes if needed
	if (movingReferenceNode) {
		blueprintMutations.unsafeMoveNodes(
			movingReferenceNode,
			null,
			blueprint,
			movingNode,
			null,
			false
		);
	}

	// If we reached this point, the drop was successful. Fonto will validate the result to
	// ensure it is valid according to the schema, and disable the operation otherwise.
	return CustomMutationResult.ok();
}
