import connectorsManager from 'fontoxml-configuration/src/connectorsManager.js';
import createDocumentControllerFromContent from 'fontoxml-documents/src/createDocumentControllerFromContent.js';
import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import serializeNodeToXml from 'fontoxml-dom-utils/src/serializeNodeToXml.js';
const documentConnector = connectorsManager.getConnector('document-connector');

// private api's :
import blueprints from 'fontoxml-blueprints/src/main.js';

const Blueprint = blueprints.Blueprint;

function preprocessDocument(documentId, modifyTemplateCallback) {
	const documentController = documentsManager.getDocumentController(documentId);

	// Allow the callback to modify the document template before sending it to the CMS
	documentController.executeFunction(function executeDocumentModification(commandContext) {
		const blueprint = new Blueprint(commandContext.dom);
		const format = commandContext.format;
		const documentNode = documentController.document.dom.documentNode;

		modifyTemplateCallback.call(undefined, documentNode, blueprint, format);

		// We'll validate later
		blueprint.realize();
	}, {});
}

/**
 * Creates a new document in the CMS based on the given local document and unloads the local template document.
 *
 * @param  {string}  documentId                    Local document ID of the document from which to clone the new one
 * @param  {Object}  [options]
 *                                                 Can be used to modify the document before it is sent to the CMS
 * @param  {Object}  [options.metadata]            The metadata passed to the CMS, see POST /document
 * @param  {string}  [options.referrerDocumentId]  The remote ID of the document providing content in which a new
 *                                                   document is created, see POST /document
 * @param  {string}  [options.folderId]            The folder ID passed to the CMS, see POST /document
 *
 * @return  {Promise}  Returns a promise which resolves to the documentId of the created document
 */
export default function createDocumentFromPreview(documentId, options) {
	if (documentsManager.getRemoteDocumentId(documentId) !== null) {
		throw new Error('The given template document must be a local document');
	}

	// Pre-process the document while it is still in previewable state, as the template may not
	// be valid until after pre-processing
	if (options.modifyTemplateCallback) {
		preprocessDocument(documentId, options.modifyTemplateCallback);
	}

	if (!options) {
		options = {};
	}

	return (
		documentCapabilitiesManager
			// Ensure editable capability level to make sure the document is valid before submitting
			// it to the CMS
			.ensureCapabilityLevel(documentId, 'editable')
			.then(function() {
				const xml = serializeNodeToXml(documentsManager.getDocumentNode(documentId));

				// Remove the document
				documentsManager.removeDocument(documentId);

				// Create the document in the CMS
				return documentConnector
					.createNew(
						xml,
						{
							...options.metadata,
							// TODO: pass this from the options
							fileExtension: '.html',
							fileName: 'output-' + new Date().toISOString()
						},
						options.referrerDocumentId,
						options.folderId
					)
					.then(function loadCreatedDocument(documentFileWithContent) {
						// We cannot reuse the documentController in the previous stage because the CMS may have modified the content
						// therefore (p)reload it in the documentsManager
						return createDocumentControllerFromContent(
							documentFileWithContent.content
						).then(function(documentController) {
							return documentsManager.addDocument(
								documentFileWithContent.documentFile,
								documentController
							);
						});
					});
			})
	);
}
