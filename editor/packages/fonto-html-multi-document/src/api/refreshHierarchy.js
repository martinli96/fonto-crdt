import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import hierarchyNodesLoader from './hierarchyNodesLoader.js';

function getDocumentIdsReferencedInHierarchy(documentsHierarchy) {
	// Build a list of unique document instances currently referenced by the hierarchy
	const referencedDocumentIdsSet = Object.create(null);
	(function collectReferencedDocumentsIds(hierarchyNodes) {
		for (let i = 0, l = hierarchyNodes.length; i < l; ++i) {
			const hierarchyNode = hierarchyNodes[i],
				documentId =
					hierarchyNode.documentReference && hierarchyNode.documentReference.documentId;
			if (documentId) {
				referencedDocumentIdsSet[documentId] = true;
			}
			collectReferencedDocumentsIds(hierarchyNode.children);
		}
	})(documentsHierarchy.children);

	return Object.keys(referencedDocumentIdsSet);
}

export default function refreshHierarchy() {
	// Save the old document ids, so that we can clean up which documents have been removed.
	const documentIdsInOldHierarchy = getDocumentIdsReferencedInHierarchy(documentsHierarchy);

	// Starting from the top-level hierarchy nodes, reload the entire hierarchy
	const oldHierarchyChildren = documentsHierarchy.children.concat();
	documentsHierarchy.clear();
	const documentsLoading = oldHierarchyChildren.map(function(hierarchyNode) {
		return hierarchyNodesLoader.processRootDocument(
			hierarchyNode.documentReference.remoteDocumentId,
			hierarchyNode.documentReference
		);
	});

	return Promise.all(documentsLoading).then(function() {
		// Remove documents which were referenced by the previous hierarchy but not by the new one
		const documentIdsInNewHierarchy = getDocumentIdsReferencedInHierarchy(documentsHierarchy);
		for (let i = 0, l = documentIdsInOldHierarchy.length; i < l; ++i) {
			const documentId = documentIdsInOldHierarchy[i];
			if (!documentIdsInNewHierarchy.includes(documentId)) {
				// Document is no longer referenced, unload it
				documentsManager.removeDocument(documentId);
			}
		}

		// We are done, signal an update to the UI
		documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

		// Resolve to nothing
	});
}
