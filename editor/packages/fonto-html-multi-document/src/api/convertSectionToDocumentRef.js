import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';

export default function convertSectionToDocumentRef(argument, blueprint) {
	const sectionNode = blueprint.lookup(argument.contextNodeId);
	if (!sectionNode || !blueprintQuery.isInDocument(blueprint, sectionNode)) {
		return CustomMutationResult.notAllowed();
	}

	const documentRefNode = namespaceManager.createElement(sectionNode, 'document-ref');
	blueprint.setAttribute(
		documentRefNode,
		'src',
		argument.src + '#' + documentsManager.getNodeById(argument.sectionNodeId).getAttribute('id')
	);

	const dataOnlyForValue = evaluateXPathToString('./@data-only-for', sectionNode, blueprint);
	if (dataOnlyForValue !== '') {
		blueprint.setAttribute(documentRefNode, 'data-only-for', dataOnlyForValue);
	}

	const dataNotForValue = evaluateXPathToString('./@data-not-for', sectionNode, blueprint);
	if (dataNotForValue !== '') {
		blueprint.setAttribute(documentRefNode, 'data-not-for', dataNotForValue);
	}

	const childSectionNodes = evaluateXPathToNodes(
		'./*[self::section or self::document-ref]',
		sectionNode,
		blueprint
	);

	if (childSectionNodes.length) {
		blueprintMutations.unsafeMoveNodes(
			childSectionNodes[0],
			childSectionNodes[childSectionNodes.length - 1],
			blueprint,
			documentRefNode,
			null
		);
	}

	blueprint.replaceChild(blueprint.getParentNode(sectionNode), documentRefNode, sectionNode);

	return CustomMutationResult.ok();
}
