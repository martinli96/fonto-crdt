import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentLoader from 'fontoxml-remote-documents/src/documentLoader.js';
import DocumentReference from 'fontoxml-documents/src/DocumentReference.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import DocumentsHierarchyNode from 'fontoxml-documents/src/DocumentsHierarchyNode.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import Notifier from 'fontoxml-utils/src/Notifier.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import HIERARCHY_NODE_QUERY from './HIERARCHY_NODE_QUERY.js';

function processChildReferences(currentManager, documentId, hierarchyNode) {
	const remoteDocumentId = documentsManager.getRemoteDocumentId(documentId),
		documentNode = documentsManager.getDocumentNode(documentId),
		referencesLoading = [];

	// Now recurse over the nodes of the standard document to find section references and build the hierarchical structure
	// they represent.
	(function findStructureNodes(parentStructureNode, parentHierarchyNode) {
		const structureNodes = evaluateXPathToNodes(
			HIERARCHY_NODE_QUERY,
			parentStructureNode,
			readOnlyBlueprint
		);
		structureNodes.forEach(
			function(structureNode) {
				if (
					evaluateXPathToBoolean('self::template-ref', structureNode, readOnlyBlueprint)
				) {
					const templateId = evaluateXPathToString(
						'@src',
						structureNode,
						readOnlyBlueprint
					);

					referencesLoading.push(
						documentLoader
							.loadDocument(templateId)
							.then(
								function(childDocumentId) {
									currentManager._newTemplateDocumentId = childDocumentId;
									// Set the capability. Locking may cause it to turn editable if the lock is acquired.
									return documentCapabilitiesManager
										.ensureCapabilityLevel(childDocumentId, 'previewable')
										.catch(function(error) {
											// Processing failed, unload with error
											throw error;
										});
								}.bind(this)
							)
							.catch(function(error) {
								// Continue loading other documents when a child document can not be loaded
								console.error(
									'Error loading definition document:\n',
									templateId,
									'\n',
									error
								);
							})
					);
					return;
				}

				const childDocumentReference = new DocumentReference(
					documentId,
					getNodeId(structureNode)
				);

				const isReferenceNode = evaluateXPathToBoolean(
					'self::document-ref',
					structureNode,
					readOnlyBlueprint
				);

				let id;
				if (isReferenceNode) {
					id = evaluateXPathToString('@src', structureNode, readOnlyBlueprint);
					childDocumentReference.setRelatedDocumentTarget(id, remoteDocumentId);
				} else {
					childDocumentReference.setTarget(remoteDocumentId);
					childDocumentReference.setInstance(documentId, getNodeId(structureNode));
				}

				const childHierarchyNode = new DocumentsHierarchyNode(childDocumentReference);
				parentHierarchyNode.addChild(childHierarchyNode);

				if (isReferenceNode) {
					childDocumentReference.setTarget(id);
					referencesLoading.push(
						documentLoader
							.loadDocument(id)
							.then(
								function(childDocumentId) {
									// It is loaded, finish the hierarchy node
									childDocumentReference.setInstance(childDocumentId);
									const childRemoteDocumentId = documentsManager.getRemoteDocumentId(
										childDocumentId
									);
									childDocumentReference.setTarget(childRemoteDocumentId);

									// Set the capability. Locking may cause it to turn editable if the lock is acquired.
									return documentCapabilitiesManager
										.ensureCapabilityLevel(childDocumentId, 'operable')
										.then(function() {
											documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();
											return childDocumentReference.documentId;
										})
										.catch(function(error) {
											// Processing failed, unload with error
											documentsManager.setDocumentError(
												childDocumentId,
												error
											);
											documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();
											throw error;
										});
								}.bind(this)
							)
							.catch(function(error) {
								// Continue loading other documents when a child document can not be loaded
								console.error(
									'Error loading document for section:\n',
									childDocumentReference,
									'\n',
									error
								);
							})
					);
				}

				findStructureNodes(structureNode, childHierarchyNode);
			}.bind(this)
		);
	}.bind(this)(documentNode.documentElement, hierarchyNode));

	documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

	return Promise.all(referencesLoading);
}

function HierarchyNodesLoader() {
	this._newTemplateDocumentId = null;
	this.templateDocumentId = null;
	this.templateDocumentIdChangedNotifier = new Notifier();
}

HierarchyNodesLoader.prototype.processRootDocument = function(
	remoteDocumentId,
	rootDocumentReference
) {
	const rootHierarchyNode = new DocumentsHierarchyNode(rootDocumentReference);
	documentsHierarchy.addChild(rootHierarchyNode);

	this._newTemplateDocumentId = null;

	// Load the document
	return documentLoader.loadDocument(remoteDocumentId).then(
		function(documentId) {
			// It is loaded, finish the hierarchy node
			rootDocumentReference.setInstance(documentId);
			// Trigger the notifier to cause updates in the view.
			documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

			// Set the capability. Locking may cause it to turn editable if the lock is acquired.
			const makeOperablePromise = documentCapabilitiesManager.ensureCapabilityLevel(
				documentId,
				'operable'
			);
			const childrenOperablePromises = processChildReferences(
				this,
				documentId,
				rootHierarchyNode
			);

			return Promise.all([makeOperablePromise, childrenOperablePromises])
				.then(
					function(something) {
						if (this._newTemplateDocumentId !== this.templateDocumentId) {
							this.templateDocumentId = this._newTemplateDocumentId;
							this.templateDocumentIdChangedNotifier.executeCallbacks();
						}
						return something;
					}.bind(this)
				)
				.catch(function(error) {
					// Processing failed, unload with error
					documentsManager.setDocumentError(documentId, error);
					throw error;
				});
		}.bind(this)
	);
};

export default new HierarchyNodesLoader();
