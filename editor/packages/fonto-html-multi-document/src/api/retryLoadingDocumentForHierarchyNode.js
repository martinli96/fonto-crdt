import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentLoader from 'fontoxml-remote-documents/src/documentLoader.js';
import refreshHierarchy from './refreshHierarchy.js';

export default function retryLoadingDocumentForHierarchyNode(hierarchyNode) {
	const documentReference = hierarchyNode.documentReference;
	if (!documentReference || documentReference.documentId) {
		return Promise.resolve();
	}

	// A DocumentReference can either be absolute (remoteDocumentId) or relative (relativeUrl and referrerId),
	// use the appropriate loading method.
	const loadingPromise = documentReference.remoteDocumentId
		? documentLoader.loadDocument(documentReference.remoteDocumentId)
		: documentLoader.loadRelatedDocument(
				documentReference.relativeUrl,
				documentReference.referrerId
		  );

	// Ensure the loading state is shown in the editor
	documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

	return loadingPromise
		.then(function(documentId) {
			// Ensure this document is at least operable. Again, locking may immediately set it to editable if locks allow it.
			return documentCapabilitiesManager.ensureCapabilityLevel(documentId, 'operable');
		})
		.then(
			function(documentId) {
				// Link the hierarchy node to the newly loaded document and signal a change to update the UI
				documentReference.setInstance(documentId);

				return refreshHierarchy();
			},
			function(_error) {
				// The error will be shown in the hierarchy automatically if we trigger an update
				documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();
			}
		);
}
