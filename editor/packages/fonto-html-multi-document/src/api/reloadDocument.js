import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import documentLoader from 'fontoxml-remote-documents/src/documentLoader.js';
import refreshHierarchy from './refreshHierarchy.js';

export default function reloadDocument(documentId) {
	const remoteDocumentId = documentsManager.getRemoteDocumentId(documentId),
		hierarchyNodesForDocument = documentsHierarchy.findAll(function(hierarchyNode) {
			return (
				hierarchyNode.documentReference &&
				hierarchyNode.documentReference.documentId === documentId
			);
		});

	hierarchyNodesForDocument.forEach(function(hierarchyNode) {
		// Unload the hierarchy node
		hierarchyNode.documentReference.setInstance(null);
	});

	// Now remove the existing copy
	documentsManager.removeDocument(documentId);

	const loading = documentLoader
		.loadDocument(remoteDocumentId)
		.catch(function(error) {
			// This causes the content view to show loading errors for all documents which have an error state
			documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

			throw error;
		})
		.then(function(newDocumentId) {
			// It is loaded, finish the hierarchy node
			hierarchyNodesForDocument.forEach(function(hierarchyNode) {
				hierarchyNode.documentReference.setInstance(newDocumentId);
			});

			return refreshHierarchy().then(function() {
				// Resolve with the new document ID of the reloaded document
				// Note that other documents may have been loaded / unloaded as a result of the new
				// hierarchy
				return newDocumentId;
			});
		});

	// Allow views to update
	documentsHierarchy.hierarchyChangedNotifier.executeCallbacks();

	return loading;
}
