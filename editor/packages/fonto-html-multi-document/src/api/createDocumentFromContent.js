import documentsManager from 'fontoxml-documents/src/documentsManager.js';
// private api's :
import createDocumentControllerFromContent from 'fontoxml-documents/src/createDocumentControllerFromContent.js';
import createDocumentFromTemplateDocument from 'fontoxml-document-from-template/src/api/createDocumentFromTemplateDocument.js';

const content =
	'<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:fontoxml:fonto-html.xsd"><head><title> </title></head><body></body></html>';

/**
 * @param  {Object}  [options]
 * @param  {function(Blueprint, Format, dom.Document):void}  [options.modifyTemplateCallback]
 *                                                 Can be used to modify the document before it is sent to the CMS
 * @param  {Object}  [options.metadata]            The metadata passed to the CMS, see POST /document
 * @param  {string}  [options.referrerDocumentId]  The remote ID of the document providing content in which a new
 *                                                   document is created, see POST /document
 * @param  {string}  [options.folderId]            The folder ID passed to the CMS, see POST /document
 *
 * @return  {Promise.<string>}  Returns a promise which resolved to the documentId of the created document.
 */
export default function createDocumentFromContent(options) {
	return createDocumentControllerFromContent(content)
		.then(function(documentController) {
			return documentsManager.addDocument(null, documentController);
		})
		.then(function(documentId) {
			return createDocumentFromTemplateDocument(documentId, options);
		});
}
