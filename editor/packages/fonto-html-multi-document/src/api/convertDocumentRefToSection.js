import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';

export default function convertDocumentRefToSection(argument, blueprint) {
	const documentNode = blueprint.lookup(argument.documentNodeNodeId);
	const documentRefNode = blueprint.lookup(argument.documentRefNodeId);
	if (
		!documentNode ||
		!documentRefNode ||
		!blueprintQuery.isInDocument(blueprint, documentRefNode)
	) {
		return CustomMutationResult.notAllowed();
	}

	const src = documentRefNode.getAttribute('src');
	let sectionId = null;
	if (src.includes('#')) {
		sectionId = src.split('#')[1];
	} else {
		sectionId = src.split('%23')[1];
	}

	const newSectionNode = namespaceManager.createElement(documentRefNode, 'section');
	const sectionNode = evaluateXPathToFirstNode(
		`./body/section[@id="${sectionId}"]`,
		documentNode.documentElement,
		blueprint
	);

	// move the content of the external section into the new section
	const clonedSectionNode = blueprint.cloneNode(sectionNode, true);
	blueprintMutations.unsafeMoveNodes(
		blueprint.getFirstChild(clonedSectionNode),
		blueprint.getLastChild(clonedSectionNode),
		blueprint,
		newSectionNode,
		null
	);

	if (blueprint.getChildNodes(documentRefNode).length > 0) {
		blueprintMutations.unsafeMoveNodes(
			blueprint.getFirstChild(documentRefNode),
			blueprint.getLastChild(documentRefNode),
			blueprint,
			newSectionNode,
			null
		);
	}

	const classAttributeValue = evaluateXPathToString(
		'./@class',
		documentNode.documentElement,
		blueprint
	);
	if (classAttributeValue) {
		blueprint.setAttribute(newSectionNode, 'class', classAttributeValue);
	}

	const dataOnlyForValue = evaluateXPathToString('./@data-only-for', documentRefNode, blueprint);
	if (dataOnlyForValue !== '') {
		blueprint.setAttribute(newSectionNode, 'data-only-for', dataOnlyForValue);
	}

	const dataNotForValue = evaluateXPathToString('./@data-not-for', documentRefNode, blueprint);
	if (dataNotForValue !== '') {
		blueprint.setAttribute(newSectionNode, 'data-not-for', dataNotForValue);
	}

	blueprint.replaceChild(
		blueprint.getParentNode(documentRefNode),
		newSectionNode,
		documentRefNode
	);

	argument.contextNodeId = getNodeId(newSectionNode);

	return CustomMutationResult.ok();
}
