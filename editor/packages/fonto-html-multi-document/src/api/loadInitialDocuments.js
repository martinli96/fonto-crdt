import DocumentReference from 'fontoxml-documents/src/DocumentReference.js';
import hierarchyNodesLoader from './hierarchyNodesLoader.js';

export default function loadInitialDocuments(remoteDocumentIds) {
	return Promise.all(
		remoteDocumentIds.map(function(remoteDocumentId) {
			// Make a document hierarchy node
			const documentReference = new DocumentReference();
			documentReference.setTarget(remoteDocumentId);

			return hierarchyNodesLoader
				.processRootDocument(remoteDocumentId, documentReference)
				.then(function() {
					return null;
				})
				.catch(function(error) {
					// Collect errors rather than failing the entire promise.all
					return error;
				});
		})
	).then(function(errors) {
		// Only fail if every document failed to load.
		// Failing here causes a permanent error which will prevent the editor from loading
		//   and show an author the error screen instead.
		if (
			errors.length > 0 &&
			errors.every(function(error) {
				return !!error;
			})
		) {
			// This is not required. Fonto allows loading no documents, or loading only errored documents.
			// This causes Fonto to display an error placeholder for the root documents.
			console.error('Unable to load any initial document', errors);
			throw errors[0];
		}
	});
}
