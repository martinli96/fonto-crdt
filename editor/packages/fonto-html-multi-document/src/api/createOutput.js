import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import connectorsManager from 'fontoxml-configuration/src/connectorsManager.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';
import getImageDataFromUrl from 'fontoxml-image-resolver/src/getImageDataFromUrl.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';

import variablesManager from 'fonto-html-sx-modules/src/api/variablesManager.js';

const configuredAssetConnector = connectorsManager.getConnector('asset-connector');

function isAnnotationMarker(node) {
	return node.target === 'fontoxml-annotation-start' || node.target === 'fontoxml-annotation-end';
}

function isChangeMarker(node) {
	return (
		node.target === 'fontoxml-change-addition-start' ||
		node.target === 'fontoxml-change-addition-end'
	);
}

function isTextPlaceholder(node) {
	return node.target === 'fontoxml-text-placeholder';
}

/**
 * Creates the head of the output.
 * @param {Node}        currentNode HTML node to create the output of.
 * @param {Node}        newNode     HTML node where the output will be created.
 * @param {Blueprint}   blueprint   The blueprint to use when generating the output.
 */
function loopHeadChildNodes(currentNode, newNode, blueprint) {
	const childNodes = blueprint.getChildNodes(currentNode);
	childNodes.forEach(childNode => {
		if (evaluateXPathToBoolean('self::head', currentNode, blueprint)) {
			const newDeepChildNode = blueprint.cloneNode(childNode, true);
			blueprint.appendChild(newNode, newDeepChildNode);
		}
	});
}

function getValuesByNameFromfilterUnits(valuesByName, filterUnit) {
	const filterUnitArr = filterUnit.split(':');
	const name = filterUnitArr[0];
	const value = filterUnitArr[filterUnitArr.length - 1];
	if (!valuesByName[name]) {
		valuesByName[name] = [];
	}
	valuesByName[name].push(value);
	return valuesByName;
}

/**
 * Gets the base64 data of an image contained within a document.
 * @param {*} remoteDocumentId Id of the document that contains the image.
 * @param {*} imageReferenceId Id of the image.
 */
async function getImageBase64Data(remoteDocumentId, imageReferenceId) {
	const previewUrl = await configuredAssetConnector.getPreviewUrl(
		remoteDocumentId,
		'web',
		imageReferenceId
	);
	const allImageData = await getImageDataFromUrl(window.document, previewUrl);
	return allImageData;
}

async function recursiveThroughChildNodes(currentNode, readOnlyBlueprint, outputOptions) {
	let actions = [];
	const childNodes = readOnlyBlueprint.getChildNodes(currentNode);
	for (const childNode of childNodes) {
		if (
			outputOptions.length > 0 &&
			evaluateXPathToBoolean('@data-only-for or @data-not-for', childNode, readOnlyBlueprint)
		) {
			const onlyFor = evaluateXPathToStrings(
					'./@data-only-for/tokenize(.)',
					childNode,
					readOnlyBlueprint
				).reduce(getValuesByNameFromfilterUnits, {}),
				notFor = evaluateXPathToStrings(
					'./@data-not-for/tokenize(.)',
					childNode,
					readOnlyBlueprint
				).reduce(getValuesByNameFromfilterUnits, {});

			let isFiltered = false;

			outputOptions.forEach(option => {
				if (isFiltered) {
					return;
				}
				const name = option.name;
				const value = option.value;
				if (notFor[name] && notFor[name].includes(value)) {
					isFiltered = true;
					return;
				}
				if (onlyFor[name] && !onlyFor[name].includes(value)) {
					isFiltered = true;
					return;
				}
			});

			if (isFiltered) {
				return;
			}
		}

		if (evaluateXPathToBoolean('self::document-ref', childNode, readOnlyBlueprint)) {
			// Resolve document reference.
			const docRefSrc = childNode.getAttribute('src');
			let docRefSectionId = null;
			let docRefRemoteDocumentId = null;
			if (docRefSrc.includes('#') || docRefSrc.includes('%23')) {
				if (docRefSrc.includes('#')) {
					docRefRemoteDocumentId = docRefSrc.split('#')[0];
					docRefSectionId = docRefSrc.split('#')[1];
				} else {
					docRefRemoteDocumentId = docRefSrc.split('%23')[0];
					docRefSectionId = docRefSrc.split('%23')[1];
				}

				// Connectors potentially uri encodes this stuff.
				docRefRemoteDocumentId = decodeURIComponent(docRefRemoteDocumentId);

				const documentId = documentsManager.getDocumentIdByRemoteDocumentId(
					docRefRemoteDocumentId
				);
				const documentNode = documentsManager.getDocumentNode(documentId);
				const docRefSectionNode = evaluateXPathToFirstNode(
					`./body/descendant::*[@id="${docRefSectionId}"]`,
					documentNode.documentElement,
					readOnlyBlueprint
				);

				actions.push((blueprint, newNode) => {
					// Create new section and clone referenced section node.
					const targetNode = namespaceManager.createElement(newNode, 'section');
					const docRefClonedSectionNode = blueprint.cloneNode(docRefSectionNode, true);
					// Move the content of the external section into the new section.
					blueprintMutations.unsafeMoveNodes(
						blueprint.getFirstChild(docRefClonedSectionNode),
						blueprint.getLastChild(docRefClonedSectionNode),
						blueprint,
						targetNode,
						null
					);

					if (blueprint.getChildNodes(childNode).length > 0) {
						blueprintMutations.unsafeMoveNodes(
							blueprint.getFirstChild(childNode),
							blueprint.getLastChild(childNode),
							blueprint,
							targetNode,
							null
						);
					}
					// Add new section to the output.
					blueprint.appendChild(newNode, targetNode);
				});
			} else {
				docRefRemoteDocumentId = docRefSrc;
				// Connectors potentially uri encodes this stuff.
				docRefRemoteDocumentId = decodeURIComponent(docRefRemoteDocumentId);
				const documentId = documentsManager.getDocumentIdByRemoteDocumentId(
					docRefRemoteDocumentId
				);
				const documentNode = documentsManager.getDocumentNode(documentId);
				// Get node and class value.
				const bodyNode = evaluateXPathToFirstNode(
					'./body',
					documentNode.documentElement,
					readOnlyBlueprint
				);
				const classAttributeValue = evaluateXPathToString(
					'./@class',
					documentNode.documentElement,
					readOnlyBlueprint
				);
				// Recursively retrieve the actions of the body and child nodes.
				const subActionsForBody = await recursiveThroughChildNodes(
					bodyNode,
					readOnlyBlueprint,
					outputOptions
				);
				const subActionsForChildNode = await recursiveThroughChildNodes(
					childNode,
					readOnlyBlueprint,
					outputOptions
				);
				// Add action to the actions array.
				actions.push((blueprint, newNode) => {
					const newSectionNode = namespaceManager.createElement(newNode, 'section');
					blueprint.appendChild(newNode, newSectionNode);
					if (classAttributeValue) {
						blueprint.setAttribute(newSectionNode, 'class', classAttributeValue);
					}
					// Do subactions as well.
					for (const subAction of subActionsForBody) {
						subAction(blueprint, newSectionNode);
					}
					for (const subAction of subActionsForChildNode) {
						subAction(blueprint, newSectionNode);
					}
				});
			}
		}
		// Variable resolving.
		else if (
			evaluateXPathToBoolean('self::span[@data-is-variable]', childNode, readOnlyBlueprint)
		) {
			// Check if there is a variable to resolve.
			const spanClassValue = evaluateXPathToString('./@class', childNode, readOnlyBlueprint);
			if (!spanClassValue) {
				return;
			}
			// Add action to actions array.
			actions.push((blueprint, newNode) => {
				// Create a span to contain the variable.
				const newSpanNode = namespaceManager.createElement(newNode, 'span');
				// Add it to the blueprint and set the class attribute.
				blueprint.appendChild(newNode, newSpanNode);
				blueprint.setAttribute(newSpanNode, 'class', spanClassValue);
				// Get the variables from the manager.
				const variables = variablesManager.getVariables();
				// Retrieve the variable, then check if it has a value assigned.
				const variable = variables.find(v => v.name === spanClassValue);
				// If the variable is not found, or it has no value, return.
				if (!variable || variable.value === null) {
					return;
				}
				// Add variable value to the output as a text node.
				const valueTextNode = newSpanNode.ownerDocument.createTextNode(variable.value);
				blueprint.appendChild(newSpanNode, valueTextNode);
			});
		}
		// Images handling.
		else if (evaluateXPathToBoolean('self::img', childNode, readOnlyBlueprint)) {
			// Get current document reference.
			// TODO try to improve this code to not depend on documentsHierarchy.
			const documents = documentsHierarchy.findAll(() => true);
			let remoteDocumentId = null;
			if (documents && documents.length > 0) {
				// If we have a document, we take its remoteDocumentId from the documentReference to use it in the URL
				remoteDocumentId = documents[0].documentReference.remoteDocumentId;
			}
			// Get image reference, used in request for image full data.
			const imageRefSrc = childNode.getAttribute('src');

			// Copy image node. We will change its 'src' value to be a base64 string.
			// Call connector to retrieve image data.
			const imageBase64Data = await getImageBase64Data(remoteDocumentId, imageRefSrc);
			actions.push((blueprint, newNode) => {
				const newChildNode = blueprint.cloneNode(childNode, false);
				blueprint.setAttribute(newChildNode, 'src', imageBase64Data.dataUrl);
				blueprint.appendChild(newNode, newChildNode);
			});
		}
		// Conref handling. Get the actions for inserting conrefs into output.
		else if (
			evaluateXPathToBoolean('self::element()[@data-conref]', childNode, readOnlyBlueprint)
		) {
			// Resolve conref.
			const src = childNode.getAttribute('data-conref');
			let sectionId = null;
			let remoteDocumentId = null;
			if (src.includes('#')) {
				remoteDocumentId = src.split('#')[0];
				sectionId = src.split('#')[1];
			} else {
				remoteDocumentId = src.split('%23')[0];
				sectionId = src.split('%23')[1];
			}
			// Connectors potentially uri encodes this stuff.
			remoteDocumentId = decodeURIComponent(remoteDocumentId);
			const documentId = documentsManager.getDocumentIdByRemoteDocumentId(remoteDocumentId);
			const documentNode = documentsManager.getDocumentNode(documentId);
			const sectionNode = evaluateXPathToFirstNode(
				`./body/descendant::*[@id="${sectionId}"]`,
				documentNode.documentElement,
				readOnlyBlueprint
			);
			// Add the action to the action array.
			actions.push((blueprint, newNode) => {
				// Create the target node and clone the section node.
				const targetNode = namespaceManager.createElement(newNode, childNode.localName);
				const clonedSectionNode = blueprint.cloneNode(sectionNode, true);
				// Move the content of the external section into the new section
				blueprintMutations.unsafeMoveNodes(
					blueprint.getFirstChild(clonedSectionNode),
					blueprint.getLastChild(clonedSectionNode),
					blueprint,
					targetNode,
					null
				);

				const childNodes = blueprint.getChildNodes(childNode);
				if (childNodes.length > 0) {
					childNodes.forEach(node => {
						blueprintMutations.unsafeMoveNodes(
							blueprint.getFirstChild(node),
							blueprint.getLastChild(node),
							blueprint,
							targetNode,
							null
						);
					});
				}
				// Add node to the output.
				blueprint.appendChild(newNode, targetNode);
			});
		} else {
			// Get actions recursively.
			const subActions = await recursiveThroughChildNodes(
				childNode,
				readOnlyBlueprint,
				outputOptions
			);
			actions.push((blueprint, newNode) => {
				const newChildNode = blueprint.cloneNode(childNode, false);
				if (
					!evaluateXPathToBoolean(
						'self::processing-instruction(author-guidance)',
						childNode,
						blueprint
					) &&
					!isAnnotationMarker(childNode) &&
					!isChangeMarker(childNode) &&
					!isTextPlaceholder(childNode)
				) {
					blueprint.appendChild(newNode, newChildNode);
				}
				for (const subAction of subActions) {
					subAction(blueprint, newChildNode);
				}
			});
		}
	}
	return actions;
}

/**
 * @callback doIet
 *
 * @param  {Blueprint}  blueprint
 * @param  {Node}       newDocumentNode
 */

/**
 * Creates the actions needed to create an output of a document. Will populate the 'newDocumentNode' node when executing the returned actions.
 *
 * @param  {Node}               htmlNode           The root node of the html document to create an output of.
 * @param  {ReadOnlyBlueprint}  readOnlyBlueprint  The read-only blueprint to use when generating the output. Will not be used to write
 * @param  {Object}             options            The options
 * @param  {Array}              options.outputOptions=[]            More options
 *
 * @return {Promise<doIet>} Returns a function accepting a blueprint and a document that actually edits the given document.
 */
export default async function createOutput(htmlNode, readOnlyBlueprint, options) {
	const outputOptions = options.outputOptions || [];
	const headNode = evaluateXPathToFirstNode('./head', htmlNode, readOnlyBlueprint);
	const classAttributeValue = evaluateXPathToString('./@class', htmlNode, readOnlyBlueprint);
	const bodyNode = evaluateXPathToFirstNode('./body', htmlNode, readOnlyBlueprint);
	const actions = await recursiveThroughChildNodes(bodyNode, readOnlyBlueprint, outputOptions);

	return (blueprint, newDocumentNode) => {
		const newHtmlNode = evaluateXPathToFirstNode('./html', newDocumentNode, readOnlyBlueprint);
		const newHeadNode = evaluateXPathToFirstNode('./head', newHtmlNode, readOnlyBlueprint);
		blueprint.setAttribute(newHtmlNode, 'data-is-output-document', 'true');
		const newBodyNode = evaluateXPathToFirstNode('./body', newHtmlNode, readOnlyBlueprint);

		const titleNode = evaluateXPathToFirstNode('./title', newHeadNode, readOnlyBlueprint);
		if (titleNode) {
			blueprint.removeChild(newHeadNode, titleNode);
		}

		if (classAttributeValue) {
			blueprint.setAttribute(newHtmlNode, 'class', classAttributeValue);
		}
		// Taking care of the head nodes.
		loopHeadChildNodes(headNode, newHeadNode, blueprint);
		// Add body to the output.
		actions.forEach(action => action(blueprint, newBodyNode));
	};
}
