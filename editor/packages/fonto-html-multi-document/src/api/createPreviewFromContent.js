import Blueprint from 'fontoxml-blueprints/src/Blueprint.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import createDocumentControllerFromContent from 'fontoxml-documents/src/createDocumentControllerFromContent.js';

const content = [
	'<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:fontoxml:fonto-html.xsd">',
	'<head><title> </title><style><!-- {{fbb:css}} --></style></head>',
	'<body></body>',
	'</html>'
].join('');

async function preprocessDocument(documentId, modifyTemplateCallback) {
	const documentController = documentsManager.getDocumentController(documentId);

	const performActions = await modifyTemplateCallback();

	// Allow the callback to modify the document template before sending it to the CMS
	documentController.executeFunction(function executeDocumentModification(commandContext) {
		const blueprint = new Blueprint(commandContext.dom);
		const documentNode = documentController.document.dom.documentNode;

		performActions(blueprint, documentNode);

		// We'll validate later
		blueprint.realize();
	}, {});
}

/**
 * @param  {Object}  options
 * @param  {function(Blueprint, Format, dom.Document):void}  [options.modifyTemplateCallback]
 *                                                 Can be used to modify the document before it is sent to the CMS *
 * @return  {Promise.<string>}  Returns a promise which resolved to the documentId of the created document.
 */
export default async function createPreviewFromContent(options) {
	const documentController = await createDocumentControllerFromContent(content);
	const documentId = await documentsManager.addDocument(null, documentController);
	if (documentsManager.getRemoteDocumentId(documentId) !== null) {
		throw new Error('The given template document must be a local document');
	}

	// Pre-process the document while it is still in previewable state, as the template may not
	// be valid until after pre-processing
	if (options.modifyTemplateCallback) {
		await preprocessDocument(documentId, options.modifyTemplateCallback);
	}
	return documentId;
}
