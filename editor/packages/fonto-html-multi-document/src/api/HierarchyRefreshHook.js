import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import Hook from 'fontoxml-core/src/Hook.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';

import refreshHierarchy from './refreshHierarchy.js';

function HierarchyRefreshHook() {
	Hook.call(this);

	this._hierarchyChanged = false;
}

HierarchyRefreshHook.prototype = Object.create(Hook.prototype);
HierarchyRefreshHook.prototype.constructor = HierarchyRefreshHook;

function isHierarchyNode(node) {
	return evaluateXPathToBoolean(
		'self::html or self::body or self::document-ref or self::template-ref',
		node,
		readOnlyBlueprint
	);
}

function hasHierarchyNode(nodeList) {
	return Array.prototype.some.call(nodeList, isHierarchyNode);
}

HierarchyRefreshHook.prototype.onAfterCommand = function(_commandContext, mutationRecords) {
	if (this._hierarchyChanged) {
		return;
	}

	this._hierarchyChanged = mutationRecords.some(function(record) {
		// Trigger on any attribute for now, we also may need to refresh on, e.g., role changes
		if (record.type === 'attributes' && isHierarchyNode(record.target)) {
			return true;
		}

		return hasHierarchyNode(record.addedNodes) || hasHierarchyNode(record.removedNodes);
	});
};

HierarchyRefreshHook.prototype.onAfterCommit = function() {
	if (this._hierarchyChanged) {
		this._hierarchyChanged = false;
		refreshHierarchy().catch(function() {});
	}
};

HierarchyRefreshHook.prototype.onAfterRollback = function() {
	this._hierarchyChanged = false;
};

export default HierarchyRefreshHook;
