import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

export default function getRefNodeIdReferencingDocument(documentElement) {
	const hierarchyNodeId = evaluateXPathToString(
		'fonto:current-hierarchy-node-id()',
		documentElement,
		readOnlyBlueprint
	);
	if (!hierarchyNodeId) {
		return null;
	}
	const hierarchyNodes = documentsHierarchy.findAll(function(n) {
		return n.getId() === hierarchyNodeId;
	});
	if (!hierarchyNodes[0] || !hierarchyNodes[0].documentReference) {
		return null;
	}
	return hierarchyNodes[0].documentReference.sourceNodeId;
}
