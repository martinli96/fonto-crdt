import React, { useState, useEffect } from 'react';

import {
	Button,
	FormRow,
	Label,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	TextInput
} from 'fds/components';
import t from 'fontoxml-localization/src/t.js';

import variablesManager from '../api/variablesManager.js';
import useDefinitionFile from './useDefinitionFile.jsx';

let variables = [];

export default function EditVariablesModal({ cancelModal, data, submitModal }) {
	const [variableValues, setVariableValues] = useState({});
	const [firstUpdate, setFirstUpdate] = useState(true);
	const definitionFile = useDefinitionFile(data.contextNodeId);
	const textInputRefByName = {};

	useEffect(() => {
		variables = variablesManager.externalVariables;
		const currentVariables = variablesManager.currentVariables;
		const editableVariableValues = {};
		variables.forEach((externalVariable, index) => {
			if (externalVariable.value === null) {
				const currentVariable = currentVariables.find(
					currentVariable => currentVariable.name === externalVariable.name
				);
				editableVariableValues[index] = currentVariable ? currentVariable.value || '' : '';
			}
		});
		setVariableValues(editableVariableValues);
	}, [definitionFile]);

	useEffect(() => {
		if (firstUpdate && data.name && textInputRefByName[data.name]) {
			setTimeout(() => {
				textInputRefByName[data.name].focus();
				setFirstUpdate(false);
			}, 0);
		}
	}, [data.name, firstUpdate, textInputRefByName, variableValues]);

	const handleSubmit = () =>
		submitModal({
			variables: Object.keys(variableValues).reduce((newVariables, index) => {
				if (variableValues[index] === '') {
					return newVariables;
				}
				newVariables.push({
					name: variables[index].name,
					value: variableValues[index]
				});
				return newVariables;
			}, [])
		});

	const handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;

			case 'Enter':
				if (Object.keys(variableValues).length) {
					handleSubmit();
				}
				break;
		}
	};

	const handleTextInputChange = (value, index) => {
		const newVariableValues = { ...variableValues };
		newVariableValues[index] = value;
		setVariableValues(newVariableValues);
	};

	const handleTextInputRef = (domNode, name) => (textInputRefByName[name] = domNode);

	return (
		<Modal size="s" onKeyDown={handleKeyDown}>
			<ModalHeader title={t('Edit the variables value')} />
			<ModalBody>
				<ModalContent flexDirection="column">
					{variables.length > 0 && (
						<ModalContent
							flexDirection="column"
							paddingSize="m"
							spaceSize="s"
							isScrollContainer
						>
							{variables.map((variable, index) => (
								<FormRow key={index} labelPosition="before" label={variable.name}>
									{variable.value && <Label isBold>{variable.value}</Label>}
									{variable.value === null && (
										<TextInput
											value={variableValues[index]}
											onChange={value => handleTextInputChange(value, index)}
											placeholder={t('Variable value')}
											onRef={domNode =>
												handleTextInputRef(domNode, variable.name)
											}
										/>
									)}
								</FormRow>
							))}
						</ModalContent>
					)}
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label={t('Cancel')} onClick={cancelModal} />
				<Button
					label={t('Save')}
					isDisabled={!Object.keys(variableValues).length}
					type="primary"
					onClick={handleSubmit}
				/>
			</ModalFooter>
		</Modal>
	);
}
