import { useState, useEffect, useRef } from 'react';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import hierarchyNodesLoader from 'fonto-html-multi-document/src/api/hierarchyNodesLoader.js';

export default function useDefinitionFile(rootDocumentNodeId) {
	const hasChanged = useRef(false);
	const [definitionFile, setDefinitionFile] = useState({
		rootDocumentNodeId: rootDocumentNodeId
	});

	const templateHasChanged = () => {
		const rootDocumentNode = documentsManager.getNodeById(rootDocumentNodeId);
		const templateRefNode = rootDocumentNode
			? evaluateXPathToFirstNode(
					'//html/head/template-ref',
					rootDocumentNode,
					readOnlyBlueprint
			  )
			: null;
		const contextNodeId = templateRefNode ? getNodeId(templateRefNode) : null;

		const templateDocumentId = hierarchyNodesLoader.templateDocumentId;
		const remoteDocumentId = documentsManager.getRemoteDocumentId(templateDocumentId);
		setDefinitionFile({
			rootDocumentNodeId: rootDocumentNodeId,
			documentId: templateDocumentId,
			remoteDocumentId: remoteDocumentId,
			contextNodeId: contextNodeId,
			hasChanged: hasChanged.current
		});
		hasChanged.current = true;
	};

	useEffect(() => {
		templateHasChanged();
		const removeCallback = hierarchyNodesLoader.templateDocumentIdChangedNotifier.addCallback(
			templateHasChanged
		);

		return removeCallback;
	}, [rootDocumentNodeId]);

	return definitionFile;
}
