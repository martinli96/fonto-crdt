import React, { useState, useEffect, useCallback } from 'react';

import {
	Button,
	CheckboxGroup,
	Flex,
	Form,
	FormRow,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	StateMessage,
	Text,
	TextLink
} from 'fds/components';
import { hasFormFeedback } from 'fds/system';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';

import outputOptionsManager from '../api/outputOptionsManager.js';

function getValuesByNameFromOutputOptionUnits(valuesByName, outputOptionUnit) {
	const outputOptionUnitArr = outputOptionUnit.split(':');
	const name = outputOptionUnitArr[0];
	const value = outputOptionUnitArr[outputOptionUnitArr.length - 1];
	if (!valuesByName[name]) {
		valuesByName[name] = [];
	}
	valuesByName[name].push(value);
	return valuesByName;
}

function getOutputOptionssAndValues(contextNode) {
	const currentValues = {
		onlyFor: contextNode
			? evaluateXPathToStrings(
					'./@data-only-for/tokenize(.)',
					contextNode,
					readOnlyBlueprint
			  ).reduce(getValuesByNameFromOutputOptionUnits, {})
			: {},
		notFor: contextNode
			? evaluateXPathToStrings(
					'./@data-not-for/tokenize(.)',
					contextNode,
					readOnlyBlueprint
			  ).reduce(getValuesByNameFromOutputOptionUnits, {})
			: {}
	};
	return outputOptionsManager.getOutputOptions(currentValues, [], false, false);
}

export default function EditOutputOptionsModal({ cancelModal, data, submitModal }) {
	const [outputOptions, setOutputOptions] = useState([]);
	const [valueByName, setValueByName] = useState({});
	const [feedbackByName, setFeedbackByName] = useState({});

	useEffect(() => {
		const contextNode = documentsManager.getNodeById(data.contextNodeId);
		const newOutputOptionsAndValues = getOutputOptionssAndValues(contextNode);

		setOutputOptions(newOutputOptionsAndValues.outputOptions);
		setValueByName(newOutputOptionsAndValues.valueByName);
	}, [data.contextNodeId]);

	const handleSubmit = useCallback(() => {
		const submitValues = outputOptions.reduce(
			(submitValues, outputOption) => {
				const values = valueByName[outputOption.name];
				if (values.length === outputOption.valueItems.length) {
					return submitValues;
				}
				if (values.length > outputOption.valueItems.length / 2) {
					outputOption.valueItems.forEach(item => {
						if (!values.includes(item.value)) {
							submitValues.notFor.push(outputOption.name + ':' + item.value);
						}
					});
				} else {
					values.forEach(value => {
						submitValues.onlyFor.push(outputOption.name + ':' + value);
					});
				}
				return submitValues;
			},
			{
				onlyFor: [],
				notFor: []
			}
		);
		submitModal({
			onlyFor: submitValues.onlyFor.length > 0 ? submitValues.onlyFor.join(' ') : null,
			notFor: submitValues.notFor.length > 0 ? submitValues.notFor.join(' ') : null
		});
	}, [outputOptions, submitModal, valueByName]);

	const handleKeyDown = useCallback(
		event => {
			switch (event.key) {
				case 'Escape':
					cancelModal();
					break;
				case 'Enter':
					if (outputOptions.length > 0 && !hasFormFeedback(feedbackByName)) {
						handleSubmit();
					}
					break;
			}
		},
		[cancelModal, feedbackByName, outputOptions.length, handleSubmit]
	);

	const handleFormFieldChange = useCallback(
		({ feedback, name, value }) => {
			setFeedbackByName({ ...feedbackByName, [name]: feedback });
			setValueByName({ ...valueByName, [name]: value });
		},
		[feedbackByName, valueByName]
	);

	const handleValidate = useCallback(value => {
		if (value.length === 0) {
			return {
				connotation: 'warning',
				message: t('Select at least one output option')
			};
		}

		return null;
	}, []);

	const handleSelectAll = useCallback(
		name => {
			const value = outputOptions
				.find(outputOption => outputOption.name === name)
				.valueItems.map(item => item.value);
			setValueByName({ ...valueByName, [name]: value });
			setFeedbackByName({ ...feedbackByName, [name]: handleValidate(value) });
		},
		[feedbackByName, outputOptions, handleValidate, valueByName]
	);

	const handleClearAll = useCallback(
		name => {
			setValueByName({ ...valueByName, [name]: [] });
			setFeedbackByName({ ...feedbackByName, [name]: handleValidate([]) });
		},
		[feedbackByName, handleValidate, valueByName]
	);

	return (
		<Modal size="s" onKeyDown={handleKeyDown}>
			<ModalHeader title={t('Set the output option')} />
			<ModalBody>
				<ModalContent flexDirection="column">
					{!outputOptions.length && (
						<StateMessage
							title={t('Missing output options')}
							visual="meh-o"
							paddingSize="l"
						/>
					)}

					{outputOptions.length > 0 && (
						<ModalContent
							flexDirection="column"
							paddingSize="m"
							spaceSize="m"
							isScrollContainer
						>
							<Form
								feedbackByName={feedbackByName}
								valueByName={valueByName}
								onFieldChange={handleFormFieldChange}
							>
								<Flex
									applyCss={{ minWidth: outputOptions.length * 120 + 'px' }}
									flex="1"
									flexDirection="row"
									spaceSize="l"
								>
									{outputOptions.map((outputOption, index) => (
										<FormRow key={index} label={outputOption.name}>
											<Flex flex="none" spaceSize="s">
												<TextLink
													label={t('Select all')}
													onClick={() =>
														handleSelectAll(outputOption.name)
													}
													size="s"
												/>
												<Text colorName="text-muted-color" size="s">
													/
												</Text>
												<TextLink
													label="Clear all"
													onClick={() =>
														handleClearAll(outputOption.name)
													}
													size="s"
												/>
											</Flex>
											<CheckboxGroup
												items={outputOption.valueItems}
												name={outputOption.name}
												validate={handleValidate}
											/>
										</FormRow>
									))}
								</Flex>
							</Form>
						</ModalContent>
					)}
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label={t('Cancel')} onClick={cancelModal} />
				<Button
					label="Save"
					isDisabled={!outputOptions.length || hasFormFeedback(feedbackByName)}
					type="primary"
					onClick={handleSubmit}
				/>
			</ModalFooter>
		</Modal>
	);
}
