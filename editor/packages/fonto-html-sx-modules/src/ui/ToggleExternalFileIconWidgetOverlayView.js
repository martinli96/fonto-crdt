import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import domQuery from 'fontoxml-dom-utils/src/domQuery.js';
import View from 'fontoxml-views/src/View.js';

function ToggleExternalFileIconWidgetOverlayView(viewRootNode, templatedView) {
	View.call(this);

	this._viewRootNode = viewRootNode;
	this._templatedView = templatedView;
}

ToggleExternalFileIconWidgetOverlayView.prototype = Object.create(View.prototype);
ToggleExternalFileIconWidgetOverlayView.prototype.constructor = ToggleExternalFileIconWidgetOverlayView;

ToggleExternalFileIconWidgetOverlayView.prototype._updateWidgets = function() {
	const widgetContainers = this._viewRootNode.querySelectorAll(
		'[data-is-toggle-external-file-icon-widget="true"]'
	);

	this._templatedView.mutationLock.unlock(() => {
		widgetContainers.forEach(widgetContainer => {
			const inclusionNodeDiv = domQuery.findClosestAncestor(
				widgetContainer,
				node => domInfo.isElement(node, 'DIV') && node.getAttribute('inclusion-node-id')
			);
			if (!inclusionNodeDiv) {
				return;
			}

			const inclusionNodeId = inclusionNodeDiv.getAttribute('inclusion-node-id');
			const initialData = JSON.parse(widgetContainer.getAttribute('operation-initial-data'));

			widgetContainer.setAttribute(
				'tooltip-content',
				'Inline this section into the current file'
			);

			if (!initialData.contextNodeId) {
				return;
			}

			const documentId = documentsManager.getDocumentIdByNodeId(initialData.contextNodeId);
			const documentNodeNodeId = getNodeId(documentsManager.getDocumentNode(documentId));

			widgetContainer.setAttribute(
				'operation-name',
				'contextual-convert-document-to-section'
			);
			widgetContainer.setAttribute(
				'operation-initial-data',
				JSON.stringify({
					documentNodeNodeId,
					documentRefNodeId: inclusionNodeId
				})
			);

			const cvIconNode = widgetContainer.querySelector('cv-icon');
			cvIconNode.setAttribute('class', 'far fa-sign-in');
		});
	});
};

ToggleExternalFileIconWidgetOverlayView.prototype.update = function() {
	View.prototype.update.call(this);

	this._updateWidgets();
};

ToggleExternalFileIconWidgetOverlayView.prototype.loadDocument = function(documentController) {
	View.prototype.loadDocument.call(this, documentController);

	this._updateWidgets();
};

export default ToggleExternalFileIconWidgetOverlayView;
