import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { Popover, PopoverBody, PopoverFooter, PopoverHeader, TextInput } from 'fds/components';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

class ChangeAltTextPopover extends Component {
	static propTypes = {
		data: PropTypes.shape({
			contextNodeId: PropTypes.string.isRequired
		}).isRequired
	};

	textInputRef = null;

	state = {
		altText: evaluateXPathToString(
			'./@alt',
			documentsManager.getNodeById(this.props.data.contextNodeId),
			readOnlyBlueprint
		)
	};

	handleChange = value => this.setState({ altText: value });

	handleTextInputRef = textInputRef => (this.textInputRef = textInputRef);

	render() {
		const { altText } = this.state;

		return (
			<Popover maxWidth="300px" minWidth="220px">
				<PopoverHeader title="Edit alternative text" />

				<PopoverBody>
					<TextInput
						onChange={this.handleChange}
						onRef={this.handleTextInputRef}
						value={altText}
					/>
				</PopoverBody>

				<PopoverFooter>
					<FxOperationButton
						label="Save"
						onClick={this.props.togglePopover}
						operationData={{
							contextNodeId: this.props.data.contextNodeId,
							attributes: {
								alt: altText
							}
						}}
						operationName="set-attributes"
					/>
				</PopoverFooter>
			</Popover>
		);
	}

	componentDidMount() {
		// This is a workaround for DEV-1423
		this.textInputRef.focus({ preventScroll: true });
	}
}

export default ChangeAltTextPopover;
