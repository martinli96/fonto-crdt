import React from 'react';

import { Flex, FormRow } from 'fds/components';

import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import t from 'fontoxml-localization/src/t.js';

export default function ChangeDefinitionFile({ definitionFile }) {
	return (
		<FormRow labelPosition="before" label="Definition file">
			<Flex flexDirection="column" justifyContent="space-between">
				<Flex justifyContent="space-between" alignItems="flex-end">
					<FxOperationButton
						label={t('Select definition...')}
						operationName="contextual-edit-definition-file"
						operationData={{
							contextNodeId:
								definitionFile.contextNodeId || definitionFile.rootDocumentNodeId,
							selectDefinition: true
						}}
					/>
				</Flex>
			</Flex>
		</FormRow>
	);
}
