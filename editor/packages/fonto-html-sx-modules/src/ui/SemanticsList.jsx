import React from 'react';

import { Flex, Label, Text, UnorderedList, UnorderedListItem } from 'fds/components';
import applyCss from 'fontoxml-styles/src/applyCss.js';

const markers = ['disc', 'circle', 'square'];

export default function SemanticsList({ semantics }) {
	return (
		<UnorderedList marker={markers[semantics[0].level % markers.length]}>
			{semantics.map((semantic, index) => (
				<UnorderedListItem key={index}>
					<Flex flex="1" flexDirection="column">
						<Flex flex="1" justifyContent="space-between">
							<Label isItalic colorName="text-muted-color">
								{semantic.name}
							</Label>
							<Flex spaceSize="m" flexDirection="column" applyCss={{ width: '70%' }}>
								{semantic.document && (
									<Text
										fontStackName="monospace"
										colorName="text-muted-color"
										align="right"
									>
										document
									</Text>
								)}
								{semantic.section && (
									<Text
										fontStackName="monospace"
										colorName="text-muted-color"
										align="right"
									>
										section
									</Text>
								)}
								{semantic.block && (
									<Text
										fontStackName="monospace"
										colorName="text-muted-color"
										align="right"
									>
										block
									</Text>
								)}
								{semantic.inline && (
									<Text
										fontStackName="monospace"
										colorName="text-muted-color"
										align="right"
									>
										inline
									</Text>
								)}
							</Flex>
						</Flex>
						{semantic.childSemantics && (
							<SemanticsList semantics={semantic.childSemantics} />
						)}
					</Flex>
				</UnorderedListItem>
			))}
		</UnorderedList>
	);
}
