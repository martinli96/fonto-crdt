import React, { useState, useEffect } from 'react';

import {
	Button,
	Flex,
	Label,
	Modal,
	ModalBody,
	ModalBodyToolbar,
	ModalContent,
	ModalFooter,
	ModalHeader,
	UnorderedList,
	UnorderedListItem,
	TabButton,
	TabButtons
} from 'fds/components';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';

import ChangeDefinitionFile from './ChangeDefinitionFile.jsx';
import outputOptionsManager from '../api/outputOptionsManager.js';
import SemanticsList from './SemanticsList.jsx';
import useDefinitionFile from './useDefinitionFile.jsx';
import variablesManager from '../api/variablesManager.js';

function getSemanticsRecursively(semanticNode) {
	const semantic = {
		level: evaluateXPathToNumber('count(ancestor::ul)', semanticNode, readOnlyBlueprint) - 1,
		name: evaluateXPathToStrings('./p/span/string()', semanticNode, readOnlyBlueprint).join(
			', '
		),
		inline: evaluateXPathToBoolean(
			'child::p[@data-inline-type = true()]',
			semanticNode,
			readOnlyBlueprint
		),
		block: evaluateXPathToBoolean(
			'child::p[@data-block-type = true()]',
			semanticNode,
			readOnlyBlueprint
		),
		section: evaluateXPathToBoolean(
			'child::p[@data-section-type = true()]',
			semanticNode,
			readOnlyBlueprint
		),
		document: evaluateXPathToBoolean(
			'child::p[@data-document-type = true()]',
			semanticNode,
			readOnlyBlueprint
		)
	};

	const childSemanticNodes = evaluateXPathToNodes('./ul/li', semanticNode, readOnlyBlueprint);
	if (childSemanticNodes.length) {
		semantic.childSemantics = childSemanticNodes.map(getSemanticsRecursively);
	}
	return semantic;
}

let isOutputDocument = false;

function ShowVariablesList({ variables }) {
	return (
		<UnorderedList>
			{variables.map((variable, index) => (
				<UnorderedListItem key={index}>
					<Flex flex="1" flexDirection="column">
						<Flex flex="1" justifyContent="space-between">
							<Label colorName="text-muted-color">{variable.name}</Label>
							<Flex spaceSize="m">
								{variable.value && (
									<Label colorName="text-muted-color">{variable.value}</Label>
								)}
							</Flex>
						</Flex>
					</Flex>
				</UnorderedListItem>
			))}
		</UnorderedList>
	);
}

function ShowOutputOptionsList({ outputOptions }) {
	return (
		<Flex flex="none" flexDirection="column" spaceSize="m">
			{outputOptions
				.reduce(
					(twoColumns, outputOption, index) => {
						outputOption.index = index;
						if (twoColumns[twoColumns.length - 1].length > 1) {
							twoColumns.push([outputOption]);
						} else {
							twoColumns[twoColumns.length - 1].push(outputOption);
						}
						return twoColumns;
					},
					[[]]
				)
				.map((column, index) => (
					<Flex key={index + 'column'} spaceSize="m">
						{column.map(outputOption => (
							<Flex flex="1" key={outputOption.index}>
								<Label colorName="text-muted-color">
									{outputOption.name}
									<UnorderedList>
										{outputOption.valueItems.map((label, fIndex) => (
											<UnorderedListItem key={fIndex}>
												<Label colorName="text-muted-color">
													{label.value}
												</Label>
											</UnorderedListItem>
										))}
									</UnorderedList>
								</Label>
							</Flex>
						))}
					</Flex>
				))}
		</Flex>
	);
}

export default function ShowSemanticsModal({ cancelModal, data }) {
	const tabs = {
		CONTENT_TYPES: '1',
		VARIABLES: '2',
		OUTPUT_OPTIONS: '3'
	};

	const [tabId, setTabId] = useState(tabs.CONTENT_TYPES);
	const [semantics, setSemantics] = useState([]);
	const [variables, setVariables] = useState(variablesManager.getVariables());
	const [outputOptions, setOutputOptions] = useState(
		outputOptionsManager.getOutputOptions(null, [], true, false)
	);
	const definitionFile = useDefinitionFile(data.contextNodeId);

	useEffect(() => {
		const rootDocumentNode = documentsManager.getNodeById(data.contextNodeId);
		isOutputDocument = rootDocumentNode
			? evaluateXPathToBoolean(
					'//html[@data-is-output-document]',
					rootDocumentNode,
					readOnlyBlueprint
			  )
			: false;
		if (isOutputDocument) {
			const semanticNodes = rootDocumentNode
				? evaluateXPathToNodes(
						'//html/head/template[1]/ul/li',
						rootDocumentNode,
						readOnlyBlueprint
				  )
				: [];
			const newSemantics = semanticNodes.map(getSemanticsRecursively);
			setSemantics(newSemantics);
			return;
		}
	}, [data.contextNodeId]);

	useEffect(() => {
		if (isOutputDocument) {
			return;
		}
		const templateDocumentId = definitionFile.documentId;
		const templateDocumentNode = templateDocumentId
			? documentsManager.getDocumentNode(templateDocumentId)
			: null;
		const semanticNodes = templateDocumentNode
			? evaluateXPathToNodes(
					'//html/head/template[1]/ul/li',
					templateDocumentNode,
					readOnlyBlueprint
			  )
			: [];
		const newSemantics = semanticNodes.map(getSemanticsRecursively);

		setSemantics(newSemantics);
		const newOutputOptions = outputOptionsManager.getOutputOptions(null, [], true, false);
		setOutputOptions(newOutputOptions);
		const newVariables = variablesManager.getVariables();
		setVariables(newVariables);
	}, [definitionFile]);

	const handleKeyDown = event => {
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;
			case 'Enter':
				cancelModal();
				break;
		}
	};

	return (
		<Modal size="s" onKeyDown={handleKeyDown}>
			<ModalHeader title={t('Definitions')} />
			<ModalBody>
				<ModalBodyToolbar>
					<TabButtons>
						<TabButton
							label={t('Content types')}
							onClick={() => {
								setTabId(tabs.CONTENT_TYPES);
							}}
							isActive={tabId === tabs.CONTENT_TYPES}
						/>

						<TabButton
							label={t('Variables')}
							onClick={() => {
								setTabId(tabs.VARIABLES);
							}}
							isActive={tabId === tabs.VARIABLES}
						/>

						<TabButton
							label={t('Output Options')}
							onClick={() => {
								setTabId(tabs.OUTPUT_OPTIONS);
							}}
							isActive={tabId === tabs.OUTPUT_OPTIONS}
						/>
					</TabButtons>
				</ModalBodyToolbar>
				<Flex flexDirection="column" spaceSize="m">
					<Flex>
						{semantics.length > 0 &&
							semantics[0].name !== '' &&
							tabId === tabs.CONTENT_TYPES && (
								<ModalContent flexDirection="column" paddingSize="m">
									<SemanticsList semantics={semantics} />
								</ModalContent>
							)}
						{variables.length > 0 &&
							variables[0].name !== '' &&
							tabId === tabs.VARIABLES && (
								<ModalContent flexDirection="column" paddingSize="m">
									<ShowVariablesList variables={variables} />
								</ModalContent>
							)}

						{outputOptions.length > 0 &&
							outputOptions[0].name !== '' &&
							tabId === tabs.OUTPUT_OPTIONS && (
								<ModalContent flexDirection="column" paddingSize="m">
									<ShowOutputOptionsList outputOptions={outputOptions} />
								</ModalContent>
							)}
					</Flex>
					<Flex paddingSize="s">
						{!isOutputDocument && (
							<ChangeDefinitionFile definitionFile={definitionFile} />
						)}
					</Flex>
				</Flex>
			</ModalBody>

			<ModalFooter>
				<Button label={t('Close')} type="primary" onClick={cancelModal} />
			</ModalFooter>
		</Modal>
	);
}
