import React, { useMemo } from 'react';
import { StateMessage } from 'fds/components';
import t from 'fontoxml-localization/src/t.js';
import authorGuidancesManager from '../../api/authorGuidances/authorGuidancesManager.js';

function determineState() {
	const cards = authorGuidancesManager.getGuidances();

	// No cards. cards === null
	if (!cards) {
		return {
			title: t('Something went wrong'),
			message: t('We could not load the author guidances'),
			visual: 'warning',
			connotation: 'warning'
		};
	}

	// TODO: Manage this state
	if (cards === 'loading') {
		return {
			title: t('Loading...'),
			message: t("Author guidances will show up here as soon as they're available"),
			visual: 'inbox',
			connotation: 'muted'
		};
	}

	// cards.length === 0
	return {
		title: t('This is empty'),
		message: t('There are no author guidances written yet'),
		visual: 'sticky-note',
		connotation: 'muted'
	};
}

export default function AuthorGuidancesStateMessage() {
	const stateMessage = useMemo(determineState, [determineState]);

	return (
		<StateMessage
			title={stateMessage.title}
			message={stateMessage.message}
			visual={stateMessage.visual}
			connotation={stateMessage.connotation}
			paddingSize={{ top: 'l' }}
		/>
	);
}
