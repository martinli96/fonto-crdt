import React, { useState, useRef, useEffect, useCallback } from 'react';

import {
	Button,
	Flex,
	Form,
	FormRow,
	Icon,
	Modal,
	ModalBody,
	ModalFooter,
	ModalHeader,
	Text,
	TextArea,
	TextInput
} from 'fds/components';

import t from 'fontoxml-localization/src/t.js';

import openURLInNewTab from '../../api/utils/openURLInNewTab.js';

export default function AuthorGuidanceModal({ data, cancelModal, submitModal }) {
	const dataInputRef = useRef();
	const [changed, setChanged] = useState(false);
	const [valueByName, setValueByName] = useState(
		data.isEdit
			? data.oldGuidance
			: {
					instruction: null,
					guidance: null,
					url: null
			  }
	);
	const [showFeedback, setShowFeedback] = useState(false);

	useEffect(() => {
		dataInputRef.current.focus();
	}, []);

	const handleFormChange = useCallback(({ valueByName: newValueByName }) => {
		setValueByName(newValueByName);
		setShowFeedback(false);
	}, []);

	const handleSubmit = () => {
		if (showFeedback) {
			return;
		}

		if (!valueByName.instruction) {
			setShowFeedback(true);
			return;
		}

		if (!valueByName.guidance) {
			valueByName.guidance = '__null';
		}

		if (!valueByName.url) {
			valueByName.url = '__null';
		} else {
			valueByName.url = valueByName.url.trim();
		}

		const dataToReturn = data.isEdit
			? {
					oldGuidance: data.oldGuidance,
					newGuidance: valueByName
			  }
			: { ...valueByName };

		submitModal(dataToReturn);
	};

	const handleKeyDown = event => {
		setChanged(true);
		switch (event.key) {
			case 'Escape':
				cancelModal();
				break;
			case 'Enter':
				if (event.ctrlKey || event.metaKey) {
					handleSubmit();
				}
				break;
		}
	};

	return (
		<Modal size="s" onKeyDown={handleKeyDown}>
			<ModalHeader
				title={data.isEdit ? t('Edit Author guidance') : t('Insert Author guidance')}
			/>
			<ModalBody>
				<Form labelPosition="above" onChange={handleFormChange} valueByName={valueByName}>
					<FormRow label={t('Instruction')} labelColorName="text-color">
						{!changed ? (
							<TextInput
								name="instruction"
								value={data.isEdit && data.oldGuidance.instruction}
								ref={dataInputRef}
							/>
						) : (
							<TextInput name="instruction" />
						)}
					</FormRow>

					<FormRow label={t('Guidance')} labelColorName="text-color">
						{!changed ? (
							<TextArea
								name="guidance"
								rows={{ minimum: 2, maximum: 5 }}
								value={data.isEdit && data.oldGuidance.guidance}
							/>
						) : (
							<TextArea name="guidance" rows={{ minimum: 2, maximum: 5 }} />
						)}
					</FormRow>

					<FormRow label={t('URL')} labelColorName="text-color">
						<Flex spaceSize="s">
							{!changed ? (
								<TextInput name="url" value={data.isEdit && data.oldGuidance.url} />
							) : (
								<TextInput name="url" />
							)}
							<Button
								isBorderless
								icon="external-link"
								type="transparent"
								tooltipContent={t('Open URL')}
								onClick={() => openURLInNewTab(valueByName['url'])}
							/>
						</Flex>
					</FormRow>
				</Form>
			</ModalBody>

			<ModalFooter>
				<Button label="Cancel" onClick={cancelModal} />

				<Flex flexDirection="row" alignItems="center" spaceSize="m">
					{showFeedback && (
						<Flex spaceSize="s" alignItems="baseline">
							<Icon icon="times-circle" colorName="icon-s-error-color" />
							<Text colorName="text-error-color">
								{t('The instruction name is not filled in yet')}
							</Text>
						</Flex>
					)}
					<Button
						isDisabled={showFeedback}
						label={data.isEdit ? t('Edit') : t('Insert')}
						onClick={() => handleSubmit()}
						type="primary"
					/>
				</Flex>
			</ModalFooter>
		</Modal>
	);
}
