import React, { Fragment, useCallback, useState } from 'react';
import {
	Button,
	Card,
	Flex,
	HorizontalSeparationLine,
	Icon,
	Label,
	Text,
	VirtualForest
} from 'fds/components';

import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';
import t from 'fontoxml-localization/src/t.js';
import useManagerState from 'fontoxml-fx/src/useManagerState.js';

import modeSwitchingManager from 'fonto-html-mode-switching/src/api/modeSwitchingManager.js';
import AuthorGuidancesStateMessage from './AuthorGuidancesStateMessage.jsx';
import authorGuidancesManager from '../../api/authorGuidances/authorGuidancesManager.js';
import openURLInNewTab from '../../api/utils/openURLInNewTab.js';

const MAX_CARD_LENGTH = 80;

export default function AuthorGuidancesForm() {
	const templateMode = modeSwitchingManager.mode === 'template';
	const [selectedGuidance, setSelectedGuidance] = useState(
		authorGuidancesManager.getSelectedAuthorGuidance()
	);
	const [guidances, setGuidances] = useState(authorGuidancesManager.getGuidances());

	useManagerState(authorGuidancesManager.authorGuidancesChangedNotifier, () =>
		setGuidances(authorGuidancesManager.getGuidances())
	);

	useManagerState(authorGuidancesManager.selectedAuthorGuidancesChangedNotifier, () => {
		const guidance = authorGuidancesManager.getSelectedAuthorGuidance();
		setSelectedGuidance(guidance);
	});

	const renderItem = useCallback(
		({ item, onClick, key }) => {
			const isSelected = selectedGuidance && selectedGuidance.id === item.id;

			return (
				<Flex
					flex="1"
					flexDirection="column"
					key={key}
					paddingSize={{ bottom: 'm', right: 's' }}
				>
					<Card isHighlighted={!!isSelected} isSelected={!!isSelected} onClick={onClick}>
						<Flex justifyContent="space-between">
							<Flex spaceSize="s">
								<Icon icon="sticky-note" size="s" />
								<Label isBold>{item.instruction}</Label>
							</Flex>
							{item.url && (
								<Button
									icon="external-link"
									type="transparent"
									tooltipContent={t('Open related URL')}
									isBorderless
									onClick={() => openURLInNewTab(item.url)}
								/>
							)}
						</Flex>
						{!isSelected && item.guidance && (
							<Text>
								{item.guidance.length > MAX_CARD_LENGTH
									? item.guidance.substring(0, MAX_CARD_LENGTH) + '...'
									: item.guidance}
							</Text>
						)}

						{isSelected && (
							<Flex flexDirection="column" spaceSize="m">
								<HorizontalSeparationLine />
								{item.guidance && <Text>{item.guidance}</Text>}
								{templateMode && (
									<Flex spaceSize="s" justifyContent="flex-end">
										<FxOperationButton
											label=""
											type="transparent"
											isBorderless
											operationName="remove-author-guidance"
											operationData={item}
										/>
										<FxOperationButton
											label=""
											type="transparent"
											isBorderless
											operationName="open-modal-and-edit-guidance"
											operationData={{ oldGuidance: item, isEdit: true }}
										/>
									</Flex>
								)}
							</Flex>
						)}
					</Card>
				</Flex>
			);
		},
		[selectedGuidance, templateMode]
	);

	function handleItemClick(guidance) {
		authorGuidancesManager.scrollToGuidance(guidance).then(() => setSelectedGuidance(guidance));
	}

	return (
		<Fragment>
			{!guidances || guidances.length === 0 ? (
				<AuthorGuidancesStateMessage />
			) : (
				<VirtualForest
					estimatedItemHeight={35}
					forest={guidances}
					itemKeyToUseAsId="id"
					renderItem={renderItem}
					onItemClick={handleItemClick}
					idToScrollIntoView={selectedGuidance && selectedGuidance.id}
				/>
			)}
		</Fragment>
	);
}
