import React, { Fragment } from 'react';
import { Flex, SidebarHeader } from 'fds/components';
import t from 'fontoxml-localization/src/t.js';
import FxOperationButton from 'fontoxml-fx/src/FxOperationButton.jsx';

import modeSwitchingManager from 'fonto-html-mode-switching/src/api/modeSwitchingManager.js';

import AuthorGuidancesForm from './AuthorGuidancesForm.jsx';

export default function AuthorGuidancesSidebar() {
	const mode = modeSwitchingManager.mode;
	const title = mode === 'template' ? t('Author guidances') : t('Guidances');

	return (
		<Fragment>
			{mode === 'template' ? (
				<SidebarHeader
					title={title}
					button={
						<FxOperationButton
							operationName="insert-author-guidance"
							label={t('Add guidance')}
							icon="sticky-note"
						/>
					}
				/>
			) : (
				<SidebarHeader title={title} />
			)}
			<Flex flex="1" flexDirection="column" spaceSize="m">
				<AuthorGuidancesForm />
			</Flex>
		</Fragment>
	);
}
