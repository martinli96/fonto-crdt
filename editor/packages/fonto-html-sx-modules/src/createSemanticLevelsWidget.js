import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import applyCss from 'fontoxml-styles/src/applyCss.js';
import t from 'fontoxml-localization/src/t.js';

const levels = [
	{
		type: 'document',
		label: 'd',
		tooltipContent: t('Can the content type be used as document type'),
		attributeName: 'data-document-type'
	},
	{
		type: 'section',
		label: 's',
		tooltipContent: t('Can the content type be used as section type'),
		attributeName: 'data-section-type'
	},
	{
		type: 'block',
		label: 'b',
		tooltipContent: t('Can the content type be used as block type'),
		attributeName: 'data-block-type'
	}
];

const operationName = 'set-semantic-level-attribute-value';

export default function createSemanticLevelsWidget() {
	return function createSemanticLevelsWidget(sourceNode) {
		const jsonMl = ['cv-inline-frame', applyCss({ float: 'right' })];

		const parentSemanticBlockNode = sourceNode.findRelatedNodes(function(sourceNodeProxy) {
			const parentSemanticBlockNode = evaluateXPathToFirstNode(
				'parent::li/parent::ul/preceding-sibling::p[1]',
				sourceNodeProxy,
				readOnlyBlueprint
			);

			return parentSemanticBlockNode ? [parentSemanticBlockNode] : [];
		})[0];

		levels.forEach(function(level) {
			if (parentSemanticBlockNode) {
				if (level.type === 'document') {
					return;
				}

				// if the parent is not document level or section level then this level can't be chosen
				if (
					level.type === 'section' &&
					evaluateXPathToBoolean(
						'not(@data-document-type = true() or @data-section-type = true())',
						parentSemanticBlockNode,
						readOnlyBlueprint
					)
				) {
					return;
				}

				// if the parent is not document level, section level or block level then this level can't be chosen
				if (
					level.type === 'block' &&
					evaluateXPathToBoolean(
						'not(@data-document-type = true() or @data-section-type = true() or @data-block-type = true())',
						parentSemanticBlockNode,
						readOnlyBlueprint
					)
				) {
					return;
				}
			}

			const levelValue = evaluateXPathToBoolean(
				'@' + level.attributeName + ' = true()',
				sourceNode,
				readOnlyBlueprint
			);

			jsonMl.push([
				'cv-inline-label-widget',
				{ 'tooltip-content': level.tooltipContent },
				level.label
			]);

			jsonMl.push([
				'cv-icon-widget',
				{
					'tooltip-content': level.tooltipContent,
					'operation-name': operationName,
					'operation-context-node-id': getNodeId(sourceNode),
					'operation-initial-data': JSON.stringify({
						level: level.type,
						value: '' + !levelValue
					})
				},
				[
					'cv-icon',
					{
						class: levelValue ? 'far fa-check-square' : 'far fa-square'
					}
				]
			]);

			jsonMl.push(['cv-inline-label-widget', ' ']);
		});

		return jsonMl;
	};
}
