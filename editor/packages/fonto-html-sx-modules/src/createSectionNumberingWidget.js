import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import applyCss from 'fontoxml-styles/src/applyCss.js';

export default function createSectionNumberingWidget(labelQuery, correctionValue) {
	return function createSectionNumberingWidgetJsonMl(sourceNode) {
		return [
			'cv-label-widget',
			Object.assign(
				{ 'cv-font-stack': 'interface' },
				applyCss({
					transform: 'translate(calc(-100% - 8px), ' + correctionValue + ')',
					marginBottom: '-28px'
				})
			),
			evaluateXPathToString(labelQuery, sourceNode, readOnlyBlueprint)
		];
	};
}
