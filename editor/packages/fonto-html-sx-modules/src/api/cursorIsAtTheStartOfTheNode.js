function checkStartParents(startElement, element, blueprint) {
	if (startElement === element) {
		return true;
	} else if (blueprint.getPreviousSibling(startElement)) {
		return false;
	}

	return checkStartParents(blueprint.getParentNode(startElement), element, blueprint);
}

/*
 * @param {Node}           element   The element that is checked
 * @param {BlueprintRange} range     The range that will be searched
 * @param {blueprint}      blueprint
 */
export default function cursorIsAtTheStartOfTheNode(element, container, offset, blueprint) {
	if (container === element && offset === 0) {
		return true;
	} else if (offset === 0) {
		return checkStartParents(blueprint.getParentNode(container), element, blueprint);
	}

	return false;
}
