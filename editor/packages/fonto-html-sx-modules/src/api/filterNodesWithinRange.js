export default function filterNodesWithinRange(
	nodes,
	range,
	predefinedStartIndex,
	predefinedEndIndex
) {
	let startIndex = predefinedStartIndex || -1;
	let endIndex = predefinedEndIndex || -1;
	let startNode = range.startContainer;
	let endNode = range.endContainer;

	while (startIndex === -1 && startNode) {
		startIndex = nodes.indexOf(startNode, 0);
		startNode = startNode.parentElement;
	}

	while (endIndex === -1 && endNode) {
		endIndex = nodes.indexOf(endNode, startIndex > 0 ? startIndex : 0);
		endNode = endNode.parentElement;
	}

	return nodes.slice(startIndex, endIndex + 1);
}
