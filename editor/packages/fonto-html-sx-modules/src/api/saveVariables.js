import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';

function replaceValueContent(variableNode, newContent, blueprint) {
	let valueNode = evaluateXPathToFirstNode('./dd', variableNode, blueprint);
	if (!valueNode) {
		valueNode = namespaceManager.createElement(variableNode, 'dd');
		blueprint.appendChild(variableNode, valueNode);
	} else {
		const childNodes = evaluateXPathToNodes('./node()', valueNode, blueprint);
		childNodes.forEach(function(childNode) {
			blueprint.removeChild(valueNode, childNode);
		});
	}

	const newContentNode = valueNode.ownerDocument.createTextNode(newContent);
	blueprint.appendChild(valueNode, newContentNode);
}

export default function saveVariables(argument, blueprint) {
	const contextNode = blueprint.lookup(argument.contextNodeId);

	if (!contextNode || !blueprintQuery.isInDocument(blueprint, contextNode)) {
		return CustomMutationResult.notAllowed();
	}

	const headNode = evaluateXPathToFirstNode('//html/head', contextNode, blueprint);
	if (!headNode) {
		return CustomMutationResult.notAllowed();
	}

	const removeVariables = !argument.variables || argument.variables.length === 0;

	let templateNode = evaluateXPathToFirstNode('./template', headNode, blueprint);

	if (!templateNode) {
		if (removeVariables) {
			return CustomMutationResult.ok();
		}
		templateNode = namespaceManager.createElement(headNode, 'template');
		const referenceNodeForTemplate = evaluateXPathToFirstNode(
			'child::*[not(self::template-ref)][preceding-sibling::*[1][self::template-ref or self::title]]',
			headNode,
			blueprint
		);
		blueprint.insertBefore(headNode, templateNode, referenceNodeForTemplate);
	}

	let dlNode = evaluateXPathToFirstNode('./dl', templateNode, blueprint);

	if (!dlNode) {
		if (removeVariables) {
			return CustomMutationResult.ok();
		}
		dlNode = namespaceManager.createElement(headNode, 'dl');
		blueprint.appendChild(templateNode, dlNode);
	} else if (removeVariables) {
		blueprint.removeChild(templateNode, dlNode);
		return CustomMutationResult.ok();
	}

	const existingVariableNodes = evaluateXPathToNodes('./di', dlNode, blueprint);
	let referenceNode = existingVariableNodes[0] || null;
	argument.variables.forEach(variable => {
		const existingVariableNodeIndex = existingVariableNodes.findIndex(variableNode =>
			evaluateXPathToBoolean('./dt/string() = $name', variableNode, blueprint, {
				name: variable.name
			})
		);
		if (existingVariableNodeIndex !== -1) {
			// Preserve variable as it is still used
			const existingVariableNode = existingVariableNodes[existingVariableNodeIndex];

			// Check if the value is still the same
			if (
				!evaluateXPathToBoolean('./dd/string() = $value', existingVariableNode, blueprint, {
					value: variable.value
				})
			) {
				// Update the value of the variable
				replaceValueContent(existingVariableNode, variable.value, blueprint);
			}

			if (
				evaluateXPathToBoolean(
					'./dt/string() = $referenceNode/dt/string()',
					existingVariableNode,
					blueprint,
					{ referenceNode: referenceNode }
				)
			) {
				// The variable is already in the correct spot. New variables should be added after this one.
				referenceNode = evaluateXPathToFirstNode(
					'following-sibling::di[1]',
					existingVariableNode,
					blueprint
				);
			} else {
				// Move the gapref in the correct order
				blueprintMutations.unsafeMoveNodes(
					existingVariableNode,
					existingVariableNode,
					blueprint,
					dlNode,
					referenceNode,
					true
				);
			}
			existingVariableNodes.splice(existingVariableNodeIndex, 1);
		} else {
			// Create a new variable
			const variableNode = namespaceManager.createElement(headNode, 'di');
			blueprint.insertBefore(dlNode, variableNode, referenceNode);
			const nameNode = namespaceManager.createElement(headNode, 'dt');
			blueprint.appendChild(variableNode, nameNode);
			const newNameContentNode = nameNode.ownerDocument.createTextNode(variable.name);
			blueprint.appendChild(nameNode, newNameContentNode);
			const valueNode = namespaceManager.createElement(headNode, 'dd');
			blueprint.appendChild(variableNode, valueNode);
			const newValueContentNode = valueNode.ownerDocument.createTextNode(variable.value);
			blueprint.appendChild(valueNode, newValueContentNode);
		}
	});

	// Remove existing variables when they aren't used any more
	existingVariableNodes.forEach(existingVariableNode => {
		blueprint.removeChild(dlNode, existingVariableNode);
	});

	return CustomMutationResult.ok();
}
