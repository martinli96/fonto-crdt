import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';

export default function indentSectionHelper(sectionNode, blueprint) {
	const newParentNode = evaluateXPathToFirstNode(
		'preceding-sibling::*[1][self::section or self::document-ref]',
		sectionNode,
		blueprint
	);

	if (!newParentNode) {
		return false;
	}

	blueprintMutations.unsafeMoveNodes(
		sectionNode,
		sectionNode,
		blueprint,
		newParentNode,
		null,
		false
	);

	return true;
}
