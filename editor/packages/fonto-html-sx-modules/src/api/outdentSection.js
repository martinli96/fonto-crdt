import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import outdentSectionHelper from './outdentSectionHelper.js';

export default function outdentSection(argument, blueprint) {
	const sectionNode = blueprint.lookup(argument.contextNodeId);

	if (!sectionNode || !blueprintQuery.isInDocument(blueprint, sectionNode)) {
		return CustomMutationResult.notAllowed();
	}

	return outdentSectionHelper(sectionNode, blueprint)
		? CustomMutationResult.ok()
		: CustomMutationResult.notAllowed();
}
