import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';
import cursorIsAtTheEndOfTheNode from './cursorIsAtTheEndOfTheNode.js';

export default function insertSemantic(argument, blueprint, _format, selection) {
	const contextNode = blueprint.lookup(argument.contextNodeId);

	if (!contextNode || !blueprintQuery.isInDocument(blueprint, contextNode)) {
		return CustomMutationResult.notAllowed();
	}

	let newNode = null;
	const parentNode = blueprint.getParentNode(contextNode);

	if (
		cursorIsAtTheEndOfTheNode(
			contextNode,
			selection.startContainer,
			selection.startOffset,
			blueprint
		)
	) {
		newNode = namespaceManager.createElement(contextNode, 'span');
		blueprint.insertBefore(parentNode, newNode, blueprint.getNextSibling(contextNode));
	} else {
		newNode = blueprintMutations.unsafeSplitUntil(
			selection.startContainer,
			selection.startOffset,
			parentNode,
			blueprint
		);
	}

	if (newNode) {
		selection.setStart(newNode, 0);
		selection.setEnd(newNode, 0);
	}

	return CustomMutationResult.ok();
}
