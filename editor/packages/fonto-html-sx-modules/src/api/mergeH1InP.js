import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';
import convertH1InPHelper from './convertH1InPHelper.js';

// Goal:
// backspace from beginning of (next) h1 into previous p
// delete from end of (previous) p into next h1
// section around h1 is unwrapped
// text in h1 is appended to end of p
// all content of section (except h1) is appended to end of section (around previous p)

export default function MergeH1InP(argument, blueprint, format, selection) {
	const contextNode = blueprint.lookup(argument.contextNodeId);
	if (!contextNode || !blueprintQuery.isInDocument(blueprint, contextNode)) {
		return CustomMutationResult.notAllowed();
	}

	// Test if the cursor is collapsed and if the parent of the cursor is the element we want to merge
	if (!selection.collapsed || contextNode !== selectionManager.getSelectedElement()) {
		return CustomMutationResult.notAllowed();
	}

	let h1Node = null;

	// backspace
	if (!argument.isForwards) {
		// If the cursor is not in an h1 element or not on the first possible position in the h1
		const firstPosition = evaluateXPathToNumber(
			'count(child::node()[self::comment() or self::processing-instruction()' +
				'[name() != "fontoxml-text-placeholder" and not(name() => starts-with("fontoxml-annotation"))]' +
				'][not(preceding-sibling::node()[self::* or self::text() or self::processing-instruction()' +
				'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]' +
				'])])',
			contextNode,
			blueprint
		);
		if (
			contextNode.nodeName !== 'h1' ||
			(contextNode !== selection.startContainer &&
				(evaluateXPathToBoolean(
					'preceding-sibling::node()[self::* or self::text() or self::processing-instruction()' +
						'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]]',
					selection.startContainer,
					blueprint
				) ||
					selection.startOffset !== 0)) ||
			(contextNode === selection.startContainer && selection.startOffset > firstPosition)
		) {
			return CustomMutationResult.notAllowed();
		}

		h1Node = contextNode;
	}
	// delete
	else {
		// If the cursor is not in a p or the p in not the last element in the section
		// or the p is contained by another element then li, ul, ol, section or body
		// or the cursor is not in the last possible position
		const lastPosition = evaluateXPathToNumber(
			'count(child::node()[not(self::comment() or self::processing-instruction()' +
				'[name() != "fontoxml-text-placeholder" and not(name() => starts-with("fontoxml-annotation"))]' +
				' and not(following-sibling::node()[self::* or self::text() or self::processing-instruction()' +
				'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]' +
				']))])',
			contextNode,
			blueprint
		);
		if (
			contextNode.nodeName !== 'p' ||
			evaluateXPathToBoolean(
				'ancestor-or-self::*[not(descendant-or-self::*[self::section or self::body])]' +
					'[' +
					'following-sibling::*[1][not(self::section)] or ' +
					'not(self::p or self::li or self::ol or self::ul)' +
					']',
				contextNode,
				blueprint
			) ||
			(contextNode !== selection.endContainer &&
				(evaluateXPathToBoolean(
					'following-sibling::node()[self::* or self::text() or self::processing-instruction()' +
						'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]]',
					selection.endContainer,
					blueprint
				) ||
					selection.endOffset !== selection.endContainer.data.length)) ||
			(contextNode === selection.endContainer && selection.endOffset < lastPosition)
		) {
			return CustomMutationResult.notAllowed();
		}

		h1Node = evaluateXPathToFirstNode(
			'(' +
				'following-sibling::section | ' +
				'ancestor-or-self::*[parent::section or parent::body]/following-sibling::section' +
				')/h1',
			contextNode,
			blueprint
		);

		if (!h1Node) {
			return CustomMutationResult.notAllowed();
		}
	}

	const newPNode = convertH1InPHelper(h1Node, blueprint, format);
	if (!newPNode) {
		return CustomMutationResult.notAllowed();
	}

	// Set the cursor at the start of the new p node to prevent cursor in unexpected place
	// (in between/below sections)
	selection.setStart(newPNode, 0);
	selection.setEnd(newPNode, 0);

	// if the new p node has an appropriate preceding sibling to merge into
	const precedingSiblingNode = evaluateXPathToFirstNode(
		// a preceding p | a preceding list's last item's p
		'preceding-sibling::*[1][self::p] | preceding-sibling::*[1][self::ul or self::ol]/descendant::li[last()]/p[last()]',
		newPNode,
		blueprint
	);

	if (precedingSiblingNode) {
		const firstChild = blueprint.getFirstChild(newPNode);
		if (firstChild) {
			// only merge the p node into this preceding sibling when it is not empty
			blueprintMutations.unsafeMoveNodes(
				firstChild,
				blueprint.getLastChild(newPNode),
				blueprint,
				precedingSiblingNode,
				null,
				true
			);
		}

		blueprint.removeChild(blueprint.getParentNode(newPNode), newPNode);
	}

	return CustomMutationResult.ok();
}
