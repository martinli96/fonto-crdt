import generateUUID from 'fontoxml-uuid-generator/src/generateUUID.js';
import parsePseudoAttributes from 'fontoxml-dom-utils/src/parsePseudoAttributes.js';
import serializePseudoAttributes from 'fontoxml-dom-utils/src/serializePseudoAttributes.js';

export default class AuthorGuidance {
	constructor(instruction, guidance, url, id) {
		this.instruction = instruction;
		this.guidance = guidance;
		this.url = url;
		this.id = id ? id : generateUUID();
	}

	getSerializedData() {
		const attributes = {
			id: this.id,
			instruction: this.instruction,
			guidance: this.guidance,
			url: this.url
		};

		return serializePseudoAttributes(attributes);
	}

	static createFromMarker(marker) {
		const attributes = parsePseudoAttributes(marker.data);

		return new AuthorGuidance(
			attributes['instruction'],
			attributes['guidance'] === '__null' ? null : attributes['guidance'],
			attributes['url'] === '__null' ? null : attributes['url'],
			attributes['id']
		);
	}
}
