import blueprints from 'fontoxml-blueprints/src/main.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';

import AuthorGuidance from './AuthorGuidance.js';

const Blueprint = blueprints.Blueprint;

export default function getAllAuthorGuidances(documentId) {
	if (!documentId) {
		return null;
	}
	const documentController = documentsManager.getDocumentController(documentId);
	if (!documentController) {
		return null;
	}
	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);

	const markers = evaluateXPathToNodes(
		'//processing-instruction()[name() => starts-with("author-guidance")]',
		document,
		blueprint
	);

	if (!markers || !markers.length) {
		return [];
	}

	const guidancesToReturn = new Array();
	markers.forEach(marker => guidancesToReturn.push(AuthorGuidance.createFromMarker(marker)));

	return guidancesToReturn;
}
