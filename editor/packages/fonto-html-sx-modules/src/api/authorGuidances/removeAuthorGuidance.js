import blueprints from 'fontoxml-blueprints/src/main.js';
import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';

const Blueprint = blueprints.Blueprint;

export default function removeAuthorGuidance(guidance, documentId) {
	if (!documentCapabilitiesManager.hasCapability(documentId, 'editable')) {
		return false;
	}

	const documentController = documentsManager.getDocumentController(documentId);
	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);
	const format = documentController.document.schemaExperience.format;

	const guidancesToRemove = evaluateXPathToNodes(
		[
			'//processing-instruction()[',
			'name() => starts-with("author-guidance") and ',
			'(fonto:get-pseudo-attribute(string(.), "id")) = $id]'
		].join(''),
		document,
		blueprint,
		{ id: guidance.id }
	);

	if (!guidancesToRemove.length) {
		return false;
	}

	blueprint.beginOverlay();

	guidancesToRemove.forEach(guidance =>
		blueprint.removeChild(blueprint.getParentNode(guidance), guidance)
	);

	if (!blueprint.isValid(format.validator)) {
		blueprint.discardOverlay();
		return false;
	}

	blueprint.applyOverlay();
	documentController.executeFunction(() => blueprint.realize(), {});

	return true;
}
