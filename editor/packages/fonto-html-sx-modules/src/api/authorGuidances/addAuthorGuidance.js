import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';

import baseFlow from 'fontoxml-base-flow/src/main.js';
import blueprints from 'fontoxml-blueprints/src/main.js';
import AuthorGuidance from './AuthorGuidance.js';

const Blueprint = blueprints.Blueprint,
	BlueprintRange = blueprints.BlueprintRange,
	BlueprintPosition = blueprints.BlueprintPosition,
	insertNodes = baseFlow.primitives.insertNodes;

export default function addAuthorGuidance(data, documentId) {
	if (!documentCapabilitiesManager.hasCapability(documentId, 'editable')) {
		return false;
	}

	const authorGuidance = new AuthorGuidance(data.instruction, data.guidance, data.url);

	const documentController = documentsManager.getDocumentController(documentId);
	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);
	const format = documentController.document.schemaExperience.format;
	const selectionRange = documentController.document.dom.selectionRange;
	const range = BlueprintRange.createFromRange(blueprint, selectionRange);

	const authorProcessingInstruction = document.createProcessingInstruction(
		'author-guidance',
		authorGuidance.getSerializedData()
	);

	blueprint.beginOverlay();

	const position = BlueprintPosition.fromOffset(range.endContainer, range.endOffset, blueprint);

	if (!insertNodes(position, [authorProcessingInstruction], blueprint, null, format)) {
		blueprint.discardOverlay();
		return null;
	}

	blueprint.applyOverlay();
	documentController.executeFunction(() => blueprint.realize(), {});

	return authorGuidance;
}
