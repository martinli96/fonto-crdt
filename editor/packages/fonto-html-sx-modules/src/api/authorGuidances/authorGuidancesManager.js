import Notifier from 'fontoxml-utils/src/Notifier.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import operationsManager from 'fontoxml-operations/src/operationsManager.js';

import addAuthorGuidance from './addAuthorGuidance.js';
import getAllAuthorGuidances from './getAllAuthorGuidances.js';
import getAuthorGuidanceWithContextNodeId from './getAuthorGuidanceWithContextNodeId.js';
import editAuthorGuidance from './editAuthorGuidance.js';
import removeAuthorGuidance from './removeAuthorGuidance.js';

function getDocumentId() {
	const documents = documentsHierarchy.findAll(() => true);
	if (documents && documents.length > 0 && documents[0].documentReference) {
		return documents[0].documentReference.documentId;
	}

	return null;
}

class AuthorGuidancesManager {
	constructor() {
		this.authorGuidances = null;
		this.selectedAuthorGuidance = null;

		this.authorGuidancesChangedNotifier = new Notifier();
		this.selectedAuthorGuidancesChangedNotifier = new Notifier();

		this.authorGuidancesChangedNotifier.addCallback(() => this.fetchAuthorGuidances());
	}

	fetchAuthorGuidances() {
		this.authorGuidances = getAllAuthorGuidances(getDocumentId());
	}

	addGuidance(guidance) {
		if (addAuthorGuidance(guidance, getDocumentId())) {
			this.authorGuidancesChangedNotifier.executeCallbacks();
		}
	}

	editGuidance(oldGuidance, newGuidance) {
		if (editAuthorGuidance(oldGuidance, newGuidance, getDocumentId())) {
			this.authorGuidancesChangedNotifier.executeCallbacks();
		}
	}

	removeGuidance(guidance) {
		if (removeAuthorGuidance(guidance, getDocumentId())) {
			this.authorGuidancesChangedNotifier.executeCallbacks();
		}
	}

	getGuidanceWithContextNodeId(id) {
		return getAuthorGuidanceWithContextNodeId(id, getDocumentId());
	}

	getGuidances() {
		if (!this.authorGuidances) {
			this.fetchAuthorGuidances();
		}

		return this.authorGuidances;
	}

	scrollToGuidance(guidance) {
		const guidanceWithContextNodeId = this.getGuidanceWithContextNodeId(guidance.id);
		return operationsManager
			.executeOperation('scroll-node-into-view', {
				contextNodeId: guidanceWithContextNodeId.contextNodeId
			})
			.then(() => {
				operationsManager.executeOperation('select-node', {
					contextNodeId: guidanceWithContextNodeId.contextNodeId
				});
			});
	}

	selectAuthorGuidance(authorGuidanceId) {
		if (this.authorGuidances && this.authorGuidances.length) {
			const index = this.authorGuidances.findIndex(
				guidance => guidance.id === authorGuidanceId
			);
			this.selectedAuthorGuidance = index !== -1 ? this.authorGuidances[index] : null;
			this.selectedAuthorGuidancesChangedNotifier.executeCallbacks();
		}
	}

	getSelectedAuthorGuidance() {
		return this.selectedAuthorGuidance;
	}
}

export default new AuthorGuidancesManager();
