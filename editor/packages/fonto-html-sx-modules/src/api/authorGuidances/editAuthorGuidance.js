import blueprints from 'fontoxml-blueprints/src/main.js';
import documentCapabilitiesManager from 'fontoxml-documents/src/documentCapabilitiesManager.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';

import AuthorGuidance from './AuthorGuidance.js';

const Blueprint = blueprints.Blueprint;

export default function editAuthorGuidance(oldGuidance, newGuidance, documentId) {
	if (!documentCapabilitiesManager.hasCapability(documentId, 'editable')) {
		return false;
	}

	const documentController = documentsManager.getDocumentController(documentId);
	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);
	const format = documentController.document.schemaExperience.format;

	let marker = evaluateXPathToNodes(
		[
			'//processing-instruction()[',
			'name() => starts-with("author-guidance") and ',
			'(fonto:get-pseudo-attribute(string(.), "id")) = $id]'
		].join(''),
		document,
		blueprint,
		{ id: oldGuidance.id }
	);

	if (!marker || !marker.length) {
		return null;
	}

	marker = marker[0];

	const guidance = AuthorGuidance.createFromMarker(marker);
	guidance.guidance = newGuidance.guidance;
	guidance.instruction = newGuidance.instruction;
	guidance.url = newGuidance.url;

	blueprint.beginOverlay();
	blueprint.setData(marker, guidance.getSerializedData());

	if (!blueprint.isValid(format.validator)) {
		blueprint.discardOverlay();
		return null;
	}

	blueprint.applyOverlay();
	documentController.executeFunction(() => blueprint.realize(), {});

	return guidance;
}
