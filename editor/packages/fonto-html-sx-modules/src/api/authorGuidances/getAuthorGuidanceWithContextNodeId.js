import blueprints from 'fontoxml-blueprints/src/main.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';

import AuthorGuidance from './AuthorGuidance.js';

const Blueprint = blueprints.Blueprint;

export default function getAuthorGuidanceWithContextNodeId(guidanceId, documentId) {
	const documentController = documentsManager.getDocumentController(documentId);
	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);

	const marker = evaluateXPathToNodes(
		[
			'//processing-instruction()[',
			'name() => starts-with("author-guidance") and ',
			'(fonto:get-pseudo-attribute(string(.), "id")) = $id]'
		].join(''),
		document,
		blueprint,
		{ id: guidanceId }
	);

	if (!marker || !marker.length) {
		return null;
	}

	const contextNodeId = getNodeId(marker[0].parentNode);
	const guidance = AuthorGuidance.createFromMarker(marker[0]);

	return {
		...guidance,
		contextNodeId
	};
}
