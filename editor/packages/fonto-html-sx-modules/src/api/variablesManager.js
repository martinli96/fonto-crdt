import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPath from 'fontoxml-selectors/src/evaluateXPath.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import indexManager from 'fontoxml-indices/src/indexManager.js';
import Notifier from 'fontoxml-utils/src/Notifier.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import externalDataManager from 'fontoxml-templated-views/src/externalDataManager.js';

import hierarchyNodesLoader from 'fonto-html-multi-document/src/api/hierarchyNodesLoader.js';

function mapVariableNodes(variableNode) {
	const valueNode = evaluateXPathToFirstNode('./dd', variableNode, readOnlyBlueprint);

	return {
		name: evaluateXPathToString('./dt/string()', variableNode, readOnlyBlueprint),
		value: valueNode ? evaluateXPathToString('./string()', valueNode, readOnlyBlueprint) : null
	};
}

function VariablesManager() {
	this.externalVariables = [];
	this.currentVariables = [];
	this._currentVariablesObserver = null;
	this._currentVariablesChangedNotifier = new Notifier();
	this._removeVariablesChangedCallback = null;
	this._firstHierarchyNodeId = null;

	hierarchyNodesLoader.templateDocumentIdChangedNotifier.addCallback(() => {
		this.externalVariables.forEach(oldVariable => {
			if (oldVariable.value !== null) {
				externalDataManager.setExternalData(
					'variable-value-by-name|' + oldVariable.name,
					undefined
				);
			}
		});
		this.externalVariables = [];
		const templateDocumentId = hierarchyNodesLoader.templateDocumentId;
		const templateDocumentNode = templateDocumentId
			? documentsManager.getDocumentNode(templateDocumentId)
			: null;
		if (!templateDocumentNode) {
			return;
		}
		this.externalVariables = evaluateXPathToNodes(
			'//html/head/template[1]/dl/di',
			templateDocumentNode,
			readOnlyBlueprint
		).map(mapVariableNodes);

		this.externalVariables.forEach(variable => {
			const currentVariable = this.currentVariables.find(
				currentVariable => currentVariable.name === variable.name
			);
			if (!currentVariable) {
				externalDataManager.setExternalData(
					'variable-value-by-name|' + variable.name,
					variable.value
				);
			}
		});
	});

	documentsHierarchy.hierarchyChangedNotifier.addCallback(
		function() {
			const firstHierarchyNode = documentsHierarchy.getFirstVisibleHierarchyNode();
			if (
				!firstHierarchyNode ||
				!firstHierarchyNode.documentReference ||
				!firstHierarchyNode.documentReference.documentId
			) {
				return;
			}

			const firstHierarchyNodeId = firstHierarchyNode.getId();

			if (firstHierarchyNodeId === this.firstHierarchyNodeId) {
				return;
			}

			if (this._removeVariablesChangedCallback) {
				this._currentVariablesObserver.stopObservingQueryResults();
				this._currentVariablesObserver = null;
				this._removeVariablesChangedCallback();
			}

			const globalDocumentNode = documentsManager.getDocumentNode(
				firstHierarchyNode.documentReference.documentId
			);

			this._currentVariablesObserver = indexManager.observeQueryResults(
				'//html/head/template[1]/dl/di',
				globalDocumentNode,
				{
					expectedResultType: evaluateXPath.NODES_TYPE
				}
			);
			this._resultsHaveChanged(this._currentVariablesObserver.getResults());
			this._removeVariablesChangedCallback = this._currentVariablesObserver.resultsChangedNotifier.addCallback(
				function() {
					this._resultsHaveChanged(this._currentVariablesObserver.getResults());
				}.bind(this)
			);
		}.bind(this)
	);
}

VariablesManager.prototype._resultsHaveChanged = function(currentVariableNodes) {
	const newCurrentVariables = currentVariableNodes.map(mapVariableNodes);

	let isStillTheSame = true;

	const removedVariables = this.currentVariables.concat();

	newCurrentVariables.forEach(newVariable => {
		const currentVariableIndex = removedVariables.findIndex(
			variable => variable.name === newVariable.name
		);
		const currentVariable = removedVariables[currentVariableIndex];
		if (!currentVariable || currentVariable.value !== newVariable.value) {
			externalDataManager.setExternalData(
				'variable-value-by-name|' + newVariable.name,
				newVariable.value
			);
			isStillTheSame = false;
		}
		if (currentVariableIndex !== -1) {
			removedVariables.splice(currentVariableIndex, 1);
		}
	});

	if (
		Object.keys(newCurrentVariables).length !== this.currentVariables.length ||
		!isStillTheSame
	) {
		this._currentVariablesChangedNotifier.executeCallbacks();
		// check if variables are removed
		removedVariables.forEach(removedVariable => {
			const externalVariable = this.externalVariables.find(
				variable => variable.name === removedVariable.name
			);
			externalDataManager.setExternalData(
				'variable-value-by-name|' + removedVariable.name,
				!externalVariable ? undefined : null
			);
		});
		this.currentVariables = newCurrentVariables;
	}
};

VariablesManager.prototype.getVariables = function() {
	return this.externalVariables.map(externalVariable => {
		if (externalVariable.value !== null) {
			return externalVariable;
		}
		const currentVariable = this.currentVariables.find(
			variable => variable.name === externalVariable.name
		);

		return {
			name: externalVariable.name,
			value: currentVariable ? currentVariable.value : null
		};
	});
};

export default new VariablesManager();
