import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import filterNodesWithinRange from './filterNodesWithinRange.js';

const unsafeMoveNodes = blueprintMutations.unsafeMoveNodes;

export default function indentListItems(argument, blueprint, format, selection) {
	const selectionCommonAncestorNode = blueprint.lookup(argument.contextNodeId);

	// Only use this custom mutation for semantics/content types.
	if (
		!selectionCommonAncestorNode ||
		!evaluateXPathToBoolean('ancestor::template', selectionCommonAncestorNode, blueprint)
	) {
		return CustomMutationResult.notAllowed();
	}

	let listItemNodes = null;
	if (domInfo.isElement(selectionCommonAncestorNode, 'li')) {
		listItemNodes = [selectionCommonAncestorNode];
	} else if (domInfo.isElement(selectionCommonAncestorNode, ['ol', 'ul'])) {
		listItemNodes = filterNodesWithinRange(selectionCommonAncestorNode.childNodes, selection);
	} else {
		// not in a list or list item
		return CustomMutationResult.notAllowed();
	}

	if (listItemNodes.length === 0) {
		return CustomMutationResult.notAllowed();
	}

	const firstListItemNode = listItemNodes[0];
	const listNode = blueprint.getParentNode(firstListItemNode);

	const previousSiblingListItem = evaluateXPathToFirstNode(
		'preceding-sibling::*[fonto:list-item(.)][1]',
		firstListItemNode,
		blueprint
	);
	if (!previousSiblingListItem) {
		return CustomMutationResult.notAllowed();
	}

	// Some extra behaviour for a semantics list

	const parentSemanticBlockNode = evaluateXPathToFirstNode(
		'child::p',
		previousSiblingListItem,
		blueprint
	);
	let highestLevel = null;
	// if parent has inline level
	if (evaluateXPathToBoolean('@data-inline-type = true()', parentSemanticBlockNode, blueprint)) {
		highestLevel = 'inline';
	}
	// if parent has block level
	if (evaluateXPathToBoolean('@data-block-type = true()', parentSemanticBlockNode, blueprint)) {
		highestLevel = 'block';
	}
	// if parent has section level or document level
	if (
		evaluateXPathToBoolean(
			'@data-section-type = true() or @data-document-type = true()',
			parentSemanticBlockNode,
			blueprint
		)
	) {
		highestLevel = 'section';
	}

	listItemNodes.forEach(function(listItemNode) {
		const semanticBlockNode = evaluateXPathToFirstNode('child::p', listItemNode, blueprint);

		blueprint.removeAttribute(semanticBlockNode, 'data-document-type');
		if (highestLevel === 'section') {
			return;
		}
		blueprint.removeAttribute(semanticBlockNode, 'data-section-type');
		if (highestLevel === 'block') {
			return;
		}
		blueprint.removeAttribute(semanticBlockNode, 'data-block-type');
		if (highestLevel === 'inline') {
			return;
		}
		blueprint.removeAttribute(semanticBlockNode, 'data-inline-type');
	});

	// If the preceding list item ends with a list, merge them
	let nestedListNode = evaluateXPathToFirstNode(
		'child::*[last()][fonto:list(.)]',
		previousSiblingListItem,
		blueprint
	);
	if (!nestedListNode) {
		nestedListNode = blueprint.cloneNode(listNode, false);
		blueprint.appendChild(previousSiblingListItem, nestedListNode);
	}

	unsafeMoveNodes(
		listItemNodes[0],
		listItemNodes[listItemNodes.length - 1],
		blueprint,
		nestedListNode,
		null,
		true
	);

	if (!format.synthesizer.completeStructure(listNode, blueprint)) {
		return CustomMutationResult.notAllowed();
	}

	return CustomMutationResult.ok();
}
