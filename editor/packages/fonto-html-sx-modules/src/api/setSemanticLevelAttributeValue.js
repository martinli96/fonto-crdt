import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';

function determineHighestLevel(semanticBlockNode, blueprint) {
	let highestLevel = null;
	if (evaluateXPathToBoolean('@data-inline-type = true()', semanticBlockNode, blueprint)) {
		highestLevel = 'inline';
	}
	if (evaluateXPathToBoolean('@data-block-type = true()', semanticBlockNode, blueprint)) {
		highestLevel = 'block';
	}
	if (evaluateXPathToBoolean('@data-section-type = true()', semanticBlockNode, blueprint)) {
		highestLevel = 'section';
	}
	if (evaluateXPathToBoolean('@data-document-type = true()', semanticBlockNode, blueprint)) {
		highestLevel = 'document';
	}

	return highestLevel;
}

function removeAttributeOfDescendantSemanticBlocks(highestLevel, semanticBlockNode, blueprint) {
	const childSemanticBlockNodes = evaluateXPathToNodes(
		'following-sibling::ul/li/p',
		semanticBlockNode,
		blueprint
	);

	childSemanticBlockNodes.forEach(function(childSemanticBlockNode) {
		const childHighestLevel = determineHighestLevel(childSemanticBlockNode, blueprint);

		blueprint.removeAttribute(childSemanticBlockNode, 'data-document-type');
		if (highestLevel !== 'section') {
			blueprint.removeAttribute(childSemanticBlockNode, 'data-section-type');
			if (highestLevel !== 'block') {
				blueprint.removeAttribute(childSemanticBlockNode, 'data-block-type');
				if (highestLevel !== 'inline') {
					blueprint.removeAttribute(childSemanticBlockNode, 'data-inline-type');
				}
			}
		}
		const newHighestLevel = determineHighestLevel(childSemanticBlockNode, blueprint);
		if (childHighestLevel === newHighestLevel) {
			return;
		}

		removeAttributeOfDescendantSemanticBlocks(
			newHighestLevel,
			childSemanticBlockNode,
			blueprint
		);
	});
}

export default function setSemanticLevelAttributeValue(argument, blueprint) {
	if (!argument.level || (argument.value !== 'false' && argument.value !== 'true')) {
		return CustomMutationResult.notAllowed();
	}

	const semanticBlockNode = blueprint.lookup(argument.contextNodeId);
	// if the contextNodeId lookup did not return a node (because the transform made contextNodeId null)
	// or if the node is no longer in the document
	if (!semanticBlockNode || !blueprintQuery.isInDocument(blueprint, semanticBlockNode)) {
		// don't allow and abort
		return CustomMutationResult.notAllowed();
	}

	if (argument.value === 'false') {
		const highestLevel = determineHighestLevel(semanticBlockNode, blueprint);

		if (highestLevel === null) {
			return CustomMutationResult.notAllowed();
		}

		// remove attribute because default is false
		blueprint.removeAttribute(semanticBlockNode, 'data-' + argument.level + '-type');

		// check of if the removed level was the highest
		if (argument.level !== highestLevel) {
			return CustomMutationResult.ok();
		}

		removeAttributeOfDescendantSemanticBlocks(
			determineHighestLevel(semanticBlockNode, blueprint),
			semanticBlockNode,
			blueprint
		);

		return CustomMutationResult.ok();
	}

	// set attribute to value
	blueprint.setAttributeNS(
		semanticBlockNode,
		null,
		'data-' + argument.level + '-type',
		argument.value
	);

	return CustomMutationResult.ok();
}
