import domInfo from 'fontoxml-dom-utils/src/domInfo.js';

function checkEndParents(endElement, element, blueprint) {
	if (endElement === element) {
		return true;
	} else if (blueprint.getNextSibling(endElement)) {
		return false;
	}

	return checkEndParents(blueprint.getParentNode(endElement), element, blueprint);
}

/*
 * @param {Node}           element   The element that is checked
 * @param {BlueprintRange} range     The range that will be searched
 * @param {blueprint}      blueprint
 */
export default function cursorIsAtTheEndOfTheNode(element, container, offset, blueprint) {
	if (container === element && offset === blueprint.getChildNodes(element).length) {
		return true;
	} else if (
		domInfo.isElement(container) &&
		offset === blueprint.getChildNodes(container).length
	) {
		return checkEndParents(blueprint.getParentNode(container), element, blueprint);
	} else if (domInfo.isTextNode(container) && offset === blueprint.getData(container).length) {
		return checkEndParents(blueprint.getParentNode(container), element, blueprint);
	}

	return false;
}
