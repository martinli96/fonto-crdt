import baseFlowUtils from 'fontoxml-base-flow/src/baseFlowUtils.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import primitives from 'fontoxml-base-flow/src/primitives.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import BlueprintPosition from 'fontoxml-blueprints/src/BlueprintPosition.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';
import outdentSectionHelper from './outdentSectionHelper.js';

export default function convertPInH1AndSplitSection(argument, blueprint, format, selection) {
	let pNode = blueprint.lookup(argument.pNodeId);
	// lookup the contextNodeId: it's a section or document-ref or null if there is no section present (@see transform in operations-section.json)
	const sectionNode = blueprint.lookup(argument.contextNodeId);

	if (
		!pNode ||
		!blueprintQuery.isInDocument(blueprint, pNode) ||
		(sectionNode && !blueprintQuery.isInDocument(blueprint, sectionNode))
	) {
		return CustomMutationResult.notAllowed();
	}

	// Default to 1, otherwise calculate for argument level
	const selectedLevel = argument.level || 1;

	const startPosition = BlueprintPosition.fromOffset(
		selection.startContainer,
		selection.startOffset,
		blueprint
	);
	const endPosition = BlueprintPosition.fromOffset(
		selection.endContainer,
		selection.endOffset,
		blueprint
	);

	const containerNode = blueprint.getParentNode(pNode);
	const documentNode = blueprintQuery.getDocumentNode(blueprint, pNode);

	if (
		// if the selection is not collapsed, and
		!selection.collapsed &&
		// the startPosition is just outside an element and the endPosition is just inside that
		// element
		// (this could happen when making selections near the start/end of an element with the
		// mouse or keyboard, so also check if the start and end position are not the same to
		// ensure we have something selected to try to wrap (see code below))
		!baseFlowUtils.arePositionsEquivalent(startPosition, endPosition, blueprint, format)
	) {
		// make sure that text ranges that start or end in inline elements are expanded to be
		// just outside those inline elements
		baseFlowUtils.expandRangeToFullyContainNodes(selection, blueprint, format, containerNode);

		const temporaryWrapperNode = documentNode.createElement('temp');

		// wrap the temporary node exactly around the (previously expanded) selection range
		blueprintMutations.unsafeWrapInSingleElement(
			containerNode,
			BlueprintPosition.fromOffset(
				selection.startContainer,
				selection.startOffset,
				blueprint
			),
			BlueprintPosition.fromOffset(selection.endContainer, selection.endOffset, blueprint),
			temporaryWrapperNode,
			blueprint
		);

		// get a reference to (an array of) the child nodes that have just been wrapped
		// this is always a single node, since the contextNodeId points to a single node
		const wrappedNodes = blueprint.getChildNodes(temporaryWrapperNode);

		blueprintMutations.unsafeCollapseElement(containerNode, temporaryWrapperNode, blueprint);

		// reassign the pNode (to be converted to h1) to the new p node that is been created
		// above by wrapping the selection (which was inside a p) inside a the temporary wrapper:
		// doing this creates a new p exactly around the selection (inside the wrapper)
		pNode = wrappedNodes[0];
	}

	const newSectionNode = documentNode.createElement('section');
	let currentLevel = 0;

	// If a section node is present, use that to indent. If there's no section node, we can just create a new one.
	if (sectionNode) {
		const sectionIsDocument = sectionNode.nodeName === 'document-ref';

		currentLevel = evaluateXPathToNumber(
			'count(ancestor-or-self::*[self::section or self::document-ref])',
			sectionNode,
			blueprint
		);

		// the given level should be smaller or equal or at most one bigger then the current level
		if (currentLevel + 1 < selectedLevel) {
			return CustomMutationResult.notAllowed();
		}

		const firstChildSectionNode = evaluateXPathToFirstNode(
			'child::*[self::section or self::document-ref][1]',
			sectionNode,
			blueprint
		);

		// move everything in the new section
		blueprintMutations.unsafeMoveNodes(
			pNode,
			// if there is a first child section and the section is not a document,
			// move everything from the p until that section (to the new section)
			!sectionIsDocument && firstChildSectionNode
				? blueprint.getPreviousSibling(firstChildSectionNode)
				: null,
			blueprint,
			newSectionNode,
			null,
			false
		);
		blueprint.insertBefore(sectionNode, newSectionNode, firstChildSectionNode);
	} else {
		// We can only indent a p to the first level, nothing more.
		if (1 < selectedLevel) {
			return CustomMutationResult.notAllowed();
		}

		// Insert the new section above the P and move the P contents into the section
		blueprint.insertBefore(blueprint.getParentNode(pNode), newSectionNode, pNode);
		blueprintMutations.unsafeMoveNodes(pNode, null, blueprint, newSectionNode, null, false);
	}

	currentLevel++;

	// Make sure the new section is on the correct level
	while (selectedLevel < currentLevel) {
		if (!outdentSectionHelper(newSectionNode, blueprint)) {
			return CustomMutationResult.notAllowed();
		}
		currentLevel--;
	}

	// convert the p to an h1 (and check if that succeeded)
	if (primitives.convertElement(blueprint, pNode, 'h1', format)) {
		// select the contents of the h1 in the new section to allow toggling back and forth
		selection.selectNodeContents(blueprint.getFirstChild(newSectionNode));

		return CustomMutationResult.ok();
	}

	return CustomMutationResult.notAllowed();
}
