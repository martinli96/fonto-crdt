export default function openURLInNewTab(url) {
	if (!url) {
		return;
	}

	url = url.trim();

	if (url) {
		if (url.indexOf('https://') === -1 || url.indexOf('http://') === -1) {
			url = 'https://' + url;
		}

		const toOpen = new URL(url);
		window.open(toOpen);
	}
}
