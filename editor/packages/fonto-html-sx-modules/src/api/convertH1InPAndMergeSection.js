import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';

import convertH1InPHelper from './convertH1InPHelper.js';
import cursorIsAtTheEndOfTheNode from './cursorIsAtTheEndOfTheNode.js';
import cursorIsAtTheStartOfTheNode from './cursorIsAtTheStartOfTheNode.js';

// Tries to
// 0) do some sanity checks to determine if the custom mutation can run
// 1) lookup the contextNodeId (should be h1) and convert it to a p
// 2) merge the content of the parent section (of h1) into either
// the preceding sibling section of the (parent) section
// the parent section or body of the (parent) section
export default function convertH1InPAndMergeSection(argument, blueprint, format, selection) {
	// 0)

	// lookup the contextNodeId: it's an p or a h1 node (@see transform in operations-section.json)
	const contextNode = blueprint.lookup(argument.contextNodeId);
	// if the contextNodeId lookup did not return a node (because the transform made contextNodeId null)
	// or if the node is no longer in the document
	if (!contextNode || !blueprintQuery.isInDocument(blueprint, contextNode)) {
		// don't allow and abort
		return CustomMutationResult.notAllowed();
	}

	if (contextNode.nodeName === 'p') {
		return CustomMutationResult.ok().setActive();
	}

	const h1Node = contextNode;

	const parentNode = blueprint.getParentNode(h1Node);

	if (
		parentNode.nodeName === 'body' ||
		evaluateXPathToBoolean(
			'if (preceding-sibling::*) then preceding-sibling::*[1][self::document-ref] else parent::document-ref',
			parentNode,
			blueprint
		)
	) {
		// We are not moving paragraphs in or out external files
		return CustomMutationResult.notAllowed();
	}

	const selectionIsAroundElement =
		blueprint.getParentNode(h1Node) === selection.findCommonAncestor();
	const cursorWasAtStart =
		selectionIsAroundElement ||
		cursorIsAtTheStartOfTheNode(
			h1Node,
			selection.startContainer,
			selection.startOffset,
			blueprint
		);
	const cursorWasAtEnd =
		selectionIsAroundElement ||
		cursorIsAtTheEndOfTheNode(h1Node, selection.endContainer, selection.endOffset, blueprint);
	const cursorWasCollapsed = selection.collapsed;

	const newPNode = convertH1InPHelper(h1Node, blueprint, format);
	if (!newPNode) {
		return CustomMutationResult.notAllowed();
	}

	// Correct the cursor after convert. As it can now be outside the new p node

	// If the cursor was at the start of the h1 node
	if (cursorWasAtStart) {
		// set the start of the cursor at the start of the new p node
		selection.setStart(newPNode, 0);
		// if the cursor was collapsed in the h1 node,
		if (cursorWasCollapsed) {
			// then also set the end of the cursor at the start of the new p node
			selection.setEnd(newPNode, 0);
		}
	}
	// If the cursor was at the end of the h1 node
	if (cursorWasAtEnd) {
		// set the end of the cursor at the end of the new p node
		selection.setEnd(newPNode, blueprint.getChildNodes(newPNode).length);
		// if the cursor was collapsed in the h1 node,
		if (cursorWasCollapsed) {
			// then also set the start of the cursor at the end of the new p node
			selection.setStart(newPNode, blueprint.getChildNodes(newPNode).length);
		}
	}

	return CustomMutationResult.ok();
}
