import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import primitives from 'fontoxml-base-flow/src/primitives.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';

export default function splitH1(argument, blueprint, format, selection) {
	const headingNode = blueprint.lookup(argument.contextNodeId);

	if (!headingNode || !blueprintQuery.isInDocument(blueprint, headingNode)) {
		return CustomMutationResult.notAllowed();
	}

	if (!selection.collapsed) {
		return CustomMutationResult.notAllowed();
	}

	const newParagraphNode = blueprintMutations.unsafeSplitUntil(
		selection.startContainer,
		selection.startOffset,
		blueprint.getParentNode(headingNode),
		blueprint
	);

	if (!newParagraphNode) {
		return CustomMutationResult.notAllowed();
	}

	if (primitives.convertElement(blueprint, newParagraphNode, 'p', format)) {
		selection.setStart(blueprint.getNextSibling(headingNode), 0);
		selection.setEnd(blueprint.getNextSibling(headingNode), 0);
		return CustomMutationResult.ok();
	}

	return CustomMutationResult.notAllowed();
}
