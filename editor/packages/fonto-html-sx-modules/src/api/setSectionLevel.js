import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';
import indentSectionHelper from './indentSectionHelper.js';
import outdentSectionHelper from './outdentSectionHelper.js';

const selector = '[self::section or self::document-ref]';

export default function setSectionLevel(argument, blueprint) {
	// lookup the contextNodeId: it's a sectionNode (@see transform in operations-section.json)
	const sectionNode = blueprint.lookup(argument.contextNodeId);
	// if the contextNodeId lookup did not return a node (because the transform made contextNodeId null)
	// or if the node is no longer in the document
	if (!sectionNode || !blueprintQuery.isInDocument(blueprint, sectionNode) || !argument.level) {
		// don't allow and abort
		return CustomMutationResult.notAllowed();
	}

	let currentLevel = evaluateXPathToNumber(
		'count(ancestor-or-self::*' + selector + ')',
		sectionNode,
		blueprint
	);
	// the section is already this level
	if (currentLevel === argument.level) {
		return CustomMutationResult.ok().setActive();
	}

	// section should indent
	if (currentLevel < argument.level) {
		const firstPrecedingSection = evaluateXPathToFirstNode(
			'preceding-sibling::*' + selector + '[1]/descendant-or-self::*' + selector + '[last()]',
			sectionNode,
			blueprint
		);

		if (!firstPrecedingSection) {
			return CustomMutationResult.notAllowed();
		}

		const firstPrecedingSectionLevel = evaluateXPathToNumber(
			'count(ancestor-or-self::*' + selector + ')',
			firstPrecedingSection,
			blueprint
		);

		if (firstPrecedingSectionLevel + 1 < argument.level) {
			return CustomMutationResult.notAllowed();
		}

		while (argument.level > currentLevel) {
			if (!indentSectionHelper(sectionNode, blueprint)) {
				return CustomMutationResult.notAllowed();
			}
			currentLevel++;
		}

		return CustomMutationResult.ok();
	}

	while (argument.level < currentLevel) {
		if (!outdentSectionHelper(sectionNode, blueprint)) {
			return CustomMutationResult.notAllowed();
		}
		currentLevel--;
	}

	return CustomMutationResult.ok();
}
