import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNumber from 'fontoxml-selectors/src/evaluateXPathToNumber.js';

// Goal:
// backspace from beginning of (next) p into previous h1
// delete from end of (previous) h1 into next p
// text in p is appended to end of h1
// other content (in section) is unaffected

export default function MergePInH1(argument, blueprint, _format, selection) {
	const mergeNode = blueprint.lookup(argument.contextNodeId);

	if (!mergeNode || !blueprintQuery.isInDocument(blueprint, mergeNode)) {
		return CustomMutationResult.notAllowed();
	}

	// Test if the cursor is collapsed and if the parent of the cursor is the element we want to merge
	if (!selection.collapsed || mergeNode !== selectionManager.getSelectedElement()) {
		return CustomMutationResult.notAllowed();
	}

	// Test if the cursor is followed by a p element and if it is on the last possible position in the h1
	if (argument.isForwards) {
		const lastPosition = evaluateXPathToNumber(
			'count(child::node()[not(self::comment() or self::processing-instruction()' +
				'[name() != "fontoxml-text-placeholder" and not(name() => starts-with("fontoxml-annotation"))]' +
				' and not(following-sibling::node()[self::* or self::text() or self::processing-instruction()' +
				'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]' +
				']))])',
			mergeNode,
			blueprint
		);
		if (
			mergeNode.nodeName !== 'h1' ||
			!evaluateXPathToBoolean(
				'following-sibling::node()[1][self::p]',
				mergeNode,
				blueprint
			) ||
			(mergeNode !== selection.startContainer &&
				(evaluateXPathToBoolean(
					'following-sibling::node()[self::* or self::text() or self::processing-instruction()' +
						'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]]',
					selection.startContainer,
					blueprint
				) ||
					selection.startOffset !== selection.startContainer.data.length)) ||
			(mergeNode === selection.startContainer && selection.startOffset < lastPosition)
		) {
			return CustomMutationResult.notAllowed();
		}
	}
	// Test if the cursor is preceded by a h1 element and if it is on the first possible position in the p
	else if (!argument.isForwards) {
		const firstPosition = evaluateXPathToNumber(
			'count(child::node()[self::comment() or self::processing-instruction()' +
				'[name() != "fontoxml-text-placeholder" and not(name() => starts-with("fontoxml-annotation"))]' +
				'][not(preceding-sibling::node()[self::* or self::text() or self::processing-instruction()' +
				'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]' +
				'])])',
			mergeNode,
			blueprint
		);
		if (
			mergeNode.nodeName !== 'p' ||
			!evaluateXPathToBoolean(
				'preceding-sibling::node()[1][self::h1]',
				mergeNode,
				blueprint
			) ||
			(mergeNode !== selection.startContainer &&
				(evaluateXPathToBoolean(
					'preceding-sibling::node()[self::* or self::text() or self::processing-instruction()' +
						'[name() = "fontoxml-text-placeholder" or name() => starts-with("fontoxml-annotation")]]',
					selection.startContainer,
					blueprint
				) ||
					selection.startOffset !== 0)) ||
			(mergeNode === selection.startContainer && selection.startOffset > firstPosition)
		) {
			return CustomMutationResult.notAllowed();
		}
	}

	const paragraphNode = argument.isForwards
			? evaluateXPathToFirstNode(
					'following-sibling::node()[1][self::p]',
					mergeNode,
					blueprint
			  )
			: mergeNode,
		headingNode = argument.isForwards
			? mergeNode
			: evaluateXPathToFirstNode(
					'preceding-sibling::node()[1][self::h1]',
					mergeNode,
					blueprint
			  ),
		parentNode = blueprint.getParentNode(mergeNode);

	// Check if the paragraph isn't empty
	if (
		!argument.isForwards &&
		!evaluateXPathToBoolean('child::node()[self::* or self::text()]', paragraphNode, blueprint)
	) {
		return CustomMutationResult.notAllowed();
	}

	const firstChild = blueprint.getFirstChild(paragraphNode);
	if (firstChild) {
		blueprintMutations.unsafeMoveNodes(
			firstChild,
			blueprint.getLastChild(paragraphNode),
			blueprint,
			headingNode,
			null,
			true
		);
	}

	blueprint.removeChild(parentNode, paragraphNode);

	return CustomMutationResult.ok();
}
