import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';

export default function outdentSectionHelper(sectionNode, blueprint) {
	const currentParentNode = evaluateXPathToFirstNode(
		'parent::*[self::section or self::document-ref]',
		sectionNode,
		blueprint
	);

	if (!currentParentNode) {
		return false;
	}

	const newParentNode = evaluateXPathToFirstNode(
		'parent::section | parent::document-ref | parent::body',
		currentParentNode,
		blueprint
	);

	const movingSiblingNode = evaluateXPathToFirstNode(
		'following-sibling::*[self::section or self::document-ref]',
		sectionNode,
		blueprint
	);

	if (movingSiblingNode) {
		blueprintMutations.unsafeMoveNodes(
			movingSiblingNode,
			null,
			blueprint,
			sectionNode,
			null,
			false
		);
	}

	blueprintMutations.unsafeMoveNodes(
		sectionNode,
		sectionNode,
		blueprint,
		newParentNode,
		blueprint.getNextSibling(currentParentNode),
		false
	);

	return true;
}
