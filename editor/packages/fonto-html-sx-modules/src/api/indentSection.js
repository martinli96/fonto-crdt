import CustomMutationResult from 'fontoxml-base-flow/src/CustomMutationResult.js';
import blueprintQuery from 'fontoxml-blueprints/src/blueprintQuery.js';
import indentSectionHelper from './indentSectionHelper.js';

export default function indentSection(argument, blueprint) {
	const contextNode = blueprint.lookup(argument.contextNodeId);

	if (!contextNode || !blueprintQuery.isInDocument(blueprint, contextNode)) {
		return CustomMutationResult.notAllowed();
	}

	return indentSectionHelper(contextNode, blueprint)
		? CustomMutationResult.ok()
		: CustomMutationResult.notAllowed();
}
