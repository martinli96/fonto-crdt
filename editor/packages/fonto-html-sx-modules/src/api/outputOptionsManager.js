import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import externalDataManager from 'fontoxml-templated-views/src/externalDataManager.js';

import hierarchyNodesLoader from 'fonto-html-multi-document/src/api/hierarchyNodesLoader.js';

class OutputOptionsManager {
	constructor() {
		this._outputOptionsByName = {};

		hierarchyNodesLoader.templateDocumentIdChangedNotifier.addCallback(() => {
			Object.keys(this._outputOptionsByName).forEach(outputOptionName => {
				externalDataManager.setExternalData(
					'output-option-value-by-name|' + outputOptionName,
					undefined
				);
			});
			this._outputOptionsByName = {};
			const templateDocumentId = hierarchyNodesLoader.templateDocumentId;
			const templateDocumentNode = templateDocumentId
				? documentsManager.getDocumentNode(templateDocumentId)
				: null;
			if (!templateDocumentNode) {
				return;
			}
			this._outputOptionsByName = evaluateXPathToNodes(
				'//html/head/template[1]/div/div',
				templateDocumentNode,
				readOnlyBlueprint
			).reduce((outputOptionByName, outputOptionNode) => {
				const name = evaluateXPathToString(
					'./span/string()',
					outputOptionNode,
					readOnlyBlueprint
				);

				outputOptionByName[name] = {
					values: evaluateXPathToStrings(
						'./p/span/string()',
						outputOptionNode,
						readOnlyBlueprint
					),
					value: null
				};

				return outputOptionByName;
			}, {});
		});
	}

	openToolbar() {
		externalDataManager.setExternalData('output-option-toolbar-is-open', true);
	}

	closeToolbar() {
		externalDataManager.setExternalData('output-option-toolbar-is-open', false);
	}

	setOutputOption(name, value) {
		this._outputOptionsByName[name].value = value;
		externalDataManager.setExternalData('output-option-value-by-name|' + name, value);
	}

	getOutputOptions(currentValues, valueItems, withValue, valuesAreNotFor) {
		const valueByName = {};
		const outputOptions = Object.keys(this._outputOptionsByName).map(outputOptionName => {
			const values = this._outputOptionsByName[outputOptionName].values;

			const outputOption = {
				name: outputOptionName
			};

			if (valueItems !== null) {
				if (values.length > 1) {
					outputOption.valueItems = valueItems.concat(
						values.map(value => ({ value: value, label: value }))
					);
				} else {
					outputOption.valueItems = valueItems;
				}
			}

			if (withValue) {
				outputOption.value = this._outputOptionsByName[outputOptionName].value;
			}

			if (currentValues === null) {
				return outputOption;
			}

			if (valuesAreNotFor) {
				if (currentValues.onlyFor[outputOptionName]) {
					valueByName[outputOptionName] = values.reduce((selectedValues, value) => {
						if (!currentValues.onlyFor[outputOptionName].includes(value)) {
							selectedValues.push(value);
						}
						return selectedValues;
					}, []);
				} else if (currentValues.notFor[outputOptionName]) {
					valueByName[outputOptionName] = currentValues.notFor[outputOptionName];
				} else {
					valueByName[outputOptionName] = [];
				}
				return outputOption;
			}

			if (currentValues.onlyFor[outputOptionName]) {
				valueByName[outputOptionName] = currentValues.onlyFor[outputOptionName];
			} else if (currentValues.notFor[outputOptionName]) {
				valueByName[outputOptionName] = values.reduce((selectedValues, value) => {
					if (!currentValues.notFor[outputOptionName].includes(value)) {
						selectedValues.push(value);
					}
					return selectedValues;
				}, []);
			} else {
				valueByName[outputOptionName] = values;
			}
			return outputOption;
		});

		if (valueItems === null && currentValues !== null) {
			return valueByName;
		}

		if (currentValues !== null) {
			return {
				outputOptions,
				valueByName
			};
		}

		return outputOptions;
	}
}

export default new OutputOptionsManager();
