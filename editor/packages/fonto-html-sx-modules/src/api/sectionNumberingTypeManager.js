import operationsManager from 'fontoxml-operations/src/operationsManager.js';
import externalDataManager from 'fontoxml-templated-views/src/externalDataManager.js';

let _sectionNumberingType = 'heading-level';

function toggleType() {
	_sectionNumberingType =
		_sectionNumberingType === 'heading-level' ? 'numbering' : 'heading-level';
	externalDataManager.setExternalData('section-numbering-type', _sectionNumberingType);
	operationsManager.invalidateOperationStatesByStepType(
		'action',
		'toggle-section-numbering-type'
	);
}

function getType() {
	return _sectionNumberingType;
}

export default {
	toggleType: toggleType,
	getType: getType
};
