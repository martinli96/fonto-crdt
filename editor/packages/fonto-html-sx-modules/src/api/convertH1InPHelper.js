import primitives from 'fontoxml-base-flow/src/primitives.js';
import blueprintMutations from 'fontoxml-blueprints/src/blueprintMutations.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import namespaceManager from 'fontoxml-dom-namespaces/src/namespaceManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';

export default function convertH1InPHelper(h1Node, blueprint, format) {
	const sectionNode = blueprint.getParentNode(h1Node);
	// if the parent of the h1 is not a section element
	// (this happens if the contextNodeId points to the root h1 in the body element)
	if (!domInfo.isElement(sectionNode, 'section')) {
		// don't allow and abort: body has to have an h1 element, it cannot be converted to a p
		return null;
	}

	// 1)

	const newPNode = namespaceManager.createElement(h1Node, 'p');
	// if the h1 has content
	if (blueprint.getFirstChild(h1Node)) {
		// move it to the new p
		blueprintMutations.unsafeMoveNodes(
			blueprint.getFirstChild(h1Node),
			blueprint.getLastChild(h1Node),
			blueprint,
			newPNode,
			null
		);
	}

	// copy all the attributes from the h1 to the new p
	const attributes = blueprint.getAllAttributes(h1Node).reduce(function(attributes, attribute) {
		attributes[attribute.name] = attribute.value;
		return attributes;
	}, {});
	blueprintMutations.unsafeSetMultipleAttributes(newPNode, attributes, blueprint);

	// replace the h1 with the new p (inside the [parent] section [of the h1])
	blueprint.replaceChild(sectionNode, newPNode, h1Node);

	// 2)

	const precedingSectionNode = evaluateXPathToFirstNode(
		'preceding-sibling::*[1][self::section]',
		sectionNode,
		blueprint
	);
	// if the preceding sibling of the section is also a section and the preceding section has
	// child sections
	if (
		precedingSectionNode &&
		evaluateXPathToBoolean('child::section', precedingSectionNode, blueprint)
	) {
		const lastDescendantSectionNode = evaluateXPathToFirstNode(
			'descendant::section[last()]',
			precedingSectionNode,
			blueprint
		);

		const childSectionNodes = evaluateXPathToNodes('child::section', sectionNode, blueprint);

		// move contents (except child sections) of the current section into the last descendant
		// section (of the previous section)
		blueprintMutations.unsafeMoveNodes(
			blueprint.getFirstChild(sectionNode),
			childSectionNodes && childSectionNodes.length > 0
				? blueprint.getPreviousSibling(childSectionNodes[0])
				: null,
			blueprint,
			lastDescendantSectionNode,
			null
		);

		if (childSectionNodes && childSectionNodes.length > 0) {
			// move all the child section nodes into the preceding section node
			blueprintMutations.unsafeMoveNodes(
				childSectionNodes[0],
				null,
				blueprint,
				precedingSectionNode,
				null
			);
		}

		// remove the current section (which has become an empty shell)
		blueprint.removeChild(blueprint.getParentNode(sectionNode), sectionNode);

		return newPNode;
	}
	// if the preceding sibling of the section is also a section without child sections
	else if (precedingSectionNode) {
		// and the section can be merged into its preceding sibling (indicated by false for the isMergeForwards parameter below)
		if (primitives.mergeNodes(sectionNode, false, blueprint, format)) {
			return newPNode;
		}
	}
	// else (the section has no preceding sections)
	// if the parent of the section (grandparent of the h1) is either the body or a section
	else if (evaluateXPathToBoolean('parent::body or parent::section', sectionNode, blueprint)) {
		// if the section can be collapsed
		// (meaning, the section element itself will be removed without deleting its contents,
		// they will be inserted in the (grand)parent at the child offset of the section element)
		if (primitives.collapseElements([sectionNode], blueprint, format)) {
			return newPNode;
		}
	}

	return null;
}
