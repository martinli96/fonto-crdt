import addReducer from 'fontoxml-indices/src/addReducer.js';
import applyCss from 'fontoxml-styles/src/applyCss.js';
import configureAsBlock from 'fontoxml-families/src/configureAsBlock.js';
import configureAsDefinitionsTableRow from 'fontoxml-families/src/configureAsDefinitionsTableRow.js';
import configureAsFrame from 'fontoxml-families/src/configureAsFrame.js';
import configureAsGroup from 'fontoxml-families/src/configureAsGroup.js';
import createRelatedNodesWidget from 'fontoxml-families/src/createRelatedNodesWidget.js';
import configureAsInlineAnchorToStructure from 'fontoxml-families/src/configureAsInlineAnchorToStructure.js';
import configureAsImageInFrame from 'fontoxml-families/src/configureAsImageInFrame.js';
import configureAsInlineFrame from 'fontoxml-families/src/configureAsInlineFrame.js';
import configureAsInlineImageInFrame from 'fontoxml-families/src/configureAsInlineImageInFrame.js';
import configureAsInlineLink from 'fontoxml-families/src/configureAsInlineLink.js';
import configureAsInlineObject from 'fontoxml-families/src/configureAsInlineObject.js';
import configureAsInvalid from 'fontoxml-families/src/configureAsInvalid.js';
import configureAsListElements from 'fontoxml-list-flow/src/configureAsListElements.js';
import configureAsRemoved from 'fontoxml-families/src/configureAsRemoved.js';
import configureAsSheetFrame from 'fontoxml-families/src/configureAsSheetFrame.js';
import configureAsStructure from 'fontoxml-families/src/configureAsStructure.js';
import configureAsStructureViewItem from 'fontoxml-families/src/configureAsStructureViewItem.js';
import configureAsTitleFrame from 'fontoxml-families/src/configureAsTitleFrame.js';
import configureProperties from 'fontoxml-families/src/configureProperties.js';
import createIconWidget from 'fontoxml-families/src/createIconWidget.js';
import createLabelQueryWidget from 'fontoxml-families/src/createLabelQueryWidget.js';
import createMarkupLabelWidget from 'fontoxml-families/src/createMarkupLabelWidget.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import externalDataManager from 'fontoxml-templated-views/src/externalDataManager.js';
import parsePseudoAttributes from 'fontoxml-dom-utils/src/parsePseudoAttributes.js';
import registerCustomXPathFunction from 'fontoxml-selectors/src/registerCustomXPathFunction.js';
import t from 'fontoxml-localization/src/t.js';

import createSemanticBlockLabelWidget from 'fonto-html-semantics/src/createSemanticBlockLabelWidget.js';

import createOutputOptionIconWidget from './createOutputOptionIconWidget.js';
import createSectionNumberingWidget from './createSectionNumberingWidget.js';
import createSemanticLevelsWidget from './createSemanticLevelsWidget.js';
import createVariableInnerJsonMl from './createVariableInnerJsonMl.js';

import outputOptionsManager from './api/outputOptionsManager.js';

const TEXT_PLACEHOLDER_STYLES = applyCss({
	'display': 'inline-block',
	'cursor': 'pointer',
	'opacity': 0.4,

	'transition': 'opacity 256ms ease-out',

	'&:hover,&:focus,.focus > &': {
		opacity: 0.7
	}
});

function getMarkupLabelXPath(defaultLabel) {
	return (
		'x__if (@class) then if (contains(@class, ":")) then tokenize(@class, ":")[last()] else @class else "' +
		defaultLabel +
		'"'
	);
}

export default function configureSxModule(sxModule) {
	addReducer(
		'urn:fontoxml:working:with:structures',
		'sectionCounter',
		'self::section or self::document-ref',
		'urn:fontoxml:working:with:structures',
		'sectionReducer'
	);

	registerCustomXPathFunction(
		'wws:get-section-numbering',
		['item()*', 'xs:boolean'],
		'xs:string',
		function(dynamicContext, numbering, inStructureView) {
			if (!numbering) {
				return '';
			}

			const dataSource = externalDataManager.getExternalDataSource('section-numbering-type');
			const result = dataSource.getValue();

			if (dynamicContext.domFacade.tryAddExternalDependency) {
				dynamicContext.domFacade.tryAddExternalDependency(addExternalDependency => {
					// eslint-disable-next-line prefer-const
					let removeCallback;

					const invalidateExternalDependency = addExternalDependency(function() {
						removeCallback();
					});

					removeCallback = dataSource.changedNotifier.addCallback(
						function externalDataChanged() {
							if (dataSource.getValue() === result) {
								return;
							}
							invalidateExternalDependency();
						}
					);
				});
			}

			let numberingLabel = '';

			if (!result || result === 'heading-level') {
				if (inStructureView) {
					return numberingLabel;
				}
				numberingLabel = 'H' + numbering.length;
			} else {
				numberingLabel = (inStructureView ? '' : '§') + numbering.join('.');
			}

			return numberingLabel;
		}
	);

	function getValuesByNameFromfilterUnits(valuesByName, filterUnit) {
		const filterUnitArr = filterUnit.split(':');
		const name = filterUnitArr[0];
		const value = filterUnitArr[filterUnitArr.length - 1];
		if (!valuesByName[name]) {
			valuesByName[name] = [];
		}
		valuesByName[name].push(value);
		return valuesByName;
	}

	registerCustomXPathFunction('wws:is-output-option-toolbar-open', [], 'xs:boolean', function(
		dynamicContext
	) {
		const dataSource = externalDataManager.getExternalDataSource(
			'output-option-toolbar-is-open'
		);
		const result = dataSource.getValue();

		if (dynamicContext.domFacade.tryAddExternalDependency) {
			dynamicContext.domFacade.tryAddExternalDependency(addExternalDependency => {
				// eslint-disable-next-line prefer-const
				let removeCallback;

				const invalidateExternalDependency = addExternalDependency(function() {
					removeCallback();
				});

				removeCallback = dataSource.changedNotifier.addCallback(
					function externalDataChanged() {
						if (dataSource.getValue() === result) {
							return;
						}
						invalidateExternalDependency();
					}
				);
			});
		}

		return !!result;
	});

	registerCustomXPathFunction('wws:is-filtered', ['node()'], 'xs:boolean', function(
		dynamicContext,
		node
	) {
		if (
			!node ||
			evaluateXPathToBoolean(
				'not(@data-only-for) and not(@data-not-for)',
				node,
				dynamicContext.domFacade
			)
		) {
			return false;
		}

		const currentValues = {
			onlyFor: evaluateXPathToStrings(
				'./@data-only-for/tokenize(.)',
				node,
				dynamicContext.domFacade
			).reduce(getValuesByNameFromfilterUnits, {}),
			notFor: evaluateXPathToStrings(
				'./@data-not-for/tokenize(.)',
				node,
				dynamicContext.domFacade
			).reduce(getValuesByNameFromfilterUnits, {})
		};
		const valuesNotForByName = outputOptionsManager.getOutputOptions(
			currentValues,
			null,
			false,
			true
		);

		const dataSourceByName = {};
		const resultByName = {};

		let isFiltered = false;

		Object.keys(valuesNotForByName).forEach(outputOptionName => {
			dataSourceByName[outputOptionName] = externalDataManager.getExternalDataSource(
				'output-option-value-by-name|' + outputOptionName
			);
			const result = dataSourceByName[outputOptionName].getValue();
			resultByName[outputOptionName] = result;

			if (isFiltered) {
				return;
			}

			const notFor = valuesNotForByName[outputOptionName];

			if (result && notFor.includes(result)) {
				isFiltered = true;
			}
		});
		if (dynamicContext.domFacade.tryAddExternalDependency) {
			dynamicContext.domFacade.tryAddExternalDependency(addExternalDependency => {
				// eslint-disable-next-line prefer-const
				let removeCallbacks;

				const invalidateExternalDependency = addExternalDependency(function() {
					removeCallbacks.forEach(removeCallback => removeCallback());
				});

				removeCallbacks = Object.keys(dataSourceByName).map(outputOptionName => {
					const dataSource = dataSourceByName[outputOptionName];
					return dataSource.changedNotifier.addCallback(function externalDataChanged() {
						if (dataSource.getValue() === resultByName[outputOptionName]) {
							return;
						}
						invalidateExternalDependency();
					});
				});
			});
		}
		return isFiltered;
	});

	configureAsRemoved(sxModule, 'self::element()');

	// filtered elements
	configureProperties(
		sxModule,
		'self::*[not(self::html)][wws:is-output-option-toolbar-open() and wws:is-filtered(.)]',
		{
			outputClass: 'is-filtered'
		}
	);

	configureProperties(sxModule, 'self::*[(@data-only-for or @data-not-for)]', {
		blockOutsideAfter: [createOutputOptionIconWidget()],
		inlineAfter: [createOutputOptionIconWidget()]
	});

	// a
	configureAsInlineLink(sxModule, 'self::a', 'link', undefined, {
		emptyElementPlaceholderText: t('type the link text'),
		popoverComponentName: 'WebReferencePopover',
		popoverData: {
			editOperationName: 'edit-a',
			targetIsPermanentId: false,
			targetQuery: '@href'
		}
	});

	// base
	configureAsRemoved(sxModule, 'self::base', 'base');

	// body
	configureAsStructure(sxModule, 'self::body', 'body');

	configureAsStructureViewItem(sxModule, 'self::body', {
		isHiddenFromView: true
	});

	// body
	configureAsStructure(sxModule, 'self::body');

	// dd
	configureAsBlock(sxModule, 'self::dd', 'value', {
		emptyElementPlaceholderText: t('type the variable value'),
		isAutoremovableIfEmpty: true
	});

	// di
	configureAsDefinitionsTableRow(sxModule, 'self::di', 'variable', {
		columns: [
			{ query: './dt', width: 1 },
			{ query: './dd', width: 1, clickOperationWhenEmpty: 'di-insert-dd' }
		],
		contextualOperations: [
			{
				name: 'contextual-insert-di--above',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-insert-di--below',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-delete-di',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			}
		],
		borders: true
	});

	// div
	configureAsGroup(sxModule, 'self::div', getMarkupLabelXPath('block'), {
		contextualOperations: [
			{ name: 'semantics::set-element-type', hideIn: ['context-menu', 'breadcrumbs-menu'] },
			{ name: 'contextual-set-output-options', hideIn: ['context-menu'] },
			{ name: 'contextual-unwrap-div', hideIn: ['context-menu'] }
		],
		defaultTextContainer: 'p',
		outputClass: 'div-family-styling',
		blockHeaderLeft: [createSemanticBlockLabelWidget()]
	});

	// div in output
	configureProperties(sxModule, 'self::div[ancestor::html[@data-is-output-document]]', {
		contextualOperations: [],
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// div container in template
	configureAsFrame(sxModule, 'self::div[parent::template]', 'Output options', {
		contextualOperations: [],
		defaultTextContainer: 'div',
		outputClass: null,
		tabNavigationItemSelector: 'self::span[parent::div] or self::p',
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// div in template
	configureAsDefinitionsTableRow(
		sxModule,
		'self::div[parent::div[parent::template]]',
		'Output option',
		{
			columns: [{ query: './span', width: 1 }, { query: './p', width: 1 }],
			borders: true,
			contextualOperations: [
				{
					name: 'contextual-insert-output-option--above',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-insert-output-option--below',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				},
				{
					name: 'contextual-delete-output-option',
					hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
				}
			],
			outputClass: null,
			blockHeaderLeft: []
		}
	);

	// dl
	configureAsFrame(sxModule, 'self::dl', 'variables table', {
		tabNavigationItemSelector: 'self::dt or self::dd',
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// dt
	configureAsBlock(sxModule, 'self::dt', 'name', {
		emptyElementPlaceholderText: t('type the variable name')
	});

	// figcaption
	configureAsTitleFrame(sxModule, 'self::figcaption', 'caption', {
		emptyElementPlaceholderText: t('Type the caption'),
		fontVariation: 'figure-title'
	});

	// figure
	configureAsFrame(sxModule, 'self::figure', getMarkupLabelXPath('figure'), {
		contextualOperations: [
			{
				name: 'semantics::set-element-type',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-set-output-options',
				hideIn: ['context-menu']
			},
			{ name: 'figure-insert-figcaption', hideIn: ['context-menu'] },
			{
				name: 'figure-append-img',
				hideIn: ['context-menu']
			},
			{
				name: 'figure-insert-img',
				hideIn: ['context-menu', 'element-menu', 'breadcrumbs-menu']
			},
			{ name: 'contextual-delete-figure', hideIn: ['context-menu'] }
		],
		defaultTextContainer: 'p',
		titleQuery: './figcaption',
		blockHeaderLeft: [createSemanticBlockLabelWidget()]
	});

	// figure in output
	configureProperties(sxModule, 'self::figure[ancestor::html[@data-is-output-document]]', {
		contextualOperations: [],
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// h1 (1 smaller than biggest)
	configureAsTitleFrame(sxModule, 'self::h1', 'heading', {
		emptyElementPlaceholderText: 'Untitled section',
		fontVariation: 'document-title'
	});

	// h1 level 2
	configureProperties(
		sxModule,
		'self::h1[parent::section and count(ancestor::*[self::section or self::document-ref]) > 1]',
		{
			fontVariation: 'section-title'
		}
	);

	// h1 level 3 or higher
	configureProperties(
		sxModule,
		'self::h1[parent::section and count(ancestor::*[self::section or self::document-ref]) > 2]',
		{
			fontVariation: 'medium-title'
		}
	);

	// H4 and H5 elements
	configureProperties(
		sxModule,
		'self::h1[parent::section and count(ancestor::*[self::section or self::document-ref]) > 3]',
		{
			fontVariation: 'small-title'
		}
	);

	// h1 in body (biggest)
	configureProperties(sxModule, 'self::h1[parent::body]', {
		blockHeaderLeft: [
			createSectionNumberingWidget(
				'parent::body/parent::html/wws:get-section-numbering(wws:sectionCounter(fonto:current-hierarchy-node-id(), .), false())',
				'4px'
			)
		]
	});

	// h1 in root document
	configureProperties(sxModule, 'self::h1[parent::body[parent::html[@data-is-root-document]]]', {
		emptyElementPlaceholderText: 'Untitled document',
		fontVariation: 'collection-title',
		blockHeaderLeft: []
	});

	// h1 in definition document
	configureProperties(
		sxModule,
		'self::h1[parent::body[parent::html[@data-is-definition-document]]]',
		{
			emptyElementPlaceholderText: 'Untitled definition document',
			fontVariation: 'document-title',
			blockHeaderLeft: []
		}
	);

	// h1 in the preview or loose
	configureProperties(
		sxModule,
		'self::h1[parent::body[parent::html[not(@data-is-definition-document) and not(@data-is-root-document)]] and not(fonto:hierarchy-source-node(fonto:current-hierarchy-node-id()))]',
		{
			priority: 2,
			fontVariation: 'document-title',
			blockHeaderLeft: []
		}
	);

	// h1 in body level 2
	configureProperties(
		sxModule,
		'self::h1[parent::body and count(fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())/ancestor::*[self::section or self::document-ref]) > 2]',
		{
			fontVariation: 'section-title',
			blockHeaderLeft: [
				createSectionNumberingWidget(
					'parent::body/parent::html/wws:get-section-numbering(wws:sectionCounter(fonto:current-hierarchy-node-id(), .), false())',
					'0px'
				)
			]
		}
	);

	// h1 in body level 3 or higher
	configureProperties(
		sxModule,
		'self::h1[parent::body and count(fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())/ancestor::*[self::section or self::document-ref]) > 3]',
		{
			fontVariation: 'medium-title'
		}
	);

	// h1 in body level 4 or higher
	configureProperties(
		sxModule,
		'self::h1[parent::body and count(fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())/ancestor::*[self::section or self::document-ref]) > 4]',
		{
			fontVariation: 'small-title'
		}
	);

	// head
	configureAsSheetFrame(
		sxModule,
		'self::head[parent::html[@data-is-definition-document]]',
		'head',
		{
			outputClass: 'sheet-frame-family-styling'
		}
	);

	// html output option elements
	configureProperties(
		sxModule,
		'self::body[wws:is-output-option-toolbar-open() and fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())[wws:is-filtered(.) or ancestor::*[wws:is-filtered(.)]]]',
		{
			outputClass: 'is-filtered'
		}
	);
	configureProperties(
		sxModule,
		'self::html[wws:is-output-option-toolbar-open() and fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())[@data-only-for or @data-not-for]]',
		{
			blockHeaderRight: [
				createLabelQueryWidget('"External file"', { inline: true }),
				createOutputOptionIconWidget(
					'fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())'
				)
			]
		}
	);

	// html
	configureAsSheetFrame(sxModule, 'self::html', getMarkupLabelXPath('section'), {
		blockHeaderLeft: [createSemanticBlockLabelWidget()],
		visibleChildSelectorOrNodeSpec: 'self::node()[not(self::head)]',
		outputClass: 'sheet-frame-family-styling',
		contextualOperations: [
			{
				name: 'semantics::set-element-type',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'structure-view']
			},
			{
				name: 'contextual-set-output-options-document-ref',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'structure-view']
			},
			{
				name: 'contextual-document-remove',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'structure-view']
			}
		],
		blockFooter: [createRelatedNodesWidget('self::aside')]
	});

	configureAsStructureViewItem(sxModule, 'self::html', {
		icon: 'file-o',
		titleQuery:
			'concat(wws:get-section-numbering(wws:sectionCounter(fonto:current-hierarchy-node-id(), .), true()), " ", ./body/h1)'
	});

	// html in output
	configureAsSheetFrame(
		sxModule,
		'self::html[@data-is-output-document]',
		getMarkupLabelXPath('document'),
		{
			blockHeaderLeft: [createMarkupLabelWidget()],
			visibleChildSelectorOrNodeSpec: 'self::node()[not(self::head)]',
			outputClass: null,
			contextualOperations: []
		}
	);

	// html in preview
	configureProperties(
		sxModule,
		'self::html[not(fonto:hierarchy-source-node(fonto:current-hierarchy-node-id())) and not(@data-is-root-document or @data-is-definition-document)]',
		{
			blockHeaderRight: [],
			outputClass: null
		}
	);

	// img
	configureAsImageInFrame(
		sxModule,
		'self::img',
		'image',
		{ referenceQuery: '@src', isPermanentId: false },
		{
			contextualOperations: [{ name: 'edit-img', hideIn: ['context-menu'] }],
			blockHeaderRight: [
				createLabelQueryWidget('""', {
					inline: true,
					prefixQuery: '"alternative text"'
				}),
				createIconWidget('edit', {
					clickPopoverComponentName: 'ChangeAltTextPopover'
				})
			]
		}
	);
	configureProperties(sxModule, 'self::img[@alt!=""]', {
		blockHeaderRight: [
			createLabelQueryWidget('""', {
				inline: true,
				prefixQuery: '"alternative text"',
				tooltipQuery: './@alt'
			}),
			createIconWidget('edit', {
				clickPopoverComponentName: 'ChangeAltTextPopover',
				clickOperation: 'do-nothing'
			})
		]
	});
	// img in inline layout
	configureAsInlineImageInFrame(
		sxModule,
		'self::img and fonto:in-inline-layout(.)',
		'inline image',
		{ referenceQuery: '@src', isPermanentId: false },
		{
			priority: 2,
			contextualOperations: [{ name: 'edit-img@alt', hideIn: ['context-menu'] }]
		}
	);

	// li
	configureProperties(sxModule, 'self::li', {
		markupLabel: 'item'
	});

	// link
	configureAsRemoved(sxModule, 'self::link', 'link');

	// meta
	configureAsRemoved(sxModule, 'self::meta', 'meta');

	// ol
	configureAsListElements(sxModule, {
		list: {
			nodeName: 'ol',
			style: configureAsListElements.NUMBERED_LIST_STYLE
		},
		item: {
			nodeName: 'li'
		},
		paragraph: {
			nodeName: 'p'
		}
	});
	configureProperties(sxModule, 'self::ol', {
		markupLabel: getMarkupLabelXPath('numbered list'),
		contextualOperations: [
			{ name: 'semantics::set-element-type', hideIn: ['context-menu', 'breadcrumbs-menu'] },
			{ name: 'contextual-set-output-options', hideIn: ['context-menu', 'breadcrumbs-menu'] }
		]
	});

	// ol in output
	configureProperties(sxModule, 'self::ol[ancestor::html[@data-is-output-document]]', {
		contextualOperations: []
	});

	// p
	configureAsBlock(sxModule, 'self::p', 'paragraph', {
		contextualOperations: [
			{ name: 'contextual-set-output-options', hideIn: ['context-menu', 'breadcrumbs-menu'] }
		]
	});

	// p semantic in template
	configureProperties(sxModule, 'self::p[parent::li and ancestor::template]', {
		markupLabel: 'content type block',
		contextualOperations: [],
		blockAfterWidth: 'extra-wide',
		blockAfter: [createSemanticLevelsWidget()]
	});

	// p output option in template
	configureAsGroup(
		sxModule,
		'self::p[parent::div[parent::div[parent::template]]]',
		'output option values',
		{
			expression: 'compact',
			contextualOperations: [],
			defaultTextContainer: 'span'
		}
	);

	// script
	configureAsRemoved(sxModule, 'self::script', 'script');

	// section output option elements
	configureProperties(
		sxModule,
		'self::*[wws:is-output-option-toolbar-open() and parent::section[wws:is-filtered(.) or ancestor::*[wws:is-filtered(.)]]]',
		{
			outputClass: 'is-filtered'
		}
	);

	// section
	configureAsStructure(sxModule, 'self::section', getMarkupLabelXPath('section'), {
		allowMergingWith: 'self::section',
		contextualOperations: [
			{
				name: 'semantics::set-element-type',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu', 'structure-view']
			},
			{
				name: 'contextual-set-output-options',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu', 'structure-view']
			},
			{
				name: 'contextual-insert-document-ref--to-existing-document',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-move-section-up',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-move-section-down',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-indent-section',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-outdent-section',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{
				name: 'contextual-remove-section',
				hideIn: ['context-menu', 'breadcrumbs-menu', 'element-menu']
			},
			{ name: 'open-save-to-section-fragment-modal', hideIn: ['context-menu'] },
			{ name: 'edit-section-fragment', hideIn: ['context-menu'] }
		],
		defaultTextContainer: 'p',
		blockHeaderLeft: [createSemanticBlockLabelWidget()]
	});

	sxModule
		.configure('fontoxml-base-flow')
		.forNodesMatching('self::section', 2)
		.isSplittable(true);

	configureAsStructureViewItem(sxModule, 'self::section', {
		icon: 'file-o',
		titleQuery:
			'concat(wws:get-section-numbering(wws:sectionCounter(fonto:current-hierarchy-node-id(), .), true()), " ", ./h1)'
	});

	// section in output
	configureAsGroup(
		sxModule,
		'self::section[ancestor::html[@data-is-output-document]]',
		getMarkupLabelXPath('section'),
		{
			contextualOperations: [],
			defaultTextContainer: 'p',
			outputClass: null,
			blockHeaderLeft: [createMarkupLabelWidget()],
			blockHeaderRight: []
		}
	);

	// span
	configureAsInlineFrame(sxModule, 'self::span', getMarkupLabelXPath('inline'), {
		contextualOperations: [
			{ name: 'semantics::set-element-type', hideIn: ['context-menu', 'breadcrumbs-menu'] },
			{ name: 'contextual-set-output-options', hideIn: ['context-menu', 'breadcrumbs-menu'] }
		],
		backgroundColor: 'blue'
	});
	sxModule
		.configure('fontoxml-base-flow')
		.forNodesMatching('self::span[not(@data-is-variable)]', 2)
		.isSplittable(true);

	configureProperties(sxModule, 'self::span[@class = "bold"]', {
		weight: 'bold'
	});
	configureProperties(sxModule, 'self::span[@class = "italic"]', {
		slant: 'italic'
	});
	configureProperties(sxModule, 'self::span[@class = "underline"]', {
		underlineStyle: 'single'
	});
	configureProperties(sxModule, 'self::span[@class = "sup"]', {
		baseline: 'superscript'
	});
	configureProperties(sxModule, 'self::span[@class = "sub"]', {
		baseline: 'subscript'
	});

	// span in output
	configureProperties(sxModule, 'self::span[ancestor::html[@data-is-output-document]]', {
		contextualOperations: []
	});

	// span is variable
	configureAsInlineObject(
		sxModule,
		'self::span[@data-is-variable]',
		getMarkupLabelXPath('variable'),
		{
			contextualOperations: [
				{
					name: 'contextual-edit-variables',
					hideIn: ['context-menu', 'breadcrumbs-menu']
				},
				{
					name: 'contextual-set-output-options',
					hideIn: ['context-menu', 'breadcrumbs-menu']
				}
			],
			createInnerJsonMl: createVariableInnerJsonMl,
			clickOperation: 'select-node',
			doubleClickOperation: 'contextual-edit-variables'
		}
	);

	configureAsInlineObject(
		sxModule,
		'self::processing-instruction(author-guidance)',
		'instruction',
		{
			createInnerJsonMl: function(sourceNode) {
				let spanAttributes = Object.assign(
					{
						contenteditable: 'false'
					},
					TEXT_PLACEHOLDER_STYLES
				);

				const parsedGuidanceAttributes = parsePseudoAttributes(sourceNode.data);

				if (!sourceNode.getViewFlag('readonly')) {
					spanAttributes = Object.assign(spanAttributes, {
						'operation-name': 'select-node-and-navigate-to-author-guidance-card',
						'operation-context-node-id': sourceNode.nodeId,
						'operation-initial-data':
							'{ "authorGuidanceId": "' + parsedGuidanceAttributes.id + '" }'
					});
				}

				return ['span', spanAttributes, parsedGuidanceAttributes.instruction];
			}
		}
	);

	// span semantic in template
	configureProperties(sxModule, 'self::span[parent::p[parent::li] and ancestor::template]', {
		contextualOperations: [],
		markupLabel: 'content type',
		backgroundColor: 'blue',
		allowMergingWith: 'self::span[parent::p[parent::li] and ancestor::template]',
		emptyElementPlaceholderText: t('type the content type')
	});

	// span output option name in template
	configureAsBlock(
		sxModule,
		'self::span[parent::div[parent::div[parent::template]]]',
		'output option name',
		{
			contextualOperations: [],
			emptyElementPlaceholderText: t('type the output option name')
		}
	);

	// span output option value in template
	configureAsBlock(
		sxModule,
		'self::span[parent::p[parent::div[parent::div[parent::template]]]]',
		'output option value',
		{
			contextualOperations: [],
			blockBefore: [createLabelQueryWidget('"\u25cf"')],
			emptyElementPlaceholderText: t('type the output option value')
		}
	);

	// style
	configureAsRemoved(sxModule, 'self::style', 'style');

	// template
	configureAsStructure(sxModule, 'self::template', 'definition');

	// title (in head)
	configureAsRemoved(sxModule, 'self::title', 'title');

	// ul
	configureAsListElements(sxModule, {
		list: {
			nodeName: 'ul',
			style: configureAsListElements.BULLETED_LIST_STYLE
		},
		item: {
			nodeName: 'li'
		},
		paragraph: {
			nodeName: 'p'
		}
	});
	configureProperties(sxModule, 'self::ul', {
		markupLabel: getMarkupLabelXPath('bulleted list'),
		contextualOperations: [
			{ name: 'semantics::set-element-type', hideIn: ['context-menu', 'breadcrumbs-menu'] },
			{ name: 'contextual-set-output-options', hideIn: ['context-menu', 'breadcrumbs-menu'] }
		]
	});

	// ul in output
	configureProperties(sxModule, 'self::ul[ancestor::html[@data-is-output-document]]', {
		contextualOperations: []
	});

	// ul in template
	configureAsFrame(sxModule, 'self::ul[parent::template]', 'content types', {
		contextualOperations: [],
		blockHeaderLeft: [createMarkupLabelWidget()]
	});

	// ul in template
	configureProperties(sxModule, 'self::ul[ancestor::template]', {
		markupLabel: 'content types',
		contextualOperations: []
	});

	// ol / ul / li / p > autoRemoveableIfEmpty > true
	sxModule
		.configure('fontoxml-base-flow')
		.forNodesMatching(
			'self::ol or self::ul or self::li or self::p[parent::li and not(@data-conref)]',
			2
		)
		.isAutoremovableIfEmpty(true);

	// aside
	configureAsInlineAnchorToStructure(sxModule, 'self::aside', 'footnote', {
		defaultTextContainer: 'p',
		endDelimiter: ']',
		startDelimiter: '['
	});

	// Disallow nested aside
	configureAsInvalid(sxModule, 'self::aside[ancestor::aside]');
}
