import addAction from 'fontoxml-operations/src/addAction.js';
import addCustomMutation from 'fontoxml-base-flow/src/addCustomMutation.js';
import addTransform from 'fontoxml-operations/src/addTransform.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import domQuery from 'fontoxml-dom-utils/src/domQuery.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import operationsManager from 'fontoxml-operations/src/operationsManager.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import editEditorSidebarTab from 'fontoxml-editor/src/editEditorSidebarTab.js';
import registerEditorSidebarTab from 'fontoxml-editor/src/registerEditorSidebarTab.js';
import t from 'fontoxml-localization/src/t.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';
import overlayViewManager from 'fontoxml-templated-views/src/overlayViewManager.js';

import AuthorGuidanceModal from './ui/authorGuidances/AuthorGuidanceModal.jsx';
import ChangeAltTextPopover from './ui/ChangeAltTextPopover.jsx';
import EditOutputOptionsModal from './ui/EditOutputOptionsModal.jsx';
import EditVariablesModal from './ui/EditVariablesModal.jsx';
import ShowSemanticsModal from './ui/ShowSemanticsModal.jsx';
import ToggleExternalFileIconWidgetOverlayView from './ui/ToggleExternalFileIconWidgetOverlayView.js';

import convertH1InPAndMergeSection from './api/convertH1InPAndMergeSection.js';
import convertPInH1AndSplitSection from './api/convertPInH1AndSplitSection.js';
import indentListItems from './api/indentListItems.js';
import indentSection from './api/indentSection.js';
import insertSemantic from './api/insertSemantic.js';
import mergeH1InP from './api/mergeH1InP.js';
import mergePInH1 from './api/mergePInH1.js';
import outdentSection from './api/outdentSection.js';
import saveVariables from './api/saveVariables.js';
import sectionNumberingTypeManager from './api/sectionNumberingTypeManager.js';
import setSectionLevel from './api/setSectionLevel.js';
import setSemanticLevelAttributeValue from './api/setSemanticLevelAttributeValue.js';
import splitH1 from './api/splitH1.js';
import AuthorGuidancesSidebar from './ui/authorGuidances/AuthorGuidancesSidebar.jsx';
import authorGuidancesManager from './api/authorGuidances/authorGuidancesManager.js';

import modeSwitchingManager from 'fonto-html-mode-switching/src/api/modeSwitchingManager.js';

export default function install() {
	addCustomMutation('convert-h1-in-p-and-merge-section', convertH1InPAndMergeSection);
	addCustomMutation('convert-p-in-h1-and-split-section', convertPInH1AndSplitSection);
	addCustomMutation('indent-list-items', indentListItems);
	addCustomMutation('indent-section', indentSection);
	addCustomMutation('insert-semantic', insertSemantic);
	addCustomMutation('merge-h1-in-p', mergeH1InP);
	addCustomMutation('merge-p-in-h1', mergePInH1);
	addCustomMutation('outdent-section', outdentSection);
	addCustomMutation('save-variables', saveVariables);
	addCustomMutation('set-section-level', setSectionLevel);
	addCustomMutation('set-semantic-level-attribute-value', setSemanticLevelAttributeValue);
	addCustomMutation('split-h1', splitH1);

	uiManager.registerReactComponent('AuthorGuidanceModal', AuthorGuidanceModal);
	uiManager.registerReactComponent('ChangeAltTextPopover', ChangeAltTextPopover);
	uiManager.registerReactComponent('EditOutputOptionsModal', EditOutputOptionsModal);
	uiManager.registerReactComponent('EditVariablesModal', EditVariablesModal);
	uiManager.registerReactComponent('ShowSemanticsModal', ShowSemanticsModal);

	operationsManager.addAlternativeOperation('tab', '_indent-list-items', 110);
	operationsManager.addAlternativeOperation('tab', 'insert-di-on-tab', 85);
	operationsManager.addAlternativeOperation('tab', 'insert-output-option-on-tab', 80);
	operationsManager.addAlternativeOperation('tab', '_convert-p-and-split-section', 40);
	operationsManager.addAlternativeOperation('tab', 'indent-section--keybinding', 30);

	operationsManager.addAlternativeOperation('shift-tab', 'outdent-section--keybinding', 40);
	operationsManager.addAlternativeOperation('shift-tab', 'convert-h1-to-p', 30);

	operationsManager.addAlternativeOperation('move-item-up', 'move-list-item-up', 75);
	operationsManager.addAlternativeOperation('move-item-up', 'move-section-up', 50);

	operationsManager.addAlternativeOperation('move-item-down', 'move-list-item-down', 75);
	operationsManager.addAlternativeOperation('move-item-down', 'move-section-down', 50);

	overlayViewManager.useOverlay(['content-view', 'content-preview'], function(
		templatedViewController
	) {
		return new ToggleExternalFileIconWidgetOverlayView(
			templatedViewController.getViewRootNode(),
			templatedViewController.getTemplatedView()
		);
	});

	addAction(
		'toggle-section-numbering-type',
		function(_stepData) {
			sectionNumberingTypeManager.toggleType();
		},
		function(_stepData) {
			return {
				active: sectionNumberingTypeManager.getType() !== 'heading-level',
				enabled: true
			};
		}
	);

	addTransform('setContextNodeIdToRootDocument', function setContextNodeIdToRootDocument(
		stepData
	) {
		const firstHierarchyNode = documentsHierarchy.getFirstVisibleHierarchyNode();
		if (!firstHierarchyNode || !firstHierarchyNode.documentReference) {
			return stepData;
		}
		const globalDocumentNode = documentsManager.getDocumentNode(
			firstHierarchyNode.documentReference.documentId
		);
		const globalHtmlNode = globalDocumentNode
			? evaluateXPathToFirstNode(
					'//html[@data-is-root-document or @data-is-output-document]',
					globalDocumentNode,
					readOnlyBlueprint
			  )
			: null;

		if (globalHtmlNode) {
			stepData.documentId = firstHierarchyNode.documentReference.documentId;
			stepData.contextNodeId = getNodeId(globalHtmlNode);
		}

		return stepData;
	});

	// TODO: not sure if we need this?
	// Currently used by "_unwrap-div" > "insert-div"
	addTransform(
		'setOperationStateForToggle',
		function(stepData) {
			return stepData;
		},
		function setOperationStateForToggle(stepData) {
			const contextNode = documentsManager.getNodeById(stepData.contextNodeId);

			if (
				contextNode &&
				(!stepData.class ||
					stepData.class ===
						evaluateXPathToString('@class', contextNode, readOnlyBlueprint))
			) {
				stepData.operationState = {
					enabled: true,
					active: true
				};
				return stepData;
			}
			stepData.operationState = {
				enabled: false
			};
			return stepData;
		}
	);

	// TODO: check if in core, if not, add to fontoxml-cms-browser?
	addTransform(
		'setBrowseContextDocumentIdForDocumentId',
		function setBrowseContextDocumentIdForDocumentId(stepData) {
			if (stepData.documentId) {
				stepData.browseContextDocumentId = documentsManager.getDocumentFile(
					stepData.documentId
				).remoteDocumentId;
			}

			return stepData;
		}
	);

	addTransform(
		'setContextNodeIdToSelectedElementOrSelectionAncestor',
		function setContextNodeIdToSelectedElementOrSelectionAncestor(stepData) {
			const selectedElement = selectionManager.getSelectedElement();
			if (selectedElement) {
				const selector = stepData.selectionAncestorNodeSpec;
				const matchingAncestor = domQuery.findClosestAncestor(selectedElement, function(
					node
				) {
					return evaluateXPathToBoolean(selector, node, readOnlyBlueprint);
				});

				if (matchingAncestor) {
					stepData.contextNodeId = getNodeId(matchingAncestor);
				}
			}

			return stepData;
		}
	);

	addTransform('createTargetForConref', function createTargetForConref(stepData) {
		const targetNodeId = stepData.nodeId;
		const targetDocumentId = stepData.targetDocumentId;

		const targetRemoteDocumentId = documentsManager.getDocumentFile(targetDocumentId)
			.remoteDocumentId;

		const targetDocument = documentsManager.getDocument(targetDocumentId);
		const targetDocumentElement = targetDocument.dom.documentNode.documentElement;
		const targetNode =
			targetNodeId && documentsManager.getNodeById(targetNodeId, targetDocumentId);

		// For links to the document element, there is no fragment
		const isLinkToElementWithinDocument = targetNode && targetNode !== targetDocumentElement;
		if (!isLinkToElementWithinDocument) {
			stepData.nodeReference = targetRemoteDocumentId;
			return stepData;
		}

		const nodeReference =
			targetRemoteDocumentId +
			'#' +
			evaluateXPathToString('@id', targetNode, readOnlyBlueprint);

		stepData.nodeReference = nodeReference;
		return stepData;
		// Build the fragment by combining the target ID with the ID of the surrounding topic
		// return evaluateXPathToString(
		// 	[
		// 		'if (fonto:dita-class(., "topic/topic")) then',
		// 		// (: For links to a topic, the fragment is the topic\'s ID :)
		// 		'  $relativeUrl || "#" || @id',
		// 		'else',
		// 		// (: For links to an element within a topic, use a combined fragment using the topic ID and node ID:)
		// 		'  let $closestTopicElement := ancestor::*[fonto:dita-class(., "topic/topic")][1]',
		// 		'  return if ($closestTopicElement) then',
		// 		'    $relativeUrl || "#" || $closestTopicElement/@id || "/" || @id',
		// 		'  else',
		// 		//   (: Not the root but also not within a topic, just use the node\'s ID :)
		// 		'    $relativeUrl || "#" || @id'
		// 	].join('\n'),
		// 	targetNode,
		// 	readOnlyBlueprint,
		// 	{ relativeUrl: relativeUrl }
		// );
	});

	addTransform('setNodeNameByNodeId', function setNodeNameByNodeId(stepData) {
		if (!stepData.nodeId) {
			stepData.operationState = {
				enabled: false,
				active: false
			};
			return stepData;
		}

		const node = documentsManager.getNodeById(stepData.nodeId, stepData.documentId);
		if (!node || !node.nodeName) {
			stepData.operationState = {
				enabled: false,
				active: false
			};
			return stepData;
		}

		stepData.nodeName = node.nodeName;

		return stepData;
	});

	registerEditorSidebarTab({
		id: 'author-guidances',
		icon: 'lightbulb',
		label: t('Guidance'),
		tooltipContent: t('Explore the author guidances'),
		Component: AuthorGuidancesSidebar,
		priority: 60
	});

	editEditorSidebarTab({
		id: 'review',
		label: 'Review'
	});

	addAction(
		'add-author-guidance',
		stepData => {
			authorGuidancesManager.addGuidance(stepData);
		},
		() => {
			return {
				active: false,
				enabled: true
			};
		}
	);

	addAction(
		'edit-author-guidance',
		stepData => {
			authorGuidancesManager.editGuidance(stepData.oldGuidance, stepData.newGuidance);
		},
		() => {
			return {
				active: false,
				enabled: true
			};
		}
	);

	addAction('remove-author-guidance', stepData => {
		authorGuidancesManager.removeGuidance(stepData);
	});

	addAction('navigate-to-author-guidance-card', stepData => {
		if (stepData.authorGuidanceId) {
			authorGuidancesManager.selectAuthorGuidance(stepData.authorGuidanceId);
		}
	});

	addAction(
		'open-output-sidebar',
		() => {
			if (modeSwitchingManager.mode === 'author') {
				operationsManager.executeOperation('create-output');
			}
		},
		() => {
			return {
				active: false,
				enabled: true
			};
		}
	);

	addTransform('setRemoteDocumentId', function getRemoteDocumentId(stepData) {
		const documents = documentsHierarchy.findAll(() => true);
		stepData.remoteDocumentId = '';
		if (documents && documents.length > 0) {
			stepData.remoteDocumentId = documents[0].documentReference.remoteDocumentId;
		}

		return stepData;
	});
}
