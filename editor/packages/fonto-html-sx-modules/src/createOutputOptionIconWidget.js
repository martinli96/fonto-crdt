import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';

function getValuesByNameFromfilterUnits(valuesByName, filterUnit) {
	const filterUnitArr = filterUnit.split(':');
	const name = filterUnitArr[0];
	const value = filterUnitArr[filterUnitArr.length - 1];
	if (!valuesByName[name]) {
		valuesByName[name] = [];
	}
	valuesByName[name].push(value);
	return valuesByName;
}

export default function createOutputOptionIconWidget(selector, options) {
	options = options || {};
	return function createFilterIconWidgetJsonMl(sourceNode) {
		const contextNode = selector
			? sourceNode.findRelatedNodes(function(sourceNodeProxy) {
					return evaluateXPathToNodes(selector, sourceNodeProxy, readOnlyBlueprint);
			  })[0]
			: sourceNode;
		const onlyFor = evaluateXPathToStrings(
				'./@data-only-for/tokenize(.)',
				contextNode,
				readOnlyBlueprint
			).reduce(getValuesByNameFromfilterUnits, {}),
			notFor = evaluateXPathToStrings(
				'./@data-not-for/tokenize(.)',
				contextNode,
				readOnlyBlueprint
			).reduce(getValuesByNameFromfilterUnits, {});

		const hasOnlyFor = Object.keys(onlyFor).length > 0;
		const hasNotFor = Object.keys(notFor).length > 0;

		const tooltipContent = {
			'tooltip-content':
				(hasOnlyFor
					? t('Only for: ') +
					  Object.keys(onlyFor)
							.map(name => name + ' [' + onlyFor[name].join(', ') + ']')
							.join(', ')
					: '') +
				(hasOnlyFor && hasNotFor ? ' ' : '') +
				(hasNotFor
					? t('Not for: ') +
					  Object.keys(notFor)
							.map(name => name + ' [' + notFor[name].join(', ') + ']')
							.join(', ')
					: '')
		};

		const attributes = Object.assign(tooltipContent, {
			'data-is-inline': options.isInline,
			'block-selection-change-on-click': true,
			'operation-name': 'contextual-set-output-options',
			'operation-context-node-id': getNodeId(contextNode)
		});

		return ['cv-icon-widget', attributes, uiManager.getIconJsonML('filter')];
	};
}
