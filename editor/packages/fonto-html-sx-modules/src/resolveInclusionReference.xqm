module namespace fonto = "http://www.fontoxml.com/functions";

import module namespace fonto-doc-load = "http://www.fontoxml.com/functions/document-loading";

declare %private function fonto:extract-document-id($raw-id as xs:string, $element as element()) as xs:string {
	(:Id is of the form DOCUMENT_ID#unused/ID:)
	if (fn:substring-before($raw-id, '#')) then
		(: normal reference :)
		fn:substring-before($raw-id, '#')
	else if (contains($raw-id, "#")) then
		(: Reference in same document :)
		fonto:remote-document-id($element)
	else
		(: Reference to full document :)
		$raw-id
};

declare %private function fonto:find-target-node ($doc as node(), $id as xs:string) as element()* {
	(:Id is of the form DOCUMENT_ID#unused/ID:)
	let $afterHash := fn:substring-after($id, '#'),
		$target := if (contains($afterHash, "/")) then
			$afterHash => fn:substring-after("/")
		else
			$afterHash
	return if ($target) then ($doc/descendant-or-self::*[@id = $target], $conrefElement/node()) else ($doc/node(), $conrefElement/node())
};

declare %public function fonto:resolve-data-conref ($conrefElement as element()) as map(*) {
	fonto-doc-load:resolve-content-reference(
		fonto:extract-document-id($conrefElement/@data-conref, $conrefElement),
		fonto:find-target-node(?, $conrefElement/@data-conref)
    )
};

declare %public function fonto:resolve-document-ref ($conrefElement as element()) as map(*) {
	fonto-doc-load:resolve-content-reference(
		fonto:extract-document-id($conrefElement/@src, $conrefElement),
		fonto:find-target-node(?, $conrefElement/@src)
	)
};