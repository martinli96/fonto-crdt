import createInlineFrameJsonMl from 'fontoxml-families/src/createInlineFrameJsonMl.js';
import createInlineLabelWidget from 'fontoxml-families/src/createInlineLabelWidget.js';
import createLabelQueryWidget from 'fontoxml-families/src/createLabelQueryWidget.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import t from 'fontoxml-localization/src/t.js';

import modeSwitchingManager from 'fonto-html-mode-switching/src/api/modeSwitchingManager.js';
import ModeType from 'fonto-html-mode-switching/src/ModeType.js';

export default function createVariableInnerJsonMl(sourceNode, renderer) {
	const variableName = evaluateXPathToString('@class', sourceNode, readOnlyBlueprint);
	const variableValue = variableName
		? sourceNode.getExternalValue('variable-value-by-name|' + variableName)
		: null;
	const isNotDefined = variableName ? variableValue === undefined : true;

	// Tooltip for variable.
	// If it's not defined, render undefined tooltip.
	// Else, in author mode, render value and double click message.
	// In template mode, render just the name.
	let tooltip;
	if (isNotDefined) {
		tooltip = variableName + ': ' + t('Remove this variable as it is not defined.');
	} else if (modeSwitchingManager.mode === ModeType.AUTHOR) {
		tooltip = variableName + ': ' + t('Double click to set value.');
	} else {
		tooltip = variableName;
	}

	return createInlineFrameJsonMl(
		[
			'cv-content',
			variableValue
				? createLabelQueryWidget('"' + variableValue + '"', {
						inline: true
				  })(sourceNode, renderer)
				: createInlineLabelWidget(
						isNotDefined
							? 'undefined variable'
							: variableValue === ''
							? 'empty value'
							: variableName
				  )(sourceNode, renderer)
		],
		sourceNode,
		renderer,
		{
			backgroundColor: 'purple',
			showWhen: 'always',
			tooltip: tooltip
		}
	);
}
