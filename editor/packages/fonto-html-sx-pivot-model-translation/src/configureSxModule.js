import pivotModelTransformerManager from 'fontoxml-pivot-model/src/pivotModelTransformerManager.js';

export default function configureSxModule(sxModule) {
	pivotModelTransformerManager.registerTransformers(sxModule, [
		{
			qualifiedName: 'table',
			isTable: true,
			cellQualifiedName: 'td'
		},
		{
			qualifiedName: 'span',
			type: 'inline'
		},
		{
			qualifiedName: 'p',
			type: 'block'
		},
		{
			qualifiedName: null,
			type: 'frame'
		},
		{
			qualifiedName: 'ul',
			type: 'group',
			flags: ['unordered-list'],
			contents: [
				{
					qualifiedName: 'li',
					type: 'frame'
				}
			]
		},
		{
			qualifiedName: 'ol',
			type: 'group',
			flags: ['ordered-list'],
			contents: [
				{
					qualifiedName: 'li',
					type: 'frame'
				}
			]
		},
		{
			qualifiedName: 'ul',
			type: 'group',
			contents: [
				{
					qualifiedName: 'li',
					type: 'frame'
				}
			]
		}
	]);
}
