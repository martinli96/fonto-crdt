import addAction from 'fontoxml-operations/src/addAction.js';
import addTransform from 'fontoxml-operations/src/addTransform.js';
import connectorsManager from 'fontoxml-configuration/src/connectorsManager.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import initialDocumentsManager from 'fontoxml-remote-documents/src/initialDocumentsManager.js';
import openModal from 'fontoxml-fx/src/openModal.js';
import operationsManager from 'fontoxml-operations/src/operationsManager.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';

import openDocumentInNewTab from 'fonto-html-masthead/src/utils/openDocumentInNewTab.js';

import filesManager from './filesManager.js';
import getExternalFragmentDocumentId from './sectionToFragment/getExternalFragmentDocumentId.js';
import OutputPreviewModal from './ui/OutputPreviewModal.jsx';
import SaveSectionToFragmentModal from './ui/SaveSectionToFragmentModal.jsx';
import updateDocumentTitleInParentFrame from './updateDocumentTitleInParentFrame.js';

export default function install() {
	uiManager.registerReactComponent('OutputPreviewModal', OutputPreviewModal);
	uiManager.registerReactComponent('SaveSectionToFragmentModal', SaveSectionToFragmentModal);

	addAction('openOutputFileInNewWindow', function openOutputFileInNewWindow(stepData) {
		if (!stepData.remoteDocumentId) {
			return addAction.CANCEL_OPERATION;
		}

		const cmsClient = connectorsManager.getCmsClient();
		let url = cmsClient.getUrlForEndpoint('/asset');
		url += '?context=' + encodeURIComponent(JSON.stringify(cmsClient.buildContext()));
		url += '&id=' + encodeURIComponent(stepData.remoteDocumentId);

		window.open(url);
	});

	addTransform('createFile', function createFileTransform(stepData) {
		return filesManager
			.createFile(stepData)
			.then(remoteDocumentId => ({ ...stepData, remoteDocumentId }));
	});

	documentsHierarchy.hierarchyChangedNotifier.addCallback(updateDocumentTitleInParentFrame);
	documentsManager.addDocumentsCollectionChangedCallback(
		{ 'cap/operable': true },
		updateDocumentTitleInParentFrame
	);

	initialDocumentsManager.initialDocumentsLoadedNotifier.addCallback(function() {
		if (documentsManager.getAllDocumentIds({ 'cap/operable': true }).length > 0) {
			return;
		}
	});

	addAction('goToEditorRouteWithDocument', function goToEditorRouteWithDocument(stepData) {
		uiManager.go('editor', { documentIds: [stepData.remoteDocumentId] });
	});

	addAction(
		'save-section-to-fragment',
		stepData => {
			openModal(SaveSectionToFragmentModal, stepData);
		},
		stepData => {
			if (!stepData || !stepData.contextNodeId) {
				return {
					active: true,
					enabled: false
				};
			}

			const documentId = getExternalFragmentDocumentId(stepData.contextNodeId);
			operationsManager.invalidateOperationStatesByStepType(
				'action',
				'save-section-to-fragment'
			);
			return {
				active: true,
				enabled: documentId === null
			};
		}
	);

	addAction(
		'edit-section-fragment',
		stepData => {
			if (stepData) {
				const documentId = getExternalFragmentDocumentId(stepData.contextNodeId);
				openDocumentInNewTab(documentId);
			}
		},
		stepData => {
			if (!stepData || !stepData.contextNodeId) {
				return {
					active: true,
					enabled: false
				};
			}

			const documentId = getExternalFragmentDocumentId(stepData.contextNodeId);
			operationsManager.invalidateOperationStatesByStepType(
				'action',
				'edit-section-fragment'
			);
			return {
				active: true,
				enabled: documentId !== null
			};
		}
	);
}
