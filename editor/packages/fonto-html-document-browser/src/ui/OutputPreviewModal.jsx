import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';

import {
	Button,
	Flex,
	Heading,
	Icon,
	Label,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalContentToolbar,
	SpinnerIcon,
	StateMessage,
	Toast
} from 'fds/components';
import { applyCss, flex } from 'fds/system';

import connectorsManager from 'fontoxml-configuration/src/connectorsManager.js';
import dataProviders from 'fontoxml-cms-browser/src/dataProviders.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import serializeNodeToXml from 'fontoxml-dom-utils/src/serializeNodeToXml.js';
import t from 'fontoxml-localization/src/t.js';

import DocumentListItem from 'fontoxml-cms-browser/src/documents/DocumentListItem.jsx';
import ModalBrowserFileAndFolderResultList from 'fontoxml-cms-browser/src/shared/ModalBrowserFileAndFolderResultList.jsx';
import ModalBrowserHierarchyBreadcrumbs from 'fontoxml-cms-browser/src/shared/ModalBrowserHierarchyBreadcrumbs.jsx';

import defaultCssContent from '../default.css.js';
import defaultHFContent from '../default.hf.html.js';

const assetConnector = connectorsManager.getConnector('asset-connector');

let dataProvider = null;

const cssStateLabels = {
	loading: {
		title: t('Loading .css files…'),
		message: null
	},
	browseError: {
		title: t('Can’t open this folder'),
		message: t('Fonto can’t open this folder. You can try again, or try a different folder.')
	},
	empty: {
		title: t('No results'),
		message: t('This folder does not contain any .css files.')
	}
};
const hfStateLabels = {
	loading: {
		title: t('Loading .hf.html files…'),
		message: null
	},
	browseError: {
		title: t('Can’t open this folder'),
		message: t('Fonto can’t open this folder. You can try again, or try a different folder.')
	},
	empty: {
		title: t('No results'),
		message: t('This folder does not contain any .hf.html files.')
	}
};

const iframeStyles = applyCss([flex('column'), { flex: 1, border: 'none' }]);

function OutputPreviewModal({ cancelModal, data, submitModal }) {
	const isMountedInDOM = useRef(true);

	const [isLoading, setIsLoading] = useState(true);
	const [isErrored, setIsErrored] = useState(false);

	const [htmlDocument, setHtmlDocument] = useState(null);
	const [cssContent, setCssContent] = useState(defaultCssContent);
	const [hfContent, setHFContent] = useState(defaultHFContent);

	const [cachedErrorByRemoteId, setItemIsErrored] = useState({});
	const [hierarchyItems, setHierarchyItems] = useState([]);
	const [request, setRequest] = useState({});

	const [cssFileItems, setCSSFileItems] = useState([]);
	const [hfFileItems, setHFFileItems] = useState([]);

	const [selectedCssFile, setSelectedCssFile] = useState(null);
	const [selectedHFFile, setSelectedHFFile] = useState(null);

	// Used to update the items with a browse callback
	const refreshItems = useCallback(
		(browseContextDocumentId, folderToLoad, noCache) => {
			if (!isMountedInDOM.current) {
				return;
			}
			setRequest({ type: 'browse', busy: true });

			return dataProvider
				.getFolderContents(data.browseContextDocumentId, folderToLoad, noCache, null)
				.then(
					result => {
						if (!isMountedInDOM.current) {
							return [];
						}

						const cssFileItems = result.items.filter(
							item => item.type === 'folder' || item.label.endsWith('.css')
						);
						const hfFileItems = result.items.filter(
							item => item.type === 'folder' || item.label.endsWith('.hf.html')
						);

						setCSSFileItems(cssFileItems);
						setHFFileItems(hfFileItems);

						setHierarchyItems(result.hierarchyItems);

						setRequest({});

						return result.items;
					},
					error => {
						if (!isMountedInDOM.current || !error) {
							// Modal is already closed or the old request was cancelled, wait for the newer one.
							return;
						}

						setSelectedCssFile(null);
						setRequest({ type: 'browse', error: error });
					}
				);
		},
		[data.browseContextDocumentId]
	);

	useEffect(() => {
		const documentId = data.documentId;
		if (!documentId) {
			setIsErrored(true);
			setIsLoading(false);
			return;
		}
		const documentNode = documentsManager.getDocumentNode(documentId);
		if (!documentNode) {
			setIsErrored(true);
			setIsLoading(false);
			return;
		}

		// IframeDocumentConnector returns a Promise, StandardDocumentConnector doesn't.
		// Because of this always wrap this instance into a Promise.
		const htmlDocument = serializeNodeToXml(documentNode.documentElement);
		setHtmlDocument(htmlDocument);
		setIsLoading(false);
	}, [data.documentId]);

	useEffect(() => {
		isMountedInDOM.current = true;
		dataProvider = dataProviders.get(data.dataProviderName);

		refreshItems(null, { id: null });

		return () => {
			isMountedInDOM.current = false;
		};
	}, [data, data.dataProviderName, refreshItems]);

	const handleRenderCSSFileItem = ({ key, item, onClick, onDoubleClick, onRef }) => (
		<DocumentListItem
			key={key}
			isDisabled={item.isDisabled || isLoading || isErrored}
			isErrored={!!cachedErrorByRemoteId[item.id]}
			isSelected={selectedCssFile && selectedCssFile.id === item.id}
			item={item}
			onClick={onClick}
			onDoubleClick={onDoubleClick}
			onRef={onRef}
		/>
	);

	const handleRenderHFFileItem = ({ key, item, onClick, onDoubleClick, onRef }) => (
		<DocumentListItem
			key={key}
			isDisabled={item.isDisabled || isLoading}
			isSelected={selectedHFFile && selectedHFFile.id === item.id}
			item={item}
			onClick={onClick}
			onDoubleClick={onDoubleClick}
			onRef={onRef}
		/>
	);

	const handleCssFileSelect = useCallback(
		newSelectedItem => {
			if (!isMountedInDOM.current) {
				return;
			}

			setSelectedCssFile(newSelectedItem);

			if (newSelectedItem.type === 'folder') {
				return;
			}

			setIsLoading(true);

			assetConnector
				.getDownloadUrl(
					documentsManager.getDocumentFile(data.rootDocumentId),
					newSelectedItem.id
				)
				.then(url =>
					fetch(url)
						.then(response => {
							if (!response.ok) {
								throw Error(response.statusText);
							}
							return response.text();
						})
						.then(text => {
							if (!isMountedInDOM.current) {
								return;
							}

							setCssContent(text);
							setIsLoading(false);

							if (!cachedErrorByRemoteId[newSelectedItem.id]) {
								return;
							}

							const updatedCachedErrorByRemoteId = { ...cachedErrorByRemoteId };
							delete updatedCachedErrorByRemoteId[newSelectedItem.id];
							setItemIsErrored(updatedCachedErrorByRemoteId);
						})
				)
				.catch(error => {
					const newCachedErrorByRemoteId = { ...cachedErrorByRemoteId };
					newCachedErrorByRemoteId[newSelectedItem.id] = error;
					setItemIsErrored(newCachedErrorByRemoteId);
					setIsLoading(false);
				});
		},
		[cachedErrorByRemoteId, data.rootDocumentId]
	);

	const handleHFFileSelect = useCallback(
		newSelectedItem => {
			if (!isMountedInDOM.current) {
				return;
			}

			setSelectedHFFile(newSelectedItem);

			if (newSelectedItem.type === 'folder') {
				return;
			}

			setIsLoading(true);

			assetConnector
				.getDownloadUrl(
					documentsManager.getDocumentFile(data.rootDocumentId),
					newSelectedItem.id
				)
				.then(url =>
					fetch(url)
						.then(response => {
							if (!response.ok) {
								throw Error(response.statusText);
							}

							return response.text();
						})
						.then(text => {
							if (!isMountedInDOM.current) {
								return;
							}

							setHFContent(text);
							setIsLoading(false);
						})
				)
				.catch(_error => {
					setIsLoading(false);
				});
		},
		[data.rootDocumentId]
	);

	const htmlDocumentWithCSSAndHF = useMemo(() => {
		if (!htmlDocument) {
			return null;
		}

		const htmlDocumentWithCSS = htmlDocument.replace('<!-- {{fbb:css}} -->', cssContent);

		const hfContentWithRegExSubstitutionTokens = hfContent
			.replace(/<template>/gim, '$1')
			.replace(/<!-- {{ffb:content}} -->/gim, '$2')
			.replace(/<\/template>/gim, '$3');

		const finalOutputDocument = htmlDocumentWithCSS.replace(
			/(<body.*?>)(.*)(<\/body>)/gim,
			hfContentWithRegExSubstitutionTokens
		);

		return finalOutputDocument;
	}, [cssContent, hfContent, htmlDocument]);

	return (
		<Modal size="l" isFullHeight>
			<ModalHeader icon="eye" title={t('Create an output document')} />

			<ModalBody>
				<ModalContent flexDirection="row">
					<ModalContent flex="1" flexDirection="column">
						{hierarchyItems.length > 0 && (
							<ModalContentToolbar>
								<ModalBrowserHierarchyBreadcrumbs
									hierarchyItems={hierarchyItems}
									refreshItems={refreshItems}
									request={request}
									isDisabled={isLoading || isErrored}
								/>
							</ModalContentToolbar>
						)}

						<ModalContent flex="none" flexDirection="row">
							<ModalContent flex="1" flexDirection="column" paddingSize="m">
								<Flex flexDirection="row" spaceSize="m">
									<Icon icon="eye" />

									<Heading level="4">{t('Choose your look & feel')}</Heading>
								</Flex>

								<Label>{t('Select a .css file from your Styles folder')}</Label>
							</ModalContent>

							<ModalContent flex="1" flexDirection="column" paddingSize="m">
								<Flex flexDirection="row" flex="none" spaceSize="s">
									<Icon icon="heading" />

									<Heading level="4">{t('Choose your header & footer')}</Heading>
								</Flex>

								<Label>{t('Select a .hf.html file from your Styles folder')}</Label>
							</ModalContent>
						</ModalContent>

						<ModalContent flex="1" flexDirection="row">
							<ModalContent flex="1" flexDirection="column">
								<ModalBrowserFileAndFolderResultList
									items={cssFileItems}
									onItemSelect={handleCssFileSelect}
									refreshItems={refreshItems}
									renderListItem={handleRenderCSSFileItem}
									request={request}
									selectedItem={selectedCssFile}
									stateLabels={cssStateLabels}
								/>
							</ModalContent>

							<ModalContent flex="1" flexDirection="column">
								<ModalBrowserFileAndFolderResultList
									items={hfFileItems}
									onItemSelect={handleHFFileSelect}
									refreshItems={refreshItems}
									renderListItem={handleRenderHFFileItem}
									request={request}
									selectedItem={selectedHFFile}
									stateLabels={hfStateLabels}
								/>
							</ModalContent>
						</ModalContent>
					</ModalContent>

					<ModalContent flex="1" flexDirection="column">
						{selectedCssFile && cachedErrorByRemoteId[selectedCssFile.id] && (
							<ModalContent flex="none" paddingSize="m">
								<Toast
									connotation="error"
									icon="exclamation-triangle"
									content={t('The css could not be loaded')}
								/>
							</ModalContent>
						)}

						<ModalContent flexDirection="column">
							{isErrored && (
								<StateMessage
									connotation="error"
									visual="exclamation-triangle"
									title={t('Can’t preview this document')}
								/>
							)}

							{isLoading && (
								<StateMessage
									visual={<SpinnerIcon />}
									title={t('Loading preview…')}
								/>
							)}

							{!isLoading && !isErrored && (
								<ModalContent flexDirection="column">
									<iframe {...iframeStyles} srcDoc={htmlDocumentWithCSSAndHF} />
								</ModalContent>
							)}
						</ModalContent>
					</ModalContent>
				</ModalContent>
			</ModalBody>

			<ModalFooter>
				<Button label="Close" onClick={cancelModal} />

				<Flex spaceSize="s">
					<Button
						label={t('Save')}
						isDisabled={isLoading || isErrored}
						onClick={() =>
							submitModal({
								fileName: `output-on-${new Date().toISOString()}`,
								fileExtension: '.html',
								fileContent: htmlDocumentWithCSSAndHF,
								operationName: '_choose-output-folder-and-create-output-file'
							})
						}
					/>

					<Button
						label={t('Save and download')}
						isDisabled={isLoading || isErrored}
						onClick={() =>
							submitModal({
								fileName: `output-on-${new Date().toISOString()}`,
								fileExtension: '.html',
								fileContent: htmlDocumentWithCSSAndHF,
								operationName: 'save-and-download-output'
							})
						}
					/>
				</Flex>
			</ModalFooter>
		</Modal>
	);
}

export default OutputPreviewModal;
