import React, { useState, useCallback } from 'react';

import {
	Block,
	Button,
	Flex,
	Form,
	FormRow,
	Icon,
	Modal,
	ModalFooter,
	ModalHeader,
	Text,
	TextInput
} from 'fds/components';
import { padding } from 'fds/system';

import operationsManager from 'fontoxml-operations/src/operationsManager.js';
import t from 'fontoxml-localization/src/t.js';

import createSectionToFragmentFile from '../sectionToFragment/createSectionToFragmentFile.js';

const modalBodyStyles = [padding('l'), { backgroundColor: '#ffffff', overflowY: 'auto' }];

function hasInvalidValues(keys, valueByName) {
	return keys.some(key => !valueByName[key]);
}

const formFieldNames = ['fragmentName', 'fragmentPath'];

function SaveSectionToFragmentModal({ data, cancelModal, submitModal }) {
	const [showFeedback, setShowFeedback] = useState(false);
	const [valueByName, setvalueByName] = useState({
		fragmentName: null,
		fragmentPath: null,
		folderData: null
	});

	const handleFormFieldChange = useCallback(({ name, value }) => {
		setvalueByName(prevValueByName => ({ ...prevValueByName, [name]: value }));
		setShowFeedback(false);
	}, []);

	function openTemplateModal() {
		operationsManager
			.executeOperation('open-folder-browser-modal')
			.then(selectedBrowserItem => {
				let path = '';
				selectedBrowserItem.hierarchyItems.forEach(item => {
					path += item.label + '/';
				});
				path += selectedBrowserItem.label;

				setvalueByName(prevValueByName => ({
					...prevValueByName,
					['fragmentPath']: path,
					['folderData']: selectedBrowserItem
				}));
				setShowFeedback(false);
			})
			.catch(() => {});
	}

	function handleSubmit() {
		if (showFeedback) {
			return;
		}

		if (hasInvalidValues(formFieldNames, valueByName)) {
			setShowFeedback(true);
			return;
		}

		createSectionToFragmentFile(data, valueByName);
		submitModal();
	}

	return (
		<Modal size="s">
			<ModalHeader title={t('Save section to fragment')} />
			<Flex applyCss={modalBodyStyles} flex="0 0 auto" flexDirection="column" spaceSize="l">
				<Block
					applyCss={{
						marginLeft: '8px'
					}}
				>
					<Form
						labelPosition="above"
						onFieldChange={handleFormFieldChange}
						valueByName={valueByName}
					>
						<FormRow
							label={t('Title of the fragment')}
							isLabelBold
							labelColorName="text-color"
						>
							<TextInput
								name="fragmentName"
								placeholder={t('Type the title of the fragment')}
							/>
						</FormRow>

						<FormRow label={t('Location')} isLabelBold labelColorName="text-color">
							<Flex spaceSize="m">
								<TextInput
									isDisabled
									name="fragmentPath"
									placeholder={t('Browse the folder structure')}
								/>
								<Button label={t('Browse')} onClick={openTemplateModal} />
							</Flex>
						</FormRow>
					</Form>
				</Block>
			</Flex>

			<ModalFooter>
				<Button label="Cancel" onClick={cancelModal} />

				<Flex flexDirection="row" alignItems="center" spaceSize="m">
					{showFeedback && (
						<Flex spaceSize="s" alignItems="baseline">
							<Icon icon="times-circle" colorName="icon-s-error-color" />
							<Text colorName="text-error-color">
								{t('Not all fields are filled in yet')}
							</Text>
						</Flex>
					)}
					<Button
						isDisabled={showFeedback}
						label={t('Save')}
						onClick={() => handleSubmit()}
						type="primary"
					/>
				</Flex>
			</ModalFooter>
		</Modal>
	);
}

export default SaveSectionToFragmentModal;
