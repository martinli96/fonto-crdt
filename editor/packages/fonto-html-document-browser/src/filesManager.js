import connectorsManager from 'fontoxml-configuration/src/connectorsManager.js';
import RequestData from 'fontoxml-connector/src/RequestData.js';

const cmsClient = connectorsManager.getCmsClient();

class FilesManager {
	constructor() {}

	createFile({ parentFolderId, fileName, fileExtension, fileContent, skipProcessing = false }) {
		const requestData = new RequestData();
		requestData.setContentType('json');
		requestData.addPostParameters({
			context: cmsClient.buildContext(parentFolderId),
			folderId: parentFolderId,
			content: fileContent,
			metadata: { fileName, fileExtension, skipProcessing }
		});

		return cmsClient
			.sendRequest('POST', '/document', requestData)
			.then(response => response.body.documentId);
	}
}
export default new FilesManager();
