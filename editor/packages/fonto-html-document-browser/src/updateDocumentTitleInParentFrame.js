import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import iframeCommunicationManager from 'fontoxml-iframe-communication/src/iframeCommunicationManager.js';

let topLevelDocumentLabel = '';

function updateTopLevelDocumentLabel(label) {
	if (topLevelDocumentLabel !== label) {
		iframeCommunicationManager.postMessageToParent('fontoxml-new-top-level-document', {
			topLevelDocumentLabel: label
		});

		topLevelDocumentLabel = label;
	}
}

export default function updateDocumentTitleInParentFrame() {
	const newTopLevelDocumentHierarchyNode = documentsHierarchy.find(function(hierarchyNode) {
		return hierarchyNode.documentReference && hierarchyNode.documentReference.documentId;
	});

	if (!newTopLevelDocumentHierarchyNode) {
		updateTopLevelDocumentLabel('');
		return;
	}

	const newTopLevelDocumentFile = documentsManager.getDocumentFile(
		newTopLevelDocumentHierarchyNode.documentReference.documentId
	);

	if (!newTopLevelDocumentFile) {
		updateTopLevelDocumentLabel('');
		return;
	}

	let newTopLevelDocumentLabel = 'Untitled';

	if (
		newTopLevelDocumentFile.metadata &&
		newTopLevelDocumentFile.metadata.hierarchy &&
		newTopLevelDocumentFile.metadata.hierarchy.length
	) {
		const newTopLevelDocumentHierarchy = newTopLevelDocumentFile.metadata.hierarchy;
		const hierarchyItem = newTopLevelDocumentHierarchy[newTopLevelDocumentHierarchy.length - 1];

		newTopLevelDocumentLabel = hierarchyItem.label;
	}

	updateTopLevelDocumentLabel(newTopLevelDocumentLabel);
}
