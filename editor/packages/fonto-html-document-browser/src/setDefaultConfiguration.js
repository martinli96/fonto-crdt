import configurationManager from 'fontoxml-configuration/src/configurationManager.js';

export default function setDefaultConfiguration() {
	configurationManager.setDefault('iframe-communication-operations-whitelist', [
		'save-and-close'
	]);

	configurationManager.setDefault('cms-browser-sends-hierarchy-items-in-browse-response', true);

	/**
	 * Set the query used to determine the element in which the title for a new document is inserted.
	 *
	 * @fontosdk
	 *
	 * @const  {XPathQuery|undefined}  document-title-element-xpath
	 * @category  add-on/fontoxml-integration-connectors
	 */
	configurationManager.setDefault('document-title-element-xpath', undefined);
}
