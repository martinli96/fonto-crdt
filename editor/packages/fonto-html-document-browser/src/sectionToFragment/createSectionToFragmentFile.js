import blueprints from 'fontoxml-blueprints/src/main.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import operationsManager from 'fontoxml-operations/src/operationsManager.js';

import filesManager from '../filesManager.js';
import getCustomFragmentFile from './getCustomFragmentFile.js';

const Blueprint = blueprints.Blueprint;

export default function createSectionToFragmentFile(contextualData, infoData) {
	const node = documentsManager.getNodeById(contextualData.contextNodeId);

	let documentId = null;
	const documents = documentsHierarchy.findAll(() => true);
	if (documents && documents.length > 0 && documents[0].documentReference) {
		documentId = documents[0].documentReference.documentId;
	}

	if (!documentId) {
		return null;
	}

	const documentController = documentsManager.getDocumentController(documentId);
	if (!documentController) {
		return;
	}

	const document = documentController.document.dom.documentNode;
	const blueprint = new Blueprint(documentController.document.dom);

	// Check if there is an existing fragment of this node
	const existingFragment = evaluateXPathToNodes('//data-conref', node, blueprint);

	if (existingFragment && existingFragment.length) {
		return;
	}

	const templateRef = evaluateXPathToString('//template-ref/@src', document, blueprint);

	const fileContent = getCustomFragmentFile(
		infoData.fragmentName,
		decodeURIComponent(templateRef),
		decodeURIComponent(node.outerHTML)
	);

	if (fileContent) {
		filesManager
			.createFile({
				parentFolderId: infoData.folderData.id,
				fileName: infoData.fragmentName,
				fileExtension: '.fragment.html',
				// Avoid excessive newlines/tabs/spaces
				fileContent: fileContent.replace(/(\n|\t)/gm, '').replace(/  +/g, ' ')
			})
			.then(remoteDocumentId => {
				const sectionId = node.attributes.find(item => item.name === 'id').B;
				if (!sectionId || !remoteDocumentId) {
					return;
				}
				const conref = remoteDocumentId + '#' + sectionId;
				// Remove section from current document
				blueprint.beginOverlay();
				blueprint.removeChild(blueprint.getParentNode(node), node);
				blueprint.applyOverlay();
				documentController.executeFunction(() => blueprint.realize(), {});
				operationsManager
					.executeOperation('vertical-insert', {
						childNodeStructure: ['section', { 'data-conref': conref }]
					})
					.finally(() => {
						// The current section has changed -> invalidate its operations
						operationsManager.invalidateOperationStatesByStepType(
							'action',
							'edit-section-fragment'
						);
						operationsManager.invalidateOperationStatesByStepType(
							'action',
							'save-section-to-fragment'
						);
					});
			});
	}
}
