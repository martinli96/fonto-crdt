import blueprints from 'fontoxml-blueprints/src/main.js';
import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';

const Blueprint = blueprints.Blueprint;

export default function getExternalFragmentDocumentId(contextNodeId) {
	const node = documentsManager.getNodeById(contextNodeId);

	if (!node) {
		return null;
	}

	let documentId = null;
	const documents = documentsHierarchy.findAll(() => true);
	if (documents && documents.length > 0 && documents[0].documentReference) {
		documentId = documents[0].documentReference.documentId;
	}

	if (!documentId) {
		return null;
	}

	const documentController = documentsManager.getDocumentController(documentId);
	if (!documentController) {
		return null;
	}

	const blueprint = new Blueprint(documentController.document.dom);
	const marker = evaluateXPathToString('//section[@id=$id]/@data-conref', node, blueprint, {
		id: node.getAttribute('id')
	});

	if (!marker) {
		return null;
	}

	const decoded = decodeURIComponent(marker);
	return decoded && decoded.split('#')[0];
}
