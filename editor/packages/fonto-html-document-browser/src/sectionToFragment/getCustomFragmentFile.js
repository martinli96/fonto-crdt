export default function getCustomFragmentFile(fileName, definitionFileId, content) {
	if (!fileName || !content) {
		return null;
	}

	let templateRef = '';
	if (definitionFileId) {
		templateRef = `<template-ref src="${definitionFileId}"/>`;
	}

	return `<?xml version="1.0" encoding="utf-8"?>
	<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:fontoxml:fonto-html.xsd" data-is-root-document="true">
	<head>
		<title>${fileName}</title>
		${templateRef}
	</head>
	<body>
		<h1>${fileName}</h1>
		${content}
	</body>
	</html>`;
}
