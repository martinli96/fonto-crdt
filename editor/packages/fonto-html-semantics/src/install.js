import registerEditorContextMenuSection from 'fontoxml-editor/src/registerEditorContextMenuSection.js';
import uiManager from 'fontoxml-modular-ui/src/uiManager.js';
import renderContextualOperationsContextMenuWithSemantics from './context-menu-section/renderContextualOperationsContextMenuWithSemantics.jsx';
import renderSelectionSemanticsContextMenu from './context-menu-section/renderSelectionSemanticsContextMenu.jsx';
import ChooseSemanticPopover from './semantics/ChooseSemanticPopover.jsx';

export default function install() {
	// above 50 = contextual operations, below 100 = spell suggestions
	registerEditorContextMenuSection(renderContextualOperationsContextMenuWithSemantics, 60);
	registerEditorContextMenuSection(renderSelectionSemanticsContextMenu, 75);

	uiManager.registerReactComponent('ChooseSemanticPopover', ChooseSemanticPopover);
}
