import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';

const LABELS_BY_ELEMENT_NAME = {
	html: 'section',
	section: 'section',
	div: 'block',
	figure: 'figure',
	ol: 'numbered list',
	ul: 'bulleted list',
	table: 'table',
	span: 'inline'
};

export default function getLabelForNode(node) {
	let label = LABELS_BY_ELEMENT_NAME[node.nodeName];
	if (!label) {
		return node.nodeName;
	}

	if (
		node.nodeName === 'html' &&
		evaluateXPathToBoolean('@data-is-root-document', node, readOnlyBlueprint)
	) {
		label = 'document';
	} else if (
		node.nodeName === 'html' &&
		evaluateXPathToBoolean('@data-is-definition-document', node, readOnlyBlueprint)
	) {
		label = 'definition';
	}

	return label;
}
