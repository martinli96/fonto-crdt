import PropTypes from 'prop-types';
import { Component } from 'react';

import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';

class FxFocusedDocument extends Component {
	static defaultProps = {
		renderEmptyState: () => null
	};

	static propTypes = {
		/**
		 * @type {function({ focusedDocumentId: string, focusedDocumentNode: Node, focusedDocumentElement: Element })}
		 */
		children: PropTypes.func.isRequired,
		renderEmptyState: PropTypes.func
	};

	state = { focusedDocumentId: selectionManager.focusedDocumentId };

	render() {
		const { focusedDocumentId } = this.state;
		if (!focusedDocumentId) {
			return this.props.renderEmptyState();
		}

		const focusedDocumentNode = documentsManager.getDocumentNode(focusedDocumentId);
		const focusedDocumentElement = focusedDocumentNode.documentElement;

		return this.props.children({
			focusedDocumentId,
			focusedDocumentNode,
			focusedDocumentElement
		});
	}

	componentDidMount() {
		this.removeNotifierCallback = selectionManager.newFocusedDocumentNotifier.addCallback(() =>
			this.setState({ focusedDocumentId: selectionManager.focusedDocumentId })
		);
	}

	componentWillUnmount() {
		this.removeNotifierCallback();
	}
}

export default FxFocusedDocument;
