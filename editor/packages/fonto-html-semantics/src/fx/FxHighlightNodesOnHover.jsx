import { Component } from 'react';

import nodeHighlightManager from 'fontoxml-focus-highlight-view/src/nodeHighlightManager.js';

class FxHighlightNodesOnHover extends Component {
	static defaultProps = {
		hoverHighlightType: 'contextual-operation-hover-highlight',
		nodeIds: []
	};

	releaseHighlight = null;

	handleMouseEnter = _event => {
		if (!this.props.hoverHighlightType) {
			return;
		}

		if (this.releaseHighlight) {
			this.releaseHighlight();
			this.releaseHighlight = null;
		}

		if (this.props.nodeIds.length > 0) {
			this.releaseHighlight = nodeHighlightManager.setHighlight(
				this.props.hoverHighlightType,
				this.props.nodeIds
			);
		}
	};

	handleMouseLeave = () => {
		if (this.releaseHighlight) {
			this.releaseHighlight();
			this.releaseHighlight = null;
		}
	};

	render() {
		return this.props.children({
			onMouseEnter: this.handleMouseEnter,
			onMouseLeave: this.handleMouseLeave
		});
	}

	componentWillUnmount() {
		if (this.releaseHighlight) {
			this.releaseHighlight();
			this.releaseHighlight = null;
		}
	}
}

export default FxHighlightNodesOnHover;
