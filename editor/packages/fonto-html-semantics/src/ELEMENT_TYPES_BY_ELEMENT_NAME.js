export default {
	html: 'section',
	section: 'section',
	div: 'block',
	figure: 'block',
	ol: 'block',
	ul: 'block',
	table: 'block',
	span: 'inline'
};
