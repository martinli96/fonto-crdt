import React from 'react';

import { Drop, Menu, MenuGroup, MenuItemWithDrop } from 'fds/components';
import caseConverter from 'fontoxml-utils/src/caseConverter.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import getContextualOperationsForList from 'fontoxml-operations/src/getContextualOperationsForList.js';
import getMarkupLabel from 'fontoxml-markup-documentation/src/getMarkupLabel.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import ContextMenuItemWithDrop from './ContextMenuItemWithDrop.jsx';
import SemanticsSubMenuWithDrop from './SemanticsSubMenuWithDrop.jsx';
import getSemanticsForNode from '../semantics/getSemanticsForNode.js';

const GROUP_COST = 1;
const MAX_FULL_ITEMS = 12;

export default function renderContextualOperationsContextMenuWithSemantics(data) {
	const { contextNodeId, closeContextMenu, getEditorContextMenuGroupsCallbacks = [] } = data;
	if (!contextNodeId) {
		return null;
	}

	// {heading: string, operations: ContextualOperation[]}[]
	const groupsForExtraItems = getEditorContextMenuGroupsCallbacks.reduce(
		(groupsForExtraItems, callback) => {
			const editorContextMenuGroups = callback(data);
			if (editorContextMenuGroups) {
				groupsForExtraItems = groupsForExtraItems.concat(editorContextMenuGroups);
			}
			return groupsForExtraItems;
		},
		[]
	);

	// {contextNodeId: string, heading: string, operations: ContextualOperation[]}[]
	let firstSection = false;
	const groupsForAncestor = evaluateXPathToNodes(
		'ancestor-or-self::node() => reverse()',
		documentsManager.getNodeById(contextNodeId),
		readOnlyBlueprint
	).reduce((groupsOfItems, node) => {
		// Avoid non default table contextual operations
		if (evaluateXPathToBoolean('self::table', node, readOnlyBlueprint)) {
			return groupsOfItems;
		}

		if (firstSection) {
			return groupsOfItems;
		}
		const contextNodeId = getNodeId(node);

		if (evaluateXPathToBoolean('self::section or self::html', node, readOnlyBlueprint)) {
			firstSection = true;
		}

		let operations = getContextualOperationsForList(node, 'context-menu-with-semantics');

		operations = operations.reduce((operations, operation) => {
			if (!operation.name) {
				return operations;
			}

			if (operation.name.startsWith('semantics::')) {
				const semantics = getSemanticsForNode(node);

				if (semantics.length) {
					operations.push({
						operationName: operation.name,
						operationData: { contextNodeId },
						semantics
					});
				}

				return operations;
			}
			operations.push({
				operationName: operation.name,
				operationData: { contextNodeId }
			});
			return operations;
		}, []);

		if (!operations.length) {
			return groupsOfItems;
		}

		const groupOfItems = {
			contextNodeId,
			heading: caseConverter.uppercaseFirstLetter(getMarkupLabel(node) || node.nodeName),
			operations: operations
		};

		groupsOfItems.push(groupOfItems);

		return groupsOfItems;
	}, []);

	if (!groupsForExtraItems.length && !groupsForAncestor.length) {
		return null;
	}

	const [groupForFirstItems, ...groupsForItems] = groupsForExtraItems.concat(groupsForAncestor);

	let itemsRendered = groupForFirstItems.operations.length + GROUP_COST;

	return (
		<Menu>
			<MenuGroup heading={groupForFirstItems.heading} key={0}>
				{groupForFirstItems.operations.map((operation, index) =>
					operation.semantics ? (
						<SemanticsSubMenuWithDrop
							{...operation}
							key={index}
							onClick={closeContextMenu}
						/>
					) : (
						<FxOperationMenuItem
							{...operation}
							key={index}
							onClick={closeContextMenu}
						/>
					)
				)}
			</MenuGroup>

			{groupsForItems &&
				groupsForItems.map((groupForItems, index) => {
					itemsRendered += groupForItems.operations.length + GROUP_COST;
					if (itemsRendered < MAX_FULL_ITEMS) {
						return (
							<MenuGroup heading={groupForItems.heading} key={index + 1}>
								{groupForItems.operations.map((operation, index) =>
									operation.semantics ? (
										<SemanticsSubMenuWithDrop
											{...operation}
											key={index}
											onClick={closeContextMenu}
										/>
									) : (
										<FxOperationMenuItem
											{...operation}
											key={index}
											onClick={closeContextMenu}
										/>
									)
								)}
							</MenuGroup>
						);
					}

					// Use a specialised version of MenuItemWithDrop which highlights the
					// contextNode on hover if a contextNodeId is set.
					const Component = groupForItems.contextNodeId
						? ContextMenuItemWithDrop
						: MenuItemWithDrop;

					return (
						<Component
							contextNodeId={groupForItems.contextNodeId}
							label={groupForItems.heading}
							key={index + 1}
							renderDrop={() => (
								<Drop minWidth={0}>
									<Menu>
										{groupForItems.operations.map((operation, index) =>
											operation.semantics ? (
												<SemanticsSubMenuWithDrop
													{...operation}
													key={index}
													onClick={closeContextMenu}
												/>
											) : (
												<FxOperationMenuItem
													{...operation}
													key={index}
													onClick={closeContextMenu}
												/>
											)
										)}
									</Menu>
								</Drop>
							)}
						/>
					);
				})}
		</Menu>
	);
}
