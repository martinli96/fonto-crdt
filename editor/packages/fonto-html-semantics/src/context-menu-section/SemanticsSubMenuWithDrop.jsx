import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { Drop, Menu } from 'fds/components';
import caseConverter from 'fontoxml-utils/src/caseConverter.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import ContextMenuItemWithDrop from './ContextMenuItemWithDrop.jsx';
import getLabelForNode from '../getLabelForNode.js';

class SemanticsSubMenuWithDrop extends Component {
	static propTypes = {
		onClick: PropTypes.func.isRequired,
		operationName: PropTypes.string.isRequired,
		operationData: PropTypes.object.isRequired,
		semantics: PropTypes.array.isRequired
	};

	items = [];

	state = {
		value: null
	};

	componentDidMount() {
		const contextNodeId = this.props.operationData.contextNodeId;
		const node = contextNodeId && documentsManager.getNodeById(contextNodeId);
		const classValue = evaluateXPathToString('./@class', node, readOnlyBlueprint);

		if (node.nodeName === 'span' || node.nodeName === 'div') {
			this.items = this.props.semantics.concat();
		} else {
			this.items = this.props.semantics.concat([
				{
					value: null,
					label: caseConverter.uppercaseFirstLetter(getLabelForNode(node))
				}
			]);
		}

		this.setState({ value: classValue === '' ? null : classValue });
	}

	render() {
		return (
			<ContextMenuItemWithDrop
				contextNodeId={this.props.operationData.contextNodeId}
				label="Mark as…"
				renderDrop={() => (
					<Drop>
						<Menu>
							{this.items.map((item, index) => (
								<FxOperationMenuItem
									key={index}
									label={item.label}
									isSelected={this.state.value === item.value}
									onClick={this.props.onClick}
									operationName={this.props.operationName}
									operationData={{
										...this.props.operationData,
										type: item.value
									}}
								/>
							))}
						</Menu>
					</Drop>
				)}
			/>
		);
	}
}

export default SemanticsSubMenuWithDrop;
