import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import domInfo from 'fontoxml-dom-utils/src/domInfo.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import selectionManager from 'fontoxml-selection/src/selectionManager.js';
function checkStartParents(startElement, element) {
	if (startElement === element) {
		return true;
	} else if (startElement.previousSibling) {
		return false;
	}

	return checkStartParents(startElement.parentElement, element);
}

function checkStartIsAtStart(startContainer, startOffset, element) {
	if (startContainer === element && startOffset === 0) {
		return true;
	} else if (startOffset === 0) {
		return checkStartParents(startContainer.parentElement, element);
	}

	return false;
}

function checkEndParents(endElement, element) {
	if (endElement === element) {
		return true;
	} else if (endElement.nextSibling) {
		return false;
	}

	return checkEndParents(endElement.parentElement, element);
}

function checkEndIsAtEnd(endContainer, endOffset, element) {
	if (endContainer === element && endOffset === element.childNodes.length) {
		return true;
	} else if (domInfo.isElement(endContainer) && endOffset === endContainer.childNodes.length) {
		return checkEndParents(endContainer.parentElement, element);
	} else if (domInfo.isTextNode(endContainer) && endOffset === endContainer.data.length) {
		return checkEndParents(endContainer.parentElement, element);
	}

	return false;
}

function isRangeExactlyInElement(range, element) {
	const startIsAtStart = checkStartIsAtStart(range.startContainer, range.startOffset, element);
	const endIsAtEnd = checkEndIsAtEnd(range.endContainer, range.endOffset, element);

	return startIsAtStart && endIsAtEnd;
}

function filterNodesWithinRange(nodes, range) {
	let startIndex = -1;
	let endIndex = -1;
	let startNode = range.startContainer;
	let endNode = range.endContainer;

	while (startIndex === -1 && startNode) {
		startIndex = nodes.indexOf(startNode, 0);
		startNode = startNode.parentElement;
	}

	while (endIndex === -1 && endNode) {
		endIndex = nodes.indexOf(endNode, startIndex > 0 ? startIndex : 0);
		endNode = endNode.parentElement;
	}

	return nodes.slice(startIndex, endIndex + 1);
}

function SelectionContext() {
	const selectedElement = selectionManager.getSelectedElement();
	this.isSelectionOutsideDocument = false;

	if (!selectedElement) {
		this.isSelectionOutsideDocument = true;
		return;
	}

	const selectionRange = selectionManager.cloneSelectionRange();
	this._selectionRange = selectionRange;

	this._ancestorNodes = evaluateXPathToNodes(
		'ancestor::node() => reverse()',
		selectedElement,
		readOnlyBlueprint
	);
	this._ancestorNodeNames = this._ancestorNodes.map(function(node) {
		return node.nodeName;
	});
	this._closestSurroundingElement = selectedElement;
	this._isSelectedWithBreadcrumbs =
		selectedElement.parentElement === selectionRange.commonAncestorContainer;
	this._isExactlyOneElementSelected =
		this._isSelectedWithBreadcrumbs || isRangeExactlyInElement(selectionRange, selectedElement);
	this._nodesInSelection = this._isExactlyOneElementSelected
		? selectedElement.childNodes
		: filterNodesWithinRange(selectedElement.childNodes, selectionRange);

	selectionRange.detach();
}

SelectionContext.prototype.isCollapsed = function() {
	return this._selectionRange.collapsed;
};

SelectionContext.prototype.isExactlyOneElementSelected = function() {
	return this._isExactlyOneElementSelected;
};

SelectionContext.prototype.isSelectedWithBreadcrumbs = function() {
	return this._isSelectedWithBreadcrumbs;
};

/*
 * @param {string|string[]} nodeName Checks if the selectedElement is nodeName
 */
SelectionContext.prototype.isSelectedElement = function(nodeName) {
	return domInfo.isElement(this._closestSurroundingElement, nodeName);
};

/*
 * @param {string|string[]} nodeName Checks if the selectedElement or an ancestor of selected element is nodeName
 */
SelectionContext.prototype.isSurroundingElement = function(nodeName) {
	if (Array.isArray(nodeName)) {
		return (
			domInfo.isElement(this._closestSurroundingElement, nodeName) ||
			this._ancestorNodeNames.findIndex(function(ancestorNodeName) {
				return nodeName.includes(ancestorNodeName);
			}) !== -1
		);
	}

	return (
		domInfo.isElement(this._closestSurroundingElement, nodeName) ||
		this._ancestorNodeNames.includes(nodeName)
	);
};

/*
 * @param {string|string[]} nodeNames Checks if the selectedNodes only contains elements with certain nodeNames
 */
SelectionContext.prototype.isSurroundingElements = function(nodeNames) {
	return (
		this._nodesInSelection.every(function(node) {
			return domInfo.isElement(node, nodeNames);
		}) && this._nodesInSelection.length >= 2
	);
};

SelectionContext.prototype.getClosestSurroundingElement = function() {
	return this._closestSurroundingElement;
};

export default SelectionContext;
