import PropTypes from 'prop-types';
import React, { Component } from 'react';

import nodeHighlightManager from 'fontoxml-focus-highlight-view/src/nodeHighlightManager.js';
import { MenuItemWithDrop } from 'fds/components';

class ContextMenuItemWithDrop extends Component {
	static displayName = 'ContextMenuItemWithDrop';

	static defaultProps = {
		contextNodeId: null,
		hoverHighlightType: 'contextual-operation-hover-highlight',
		icon: '',
		label: null,
		tooltipContent: null
	};

	static propTypes = {
		contextNodeId: PropTypes.string,
		hoverHighlightType: PropTypes.string,
		icon: PropTypes.string,
		label: PropTypes.string,
		renderDrop: PropTypes.func.isRequired,
		tooltipContent: PropTypes.string
	};

	releaseHighlight = null;

	handleMouseEnter = _event => {
		if (this.props.contextNodeId) {
			if (this.releaseHighlight) {
				this.releaseHighlight();
			}
			this.releaseHighlight = nodeHighlightManager.setHighlight(
				this.props.hoverHighlightType,
				this.props.contextNodeId
			);
		}
	};

	handleMouseLeave = () => {
		if (this.releaseHighlight) {
			this.releaseHighlight();
			this.releaseHighlight = null;
		}
	};

	render() {
		const { icon, label, renderDrop, tooltipContent } = this.props;

		return (
			<MenuItemWithDrop
				icon={icon}
				label={label}
				onMouseLeave={this.handleMouseLeave}
				renderDrop={renderDrop}
				tooltipContent={tooltipContent}
				onMouseEnter={this.handleMouseEnter}
			/>
		);
	}

	componentWillUnmount() {
		if (this.releaseHighlight) {
			this.releaseHighlight();
			this.releaseHighlight = null;
		}
	}
}

export default ContextMenuItemWithDrop;
