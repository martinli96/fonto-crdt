import React from 'react';

import { Menu, MenuGroup } from 'fds/components';

import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import t from 'fontoxml-localization/src/t.js';

import SelectionContext from './SelectionContext.js';
import getSemanticsForNode from '../semantics/getSemanticsForNode.js';

export default function renderSelectionSemanticsContextMenu({ closeContextMenu }) {
	const selectionContext = new SelectionContext();

	if (selectionContext.isSelectionOutsideDocument) {
		return null;
	}

	let elementName;
	let semantics = [];

	if (
		!selectionContext.isCollapsed() &&
		!selectionContext.isExactlyOneElementSelected() &&
		selectionContext.isSurroundingElement(['caption', 'figcaption', 'h1', 'p'])
	) {
		// a span element can be created
		elementName = 'span';
	} else if (
		!selectionContext.isCollapsed() &&
		(selectionContext.isSurroundingElements([
			'p',
			'div',
			'ul',
			'ol',
			'li',
			'figure',
			'table'
		]) ||
			(selectionContext.isSelectedElement(['div', 'figure', 'table']) &&
				selectionContext.isSelectedWithBreadcrumbs()) ||
			(selectionContext.isSelectedElement(['p', 'ul', 'ol']) &&
				selectionContext.isExactlyOneElementSelected()))
	) {
		// a div element can be created
		elementName = 'div';
	}

	if (elementName) {
		semantics = getSemanticsForNode(
			selectionContext.getClosestSurroundingElement(),
			true,
			elementName
		);
	} else {
		return null;
	}

	return (
		<Menu>
			{semantics.length > 0 && (
				<MenuGroup heading={t('Mark selection as')}>
					{semantics.map((item, index) => (
						<FxOperationMenuItem
							key={index}
							label={item.label}
							onClick={closeContextMenu}
							operationName={'semantics::create-' + elementName + '-and-set-type'}
							operationData={{
								type: item.value
							}}
						/>
					))}
				</MenuGroup>
			)}
			<MenuGroup>
				<FxOperationMenuItem
					onClick={closeContextMenu}
					operationName={'create-' + elementName + '-and-set-output-option'}
				/>
			</MenuGroup>
		</Menu>
	);
}
