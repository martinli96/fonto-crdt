import documentsHierarchy from 'fontoxml-documents/src/documentsHierarchy.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToBoolean from 'fontoxml-selectors/src/evaluateXPathToBoolean.js';
import evaluateXPathToNodes from 'fontoxml-selectors/src/evaluateXPathToNodes.js';
import evaluateXPathToStrings from 'fontoxml-selectors/src/evaluateXPathToStrings.js';
import getNodeId from 'fontoxml-dom-identification/src/getNodeId.js';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import hierarchyNodesLoader from 'fonto-html-multi-document/src/api/hierarchyNodesLoader.js';

import ELEMENT_TYPES_BY_ELEMENT_NAME from '../ELEMENT_TYPES_BY_ELEMENT_NAME.js';

function semanticLevelIsSameOrHigher(elementType, node) {
	if (elementType === 'document') {
		if (
			evaluateXPathToBoolean(
				'child::p[@data-document-type = true()]',
				node,
				readOnlyBlueprint
			)
		) {
			return 'same';
		}
	} else if (elementType === 'section') {
		if (
			evaluateXPathToBoolean('child::p[@data-section-type = true()]', node, readOnlyBlueprint)
		) {
			return 'same';
		} else if (
			evaluateXPathToBoolean(
				'child::p[@data-document-type = true()]',
				node,
				readOnlyBlueprint
			)
		) {
			return 'higher';
		}
	} else if (elementType === 'block') {
		if (
			evaluateXPathToBoolean('child::p[@data-block-type = true()]', node, readOnlyBlueprint)
		) {
			return 'same';
		} else if (
			evaluateXPathToBoolean(
				'child::p[@data-document-type = true() or @data-section-type = true()]',
				node,
				readOnlyBlueprint
			)
		) {
			return 'higher';
		}
	} else if (elementType === 'inline') {
		if (
			evaluateXPathToBoolean('child::p[@data-inline-type = true()]', node, readOnlyBlueprint)
		) {
			return 'same';
		} else if (
			evaluateXPathToBoolean(
				'child::p[@data-document-type = true() or @data-section-type = true() or @data-block-type = true()]',
				node,
				readOnlyBlueprint
			)
		) {
			return 'higher';
		}
	}

	return false;
}

let ancestorRefNodes;

function hasAncestorSemantic(isParentNode, ancestorClass) {
	let hasAncestorSemantic = false;
	for (let i = 0; ancestorRefNodes.length > i; ++i) {
		if (
			evaluateXPathToBoolean(
				'ancestor' +
					(isParentNode ? '-or-self' : '') +
					'::*[@class="' +
					ancestorClass +
					'"]',
				ancestorRefNodes[i],
				readOnlyBlueprint
			)
		) {
			hasAncestorSemantic = true;
			break;
		}
	}
	return hasAncestorSemantic;
}

function recursiveSemanticItemNodes(
	semanticItemNodes,
	semanticsForNode,
	parentPrefix,
	elementType,
	isParentNode
) {
	return semanticItemNodes.reduce(function(semanticsForNode, semanticItemNode) {
		const isSameOrHigher = semanticLevelIsSameOrHigher(elementType, semanticItemNode);
		if (isSameOrHigher) {
			if (isSameOrHigher === 'same') {
				semanticsForNode = semanticsForNode.concat(
					evaluateXPathToStrings(
						'./p/span/string()',
						semanticItemNode,
						readOnlyBlueprint
					).map(function(semantic) {
						return {
							label: semantic,
							value: parentPrefix + semantic
						};
					})
				);
			}

			const childSemanticItemNodes = evaluateXPathToNodes(
				'./ul/li',
				semanticItemNode,
				readOnlyBlueprint
			);
			if (childSemanticItemNodes.length) {
				const ancestorSemantics = evaluateXPathToStrings(
					'./p/span/string()',
					semanticItemNode,
					readOnlyBlueprint
				);

				ancestorSemantics.forEach(function(ancestorSemantic) {
					if (hasAncestorSemantic(isParentNode, parentPrefix + ancestorSemantic)) {
						semanticsForNode = recursiveSemanticItemNodes(
							childSemanticItemNodes,
							semanticsForNode,
							parentPrefix + ancestorSemantic + ':',
							elementType,
							isParentNode
						);
					}
				});
			}
		}

		return semanticsForNode;
	}, semanticsForNode);
}

/*
    get the semantics for the node itself or, for the selection with the given nodeName
*/
export default function getSemanticsForNode(node, isParentNode, nodeName) {
	let elementType = ELEMENT_TYPES_BY_ELEMENT_NAME[nodeName || node.nodeName];

	if (!elementType) {
		return [];
	}

	if (
		!nodeName &&
		node.nodeName === 'html' &&
		evaluateXPathToBoolean('@data-is-root-document', node, readOnlyBlueprint)
	) {
		elementType = 'document';
	} else if (
		evaluateXPathToBoolean(
			'ancestor-or-self::html[@data-is-definition-document]',
			node,
			readOnlyBlueprint
		)
	) {
		return [];
	}

	const templateDocumentId = hierarchyNodesLoader.templateDocumentId;
	const templateDocumentNode = templateDocumentId
		? documentsManager.getDocumentNode(templateDocumentId)
		: null;
	const semanticNodes = templateDocumentNode
		? evaluateXPathToNodes(
				'//html/head/template[1]/ul/descendant::li',
				templateDocumentNode,
				readOnlyBlueprint
		  )
		: [];

	if (!semanticNodes.length) {
		return [];
	}

	ancestorRefNodes = [node];
	if (
		evaluateXPathToBoolean(
			'ancestor-or-self::html[not(@data-is-root-document)]',
			node,
			readOnlyBlueprint
		)
	) {
		const nodeId = getNodeId(node);
		const documentId = documentsManager.getDocumentIdByNodeId(nodeId);
		const hierarchyNode = documentsHierarchy.find(function(hierarchyNode) {
			return (
				hierarchyNode.documentReference &&
				hierarchyNode.documentReference.documentId === documentId
			);
		});
		if (hierarchyNode && hierarchyNode.documentReference) {
			const refNode = documentsManager.getNodeById(
				hierarchyNode.documentReference.sourceNodeId
			);
			if (refNode) {
				ancestorRefNodes.push(refNode);
			}
		}
	}

	return recursiveSemanticItemNodes(semanticNodes, [], '', elementType, isParentNode);
}
