import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { Menu, MenuGroup } from 'fds/components';
import caseConverter from 'fontoxml-utils/src/caseConverter.js';
import documentsManager from 'fontoxml-documents/src/documentsManager.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import FxOperationMenuItem from 'fontoxml-fx/src/FxOperationMenuItem.jsx';
import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';

import getSemanticsForNode from './getSemanticsForNode.js';
import getLabelForNode from '../getLabelForNode.js';

class ChooseSemanticMenu extends Component {
	static defaultProps = {
		operationName: 'semantics::set-element-type'
	};
	static propTypes = {
		nodeId: PropTypes.string.isRequired,
		onClick: PropTypes.func.isRequired,
		operationName: PropTypes.string,
		selectedDocumentType: PropTypes.string
	};

	items = [];
	elementLabel = '';

	state = {
		value: null
	};

	componentDidMount() {
		const node = this.props.nodeId && documentsManager.getNodeById(this.props.nodeId);
		this.elementLabel = getLabelForNode(node);
		const classValue = evaluateXPathToString('./@class', node, readOnlyBlueprint);

		const semantics = getSemanticsForNode(node);

		if (node.nodeName === 'span' || node.nodeName === 'div') {
			this.items = semantics;
		} else {
			this.items = semantics.concat([
				{ value: null, label: caseConverter.uppercaseFirstLetter(this.elementLabel) }
			]);
		}

		this.setState({ value: classValue === '' ? null : classValue });
	}

	render() {
		return (
			<Menu>
				<MenuGroup
					heading={
						'Choose ' +
						(this.elementLabel === '' ? 'element' : this.elementLabel) +
						' type'
					}
				>
					{this.items.map((item, index) => (
						<FxOperationMenuItem
							key={index}
							label={item.label}
							isSelected={this.state.value === item.value}
							onClick={this.props.onClick}
							operationName={this.props.operationName}
							operationData={{
								contextNodeId: this.props.nodeId,
								type: item.value
							}}
						/>
					))}
				</MenuGroup>
			</Menu>
		);
	}
}

export default ChooseSemanticMenu;
