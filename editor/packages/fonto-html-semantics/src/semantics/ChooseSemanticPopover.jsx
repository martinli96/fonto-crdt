import React, { Component } from 'react';

import { Popover, PopoverBody } from 'fds/components';

import FxXPath, { XPATH_RETURN_TYPES } from 'fontoxml-fx/src/FxXPath.jsx';

import FxFocusedDocument from '../fx/FxFocusedDocument.jsx';

import FxHighlightNodesOnHover from '../fx/FxHighlightNodesOnHover.jsx';

import ChooseSemanticMenu from './ChooseSemanticMenu.jsx';

class ChooseSemanticPopover extends Component {
	render() {
		const contextNodeId = this.props.data.contextNodeId;
		const nodeIds = Array.isArray(contextNodeId) ? contextNodeId : [contextNodeId];

		return (
			<FxHighlightNodesOnHover nodeIds={nodeIds}>
				{({ onMouseEnter, onMouseLeave }) => (
					<Popover onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
						<PopoverBody paddingSize={0}>
							<FxFocusedDocument>
								{({ focusedDocumentElement }) => (
									<FxXPath
										context={focusedDocumentElement}
										expression="./@class"
										returnType={XPATH_RETURN_TYPES.STRING_TYPE}
									>
										{selectedDocumentType => (
											<ChooseSemanticMenu
												nodeId={nodeIds[0]}
												onClick={this.props.togglePopover}
												operationName={this.props.data.operationName}
												selectedDocumentType={selectedDocumentType}
											/>
										)}
									</FxXPath>
								)}
							</FxFocusedDocument>
						</PopoverBody>
					</Popover>
				)}
			</FxHighlightNodesOnHover>
		);
	}
}

export default ChooseSemanticPopover;
