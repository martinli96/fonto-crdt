import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';
import applyCss from 'fontoxml-styles/src/applyCss.js';

const widgetStyles = applyCss({
	cursor: 'pointer'
});

const documentIconContainerStyles = applyCss({
	position: 'absolute',
	transform: 'translateX(calc(-100% - 8px))'
});

const iconStyles = applyCss({
	display: 'inline-flex',
	flexDirection: 'row',
	alignItems: 'center',
	userSelect: 'none'
});
const innerIconStyles = applyCss({
	display: 'block',
	fontFamily: 'FontAwesome',
	WebkitFontSmoothing: 'antialiased',
	fontSize: '0.875rem',
	lineHeight: '0.875rem'
});

export default function createSemanticSectionLabelWidget() {
	return function createSemanticSectionLabelWidgetJsonMl(sourceNode) {
		const documentType = evaluateXPathToString('./@class', sourceNode, readOnlyBlueprint);

		const jsonMl = [
			'cv-inline-label-widget',
			Object.assign(
				{ 'cv-font-stack': 'interface' },
				{
					'block-selection-change-on-click': true,
					'popover-component-name': 'ChooseSemanticPopover',
					'popover-data': JSON.stringify({
						operationName: 'semantics::set-element-type'
					}),
					'popover-context-node-id': sourceNode.nodeId
				},
				widgetStyles
			),
			[
				'span',
				documentIconContainerStyles,
				[
					'span',
					iconStyles,
					['span', Object.assign({ class: 'icon-file-text-o icon-fw' }, innerIconStyles)]
				]
			],
			['span', documentType ? documentType : 'document'],
			[
				'span',
				iconStyles,
				['span', Object.assign({ class: 'icon-angle-down icon-fw' }, innerIconStyles)]
			]
		];

		return jsonMl;
	};
}
