import readOnlyBlueprint from 'fontoxml-blueprints/src/readOnlyBlueprint.js';
import evaluateXPathToFirstNode from 'fontoxml-selectors/src/evaluateXPathToFirstNode.js';
import evaluateXPathToString from 'fontoxml-selectors/src/evaluateXPathToString.js';

export default function createSemanticBlockLabelWidget() {
	return function createSemanticBlockLabelWidgetJsonMl(sourceNode) {
		const semanticNode = evaluateXPathToFirstNode(
			'if (self::body) then parent::html else .',
			sourceNode,
			readOnlyBlueprint
		);
		let semantic = evaluateXPathToString('./@class', semanticNode, readOnlyBlueprint);

		if (semantic === '') {
			return;
		}

		const semantics = semantic.split(':');
		semantic = semantics[semantics.length - 1];

		return [
			'cv-inline-label-widget',
			Object.assign(
				{ 'cv-font-stack': 'interface' },
				{
					'block-selection-change-on-click': true,
					'popover-component-name': 'ChooseSemanticPopover',
					'popover-context-node-id': semanticNode.nodeId
				}
			),
			['span', semantic],
			['cv-icon', { class: 'icon icon-angle-down' }]
		];
	};
}
