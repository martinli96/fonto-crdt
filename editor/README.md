# fonto-html-editor

## Changes in Platform

NOTE: When the editor is upgraded, make sure the following changes are still there:

* DEV-1380 Add "context-menu-with-semantics" to the list of contextual operations menus. Changed in fontoxml-operations/src/metadata/OperationsNodeMetadataConfigurationApi.js
* Dynamic markup labels with xpath. Changed in fontoxml-markup-documentation/src/getMarkupLabel.js

## Technical debt
Used isEditableDocumentAndView which is a private API from fontoxml families, for data source placeholder.

Could not do validation of the html-fragment against the html-fragment schema, as it is not possible to only validate an xml fragment without loading it is a document.

## Installation
- Install [NodeJS](https://nodejs.org).
- Install local dependencies
  ```
  $ npm install
  ```

## Building the application

```
$ npm run build
```

For additional help for this command, run: `$ npm run build -- --help`

## Serving the application

There are two ways of serving the application. Both methods stub the CMS connector endpoints.

### Development server

Serving the application while building the files from source on each load.

```
$ npm run server
```

### Build server

Serving the application directly from the `dist` directory.

```
$ npm run build-server
```

For additional help for this command, run: `$ npm run server -- --help`
