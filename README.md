# Fonto for Business

This repository contains all files you need to run the Fonto for Business editor including its schema.

# Prerequisites

You will also need FDT to run the Editor.


## Notes

The editor has one inlined platform packages and relies on a nightly at the moment of writing.
* Dynamic markup labels with xpath. Changed in fontoxml-markup-documentation/src/getMarkupLabel.js

Used isEditableDocumentAndView which is a private API from fontoxml families, for data source placeholder.

Could not do validation of the html-fragment against the html-fragment schema, as it is not possible to only validate an xml fragment without loading it is a document.
