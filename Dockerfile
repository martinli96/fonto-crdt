# FDT image
FROM node:8-alpine as fonto-sdk-7.7
WORKDIR /app

RUN apk add --no-cache \
    curl

ARG FONTO_FDT_LICENSE
RUN echo "$FONTO_FDT_LICENSE"  > ~/fonto.lic && \
    chmod 600 ~/fonto.lic

RUN npm config set unsafe-perm true
RUN npm install --global @fontoxml/fontoxml-development-tools@3.0.0
RUN fdt license validate

# Editor build image
FROM fonto-sdk-7.7 as build-editor
WORKDIR /app

COPY ./editor/ ./
RUN fdt editor build

EXPOSE 80/tcp

ENTRYPOINT ["fdt", "editor", "run", "--port", "80", "--dist", "--savemode", "session"]